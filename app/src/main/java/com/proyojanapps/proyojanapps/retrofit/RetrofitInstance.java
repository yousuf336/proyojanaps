package com.proyojanapps.proyojanapps.retrofit;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {
    public static String BASE_URL = "https://proyojanearn.com";
    private static String BASE_URL_PLACE_API = "https://maps.googleapis.com/maps/api";
    private static String BASE_URL_FCM = "https://fcm.googleapis.com";
    private static Retrofit retrofit;

    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(15, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .build();

    public static Retrofit getRetrofitInstance() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        }
        return retrofit;
    }

    public static Retrofit getRetrofitInstanceForPlaceAPI() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL_PLACE_API).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }

    public static Retrofit getRetrofitInstanceForFCM() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL_FCM).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
