package com.proyojanapps.proyojanapps.retrofit;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiService {
    //    @GET("/place/autocomplete/json")
//    Call<Predictions> getPlacesAutoComplete(
//            @Query("input") String input,
//            @Query("types") String types,
//            @Query("location") String location,
//            @Query("radius") String radius,
//            @Query("strictbounds") String strictbounds,
//            @Query("key") String key
//    );
//
//
//    @Headers({"Content-Type: application/json", "Authorization: key=AAAAJSsJJdQ:APA91bFVcYxW47KrqwHopxTr3Wm2mKypbm7m6_qVZD4D27yuYC1nk4UznzCFq73nTVQVD25QIcqrjY6CYzpvbu2CPueOgjW3qk2sWl0ESLKGCtT_4Hr3uSkGXStl0-g9z6HDKVNWzLA0"})
//    @POST("/fcm/send")
//    Call<ResponseBody> sendNotification(
//            @Body Notification notification);
//
//
//    @POST("/api/brandList")
//    Call<Object> getBrandList();
//
//    @POST("/api/categoryList")
//    Call<Object> getCategoryList();
//
//
//    @Headers({"Accept: application/json"})
//    @POST("/api/customerToDealerReviewNotSubmittedList")
//    Call<Object> getOrderListWithoutRating(@Header("Authorization") String token);
    @Headers({"Accept: application/json"})
    @POST("/api/notificationList")
    Call<Object> getNotificationList(@Header("Authorization") String token);

    @Headers({"Accept: application/json"})
    @POST("/api/notificationRead")
    Call<Object> changeNotificationStatus(@Header("Authorization") String token,
                                          @Query("notificationId") int notificationId);

    @GET("/api/slider")
    Call<Object> getSliderList();

    @GET("/api/division")
    Call<Object> getDivisionList();

    @GET("/api/termscondition")
    Call<Object> getTermsAndCondition();

    @POST("/api/district")
    Call<Object> getDistrictList(@Query("division_id") int divisionId);

    @POST("/api/upazila")
    Call<Object> getUpazilaList(@Query("district_id") int district_id);

    @POST("/api/union")
    Call<Object> getUnionList(@Query("upazilla_id") int upazilla_id);

    @POST("/api/visitorcontact")
    Call<Object> submitVisitorContact(@Body Map<String, Object> map);

    @GET("/api/notice")
    Call<Object> getNotice();

    @GET("/api/my_income_plan")
    Call<Object> myIncomePlan();

    @Headers({"Accept: application/json"})
    @POST("/api/freelancer_registration")
    Call<Object> freelancerRegistration(@Body Map<String, Object> registrationMap);

    @Headers({"Accept: application/json"})
    @POST("/api/freelancer_login")
    Call<Object> freelancerLogin(@Body Map<String, Object> loginMap);

    @Headers({"Accept: application/json"})
    @POST("/api/forgetphoneverify")
    Call<Object> freelancerForgetphoneverify(@Query("phone") String phone);

    @Headers({"Accept: application/json"})
    @POST("/api/forgetcodeverify")
    Call<Object> freelancerForgetcodeverify(@Body Map<String, Object> forgetcodeverifyMap);

    @Headers({"Accept: application/json"})
    @POST("/api/forgetconfirm")
    Call<Object> freelancerForgetconfirm(@Body Map<String, Object> forgetconfirmMap);


    @Headers({"Accept: application/json"})
    @POST("/api/change_password")
    Call<Object> freelancerChangePassword(@Body Map<String, Object> changePasswordMap);

    @Headers({"Accept: application/json"})
    @POST("/api/change_number")
    Call<Object> freelancerChangeNumber(@Body Map<String, Object> changeNumberMap);

    @Headers({"Accept: application/json"})
    @POST("/api/resend_code")
    Call<Object> freelancerResendCode(@Query("freelancer_id") int freelancer_id);

    @Headers({"Accept: application/json"})
    @POST("/api/security_pin")
    Call<Object> freelancerSecurityPin(@Body Map<String, Object> securityMap);

    @Headers({"Accept: application/json"})
    @POST("/api/freelancer/account/active")
    Call<Object> freelancerActive(@Query("freelancer_id") int freelancer_id);

    @Headers({"Accept: application/json"})
    @POST("/api/workplace")
    Call<Object> workplace(@Query("freelancer_id") int freelancer_id);

    @Headers({"Accept: application/json"})
    @POST("/api/mynetwork")
    Call<Object> freelancerMyNetwork(@Query("freelancer_id") int freelancer_id);

    @Headers({"Accept: application/json"})
    @POST("/api/incomereport")
    Call<Object> freelancerIncomereport(@Query("freelancer_id") int freelancer_id);

    @Headers({"Accept: application/json"})
    @GET("/api/incomeplans")
    Call<Object> freelancerIncomeplans();

    @Headers({"Accept: application/json"})
    @POST("/api/cash_out_amount")
    Call<Object> cashOut(@Body Map<String, Object> cashOutMap);

    @Headers({"Accept: application/json"})
    @POST("/api/transfer_amount")
    Call<Object> freelancerTransferAmount(@Body Map<String, Object> transferAmountMap);

    @Headers({"Accept: application/json"})
    @POST("/api/working_renew")
    Call<Object> workingRenew(@Query("freelancer_id") int freelancer_id);

    @Headers({"Accept: application/json"})
    @POST("/api/withdraw_balance")
    Call<Object> withdrawBalance(@Query("freelancer_id") int freelancer_id);

    @Headers({"Accept: application/json"})
    @POST("/api/professional_career")
    Call<Object> addWork(@Body Map<String, Object> map);

    @Headers({"Accept: application/json"})
    @POST("/api/education")
    Call<Object> addEducation(@Body Map<String, Object> map);

    @Headers({"Accept: application/json"})
    @POST("/api/skill")
    Call<Object> addSkill(@Body Map<String, Object> map);

    @Headers({"Accept: application/json"})
    @POST("/api/freelancer_history")
    Call<Object> freelancerHistory(@Query("freelancer_id") int freelancer_id);

    @Headers({"Accept: application/json"})
    @GET("/api/my_offers")
    Call<Object> myOffer();


    @Multipart
    @Headers({"Accept: application/json"})
    @POST("/api/chageprofilepic")
    Call<Object> freelancerChageprofilepic(
            @Part("freelancer_id") RequestBody freelancer_id,
            @Part MultipartBody.Part profilepic);

    @Headers({"Accept: application/json"})
    @POST("/api/profileupdate")
    Call<Object> updateProfile(@Body Map<String, Object> map);

    @Multipart
    @Headers({"Accept: application/json"})
    @POST("/api/agent_registration")
    Call<Object> agentRegistration(@Part("freelancer_id") RequestBody freelancer_id,
                                   @Part("dateofbirth") RequestBody dateofbirth,
                                   @Part("gender") RequestBody gender,
                                   @Part("occupation") RequestBody occupation,
                                   @Part("address") RequestBody address,
                                   @Part("division") RequestBody division,
                                   @Part("district") RequestBody district,
                                   @Part("thana") RequestBody thana,
                                   @Part("union") RequestBody union,
                                   @Part("sendernumber") RequestBody sendernumber,
                                   @Part("transectionid") RequestBody transectionid,
                                   @Part("amount") RequestBody amount,
                                   @Part MultipartBody.Part nidfront,
                                   @Part MultipartBody.Part nidback,
                                   @Part MultipartBody.Part trans_slip);

    @Headers({"Accept: application/json"})
    @POST("/api/agent_login")
    Call<Object> agentLogin(@Body Map<String, Object> loginMap);


    @GET("/api/agent_list")
    Call<Object> agentList();

    @GET("/api/testimonial")
    Call<Object> getTestimonial();

    @GET("/api/about_us")
    Call<Object> aboutUs();

    @Headers({"Accept: application/json"})
    @POST("/api/send_money")
    Call<Object> agentSendMoney(@Body Map<String, Object> sendMoneyMap);

    @Headers({"Accept: application/json"})
    @POST("/api/agent_cash_out_request_list")
    Call<Object> agentCashoutRequestList(@Query("agent_reefer_id") String agent_reefer_id);

    @Headers({"Accept: application/json"})
    @POST("/api/agent_cash_out_paid")
    Call<Object> agentCashOutPaid(@Query("agent_id") int agent_id, @Query("hidden_id") int hidden_id);

    @Headers({"Accept: application/json"})
    @POST("/api/agent_cash_out_unpaid")
    Call<Object> agentCashOutUnpaid(@Query("agent_id") int agent_id, @Query("hidden_id") int hidden_id);

    @Headers({"Accept: application/json"})
    @POST("/api/agent_send_money_list")
    Call<Object> agentSendMoneyList(@Query("agent_id") int agent_id);

    @Headers({"Accept: application/json"})
    @POST("/api/agent_cash_in_list")
    Call<Object> agentCashInList(@Query("agent_id") int agent_id);

    @Headers({"Accept: application/json"})
    @POST("/api/agent_cash_out_list")
    Call<Object> agentCashOutList(@Query("agent_id") int agent_id);


    @Headers({"Accept: application/json"})
    @POST("/api/agent_submit_cash_in")
    Call<Object> agentSubmitCashin(@Body Map<String, Object> map);

    @Headers({"Accept: application/json"})
    @POST("/api/agent_submit_cash_out")
    Call<Object> agentSubmitCashOut(@Body Map<String, Object> map);
}
