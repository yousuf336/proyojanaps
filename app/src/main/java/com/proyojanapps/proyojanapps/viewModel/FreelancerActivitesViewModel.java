package com.proyojanapps.proyojanapps.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.FreelancerIncomeReport;
import com.proyojanapps.proyojanapps.model.FreelancerNetwork;
import com.proyojanapps.proyojanapps.model.IncomePlans;
import com.proyojanapps.proyojanapps.model.Workplace;
import com.proyojanapps.proyojanapps.repository.FreelancerActivitesRepo;
import com.proyojanapps.proyojanapps.repository.FreelancerRepo;

import java.util.List;

public class FreelancerActivitesViewModel extends AndroidViewModel {
    private FreelancerActivitesRepo freelancerActivitesRepo;

    public FreelancerActivitesViewModel(@NonNull Application application) {
        super(application);
        freelancerActivitesRepo = new FreelancerActivitesRepo(application);
    }

    public MutableLiveData<List<IncomePlans>> getIncomePlans() {
        return freelancerActivitesRepo.incomePlan();
    }

    public MutableLiveData<List<FreelancerNetwork>> myNetwork(int freelancer_id) {
        return freelancerActivitesRepo.myNetwork(freelancer_id);
    }

    public MutableLiveData<FreelancerIncomeReport> incomeReport(int freelancer_id) {
        return freelancerActivitesRepo.incomeReport(freelancer_id);
    }

    public MutableLiveData<String> cashOut(int freelancer_id, String agent_id, int payment_method, String amount) {
        return freelancerActivitesRepo.cashOut(freelancer_id, agent_id, payment_method, amount);
    }

    public MutableLiveData<String> transferAmount(int freelancer_id, String ownreferid, int payment_method, String amount) {
        return freelancerActivitesRepo.transferAmount(freelancer_id, ownreferid, payment_method, amount);
    }

    public MutableLiveData<Workplace> myWorkPlace(int freelancer_id) {
        return freelancerActivitesRepo.workplace(freelancer_id);
    }

    public MutableLiveData<String> workingRenew(int freelancer_id) {
        return freelancerActivitesRepo.workingRenew(freelancer_id);
    }
    public MutableLiveData<String> withdrawBalance(int freelancer_id) {
        return freelancerActivitesRepo.withdrawBalance(freelancer_id);
    }
}
