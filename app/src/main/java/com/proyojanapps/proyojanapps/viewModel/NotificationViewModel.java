package com.proyojanapps.proyojanapps.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.proyojanapps.proyojanapps.model.Notification;
import com.proyojanapps.proyojanapps.model.ResponseBody;
import com.proyojanapps.proyojanapps.repository.NotificationRepo;

import java.util.List;

public class NotificationViewModel extends AndroidViewModel {
    private NotificationRepo notificationRepo;

    public NotificationViewModel(@NonNull Application application) {
        super(application);
        notificationRepo = new NotificationRepo(application);
    }


    public MutableLiveData<List<Notification>> getNotificationList() {
        return notificationRepo.getNotificationList();
    }


    public MutableLiveData<ResponseBody> changeNotificationStatus(int notificationId) {
        return notificationRepo.changeNotificationStatus(notificationId);
    }
}
