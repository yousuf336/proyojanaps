package com.proyojanapps.proyojanapps.viewModel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.proyojanapps.proyojanapps.model.AboutUs;
import com.proyojanapps.proyojanapps.model.Agent;
import com.proyojanapps.proyojanapps.model.Contact;
import com.proyojanapps.proyojanapps.model.District;
import com.proyojanapps.proyojanapps.model.Division;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.TermsAndCondition;
import com.proyojanapps.proyojanapps.model.Testimonial;
import com.proyojanapps.proyojanapps.model.Union;
import com.proyojanapps.proyojanapps.model.Upazilla;
import com.proyojanapps.proyojanapps.repository.AgentRepo;
import com.proyojanapps.proyojanapps.repository.UtilityRepo;

import java.util.List;
import java.util.Map;

public class UtilityViewModel extends AndroidViewModel {

    private UtilityRepo utilityRepo;

    public UtilityViewModel(@NonNull Application application) {
        super(application);
        utilityRepo = new UtilityRepo(application);
    }


    public MutableLiveData<List<Division>> getAllDivision() {
        return utilityRepo.getDivisionList();
    }

    public MutableLiveData<List<District>> getDistrictList(int divisionId) {
        return utilityRepo.getDistrictList(divisionId);
    }

    public MutableLiveData<List<Upazilla>> getUpazilaList(int districtId) {
        return utilityRepo.getUpazilaList(districtId);
    }

    public MutableLiveData<List<Union>> getUnionList(int upazilaId) {
        return utilityRepo.getUnionList(upazilaId);
    }

    public MutableLiveData<TermsAndCondition> getTermsAndConditions() {
        return utilityRepo.getTermsAndConditions();
    }

    public MutableLiveData<String> getNotice() {
        return utilityRepo.getNotice();
    }

    public MutableLiveData<String> submitMessage(Map<String, Object> map) {
        return utilityRepo.submitMessage(map);
    }

    public MutableLiveData<List<Testimonial>> getTestimonialList() {
        return utilityRepo.getTestimonial();
    }

    public MutableLiveData<List<Contact>> getAllContact(Context context) {
        return utilityRepo.readContact(context);
    }

    public MutableLiveData<AboutUs> aboutUst() {
        return utilityRepo.getAboutUs();
    }

}
