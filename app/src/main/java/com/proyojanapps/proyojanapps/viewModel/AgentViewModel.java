package com.proyojanapps.proyojanapps.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.proyojanapps.proyojanapps.model.Agent;
import com.proyojanapps.proyojanapps.model.AgentList;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.repository.AgentRepo;
import com.proyojanapps.proyojanapps.repository.FreelancerRepo;

import java.util.List;

public class AgentViewModel extends AndroidViewModel {

    private AgentRepo agentRepo;

    public AgentViewModel(@NonNull Application application) {
        super(application);
        agentRepo = new AgentRepo(application);
    }


    public MutableLiveData<String> agentRegistration(Agent agent) {
        return agentRepo.agentRegistration(agent);
    }


    public MutableLiveData<Agent> logIn(String username, String password) {
        return agentRepo.login(username, password);
    }
    public MutableLiveData<List<AgentList>> getAgentList() {
        return agentRepo.agentList();
    }
}
