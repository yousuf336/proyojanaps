package com.proyojanapps.proyojanapps.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.proyojanapps.proyojanapps.model.Transaction;
import com.proyojanapps.proyojanapps.repository.AgentActivitesRepo;

import java.util.List;

public class AgentActivitesViewModel extends AndroidViewModel {

    private AgentActivitesRepo agentRepo;

    public AgentActivitesViewModel(@NonNull Application application) {
        super(application);
        agentRepo = new AgentActivitesRepo(application);
    }

    public MutableLiveData<String> sendMoney(int agent_id, String ownreferid, String amount, String password) {
        return agentRepo.sendMoney(agent_id, ownreferid, amount, password);
    }

    public MutableLiveData<List<Transaction.AgentCashOutRequest>> cashOutRequestList(String agent_reefer_id) {
        return agentRepo.cashOutRequestList(agent_reefer_id);
    }

    public MutableLiveData<List<Transaction.AgentSendMoney>> sendMoneytList(int agent_id) {
        return agentRepo.sendMoneyList(agent_id);
    }

    public MutableLiveData<List<Transaction.AgentCashOut>> cashOutList(int agent_id) {
        return agentRepo.cashOutList(agent_id);
    }

    public MutableLiveData<List<Transaction.AgentCashin>> cashinList(int agent_id) {
        return agentRepo.cashInList(agent_id);
    }

    public MutableLiveData<String> submitCashout(Transaction.AgentCashOut cashOut) {
        return agentRepo.cashoutSubmit(cashOut);
    }

    public MutableLiveData<String> submitCashin(Transaction.AgentCashin cashin) {
        return agentRepo.cashinSubmit(cashin);
    }

    public MutableLiveData<String> cashoutPaid(int agent_id, int hiddenId) {
        return agentRepo.cashoutPaid(agent_id, hiddenId);
    }

    public MutableLiveData<String> cashoutUnPaid(int agent_id, int hiddenId) {
        return agentRepo.cashoutUnPaid(agent_id, hiddenId);
    }
}
