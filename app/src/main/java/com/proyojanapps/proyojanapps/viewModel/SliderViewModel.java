package com.proyojanapps.proyojanapps.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.proyojanapps.proyojanapps.model.Slider;
import com.proyojanapps.proyojanapps.repository.SliderRepo;

import java.util.List;

public class SliderViewModel extends AndroidViewModel {

    private SliderRepo sliderRepo;

    public SliderViewModel(@NonNull Application application) {
        super(application);
        sliderRepo = new SliderRepo(application);
    }


    public MutableLiveData<List<Slider>> getSliderList() {
        return sliderRepo.getSliderList();
    }
}
