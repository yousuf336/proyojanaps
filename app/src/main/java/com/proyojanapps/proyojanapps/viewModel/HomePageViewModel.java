package com.proyojanapps.proyojanapps.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.proyojanapps.proyojanapps.model.Slider;
import com.proyojanapps.proyojanapps.model.Testimonial;
import com.proyojanapps.proyojanapps.repository.HomePageRepo;
import com.proyojanapps.proyojanapps.repository.SliderRepo;

import java.util.List;

public class HomePageViewModel extends AndroidViewModel {

    private HomePageRepo homePageRepo;

    public HomePageViewModel(@NonNull Application application) {
        super(application);
        homePageRepo = new HomePageRepo(application);
    }


    public MutableLiveData<List<Testimonial>> getTestimonialList() {
        return homePageRepo.getTestimonial();
    }
}
