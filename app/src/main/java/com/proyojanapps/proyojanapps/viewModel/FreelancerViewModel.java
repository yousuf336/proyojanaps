package com.proyojanapps.proyojanapps.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.FreelancerIncomeReport;
import com.proyojanapps.proyojanapps.model.FreelancerNetwork;
import com.proyojanapps.proyojanapps.repository.FreelancerRepo;

import java.io.File;
import java.util.List;

public class FreelancerViewModel extends AndroidViewModel {

    private FreelancerRepo freelancerRepo;

    public FreelancerViewModel(@NonNull Application application) {
        super(application);
        freelancerRepo = new FreelancerRepo(application);
    }


    public MutableLiveData<Freelancer> frelancerRegistration(Freelancer registration) {
        return freelancerRepo.registration(registration);
    }


    public MutableLiveData<Freelancer> logIn(String userId, String password) {
        return freelancerRepo.login(userId, password);
    }

    public MutableLiveData<String> securityPinSubmition(int id, String pin) {
        return freelancerRepo.securityPin(id, pin);
    }

    public MutableLiveData<Freelancer> forgetPhoneverify(String phone) {
        return freelancerRepo.forgetPhoneverify(phone);
    }

    public MutableLiveData<Freelancer> changeNumber(int frelancerId, String phone) {
        return freelancerRepo.changeNumber(frelancerId, phone);
    }

    public MutableLiveData<Freelancer> resendCode(int frelancerId) {
        return freelancerRepo.resendCode(frelancerId);
    }

    public MutableLiveData<Freelancer> forgetPhoneCodeVerify(String freelancer_id, String forgetverifycode) {
        return freelancerRepo.forgetCodeverify(freelancer_id, forgetverifycode);
    }

    public MutableLiveData<String> forgetConfirm(String freelancer_id, String fnewpassword) {
        return freelancerRepo.forgetConfirm(freelancer_id, fnewpassword);
    }

    public MutableLiveData<String> changePassword(int freelancer_id, String oldPassword, String newPassword) {
        return freelancerRepo.changePassword(freelancer_id, oldPassword, newPassword);
    }

    public MutableLiveData<String> accountActivation(int freelancer_id) {
        return freelancerRepo.activeAccount(freelancer_id);
    }

    public MutableLiveData<String> changeProfilePic(int freelancer_id, File profilePic) {
        return freelancerRepo.changeProfilePic(freelancer_id, profilePic);
    }

    public MutableLiveData<Freelancer> addWork(Freelancer.Work work) {
        return freelancerRepo.addWork(work);
    }

    public MutableLiveData<Freelancer> addEducation(Freelancer.Education education) {
        return freelancerRepo.addEducation(education);
    }

    public MutableLiveData<Freelancer> addSkill(Freelancer.Skill skill) {
        return freelancerRepo.addSkill(skill);
    }

    public MutableLiveData<Freelancer> updateProfile(Freelancer freelancer) {
        return freelancerRepo.profileUpdate(freelancer);
    }

    public MutableLiveData<List<Freelancer.History>> history(int freelancer_id) {
        return freelancerRepo.history(freelancer_id);
    }

    public MutableLiveData<List<Freelancer.MyOffers>> myOffer() {
        return freelancerRepo.myOffer();
    }

  public MutableLiveData<List<Freelancer.MyIncomePlan>> myIncomePlan() {
        return freelancerRepo.myIncomePlan();
    }


}
