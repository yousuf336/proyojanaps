package com.proyojanapps.proyojanapps.view.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.DialogImageZoomBinding;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;

public class ZoomImageDialog extends DialogFragment {

    private DialogImageZoomBinding binding;
    private Dialog dialog;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_image_zoom, container, false);

        Bundle bundle = getArguments();
        String imageUrl = bundle.getString("imageUrl", "");

        if (!imageUrl.equals("")) {
            Glide.with(context).applyDefaultRequestOptions(new RequestOptions()
                    .placeholder(R.drawable.ic_image_black_24dp)).load(RetrofitInstance.BASE_URL + imageUrl).into(binding.photoView);

        }

        binding.backBtn.setOnClickListener(v -> dismiss());

        return binding.getRoot();
    }


    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();
        if (dialog != null) {
            //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getActivity().getResources().getColor(R.color.transparent_black)));
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
