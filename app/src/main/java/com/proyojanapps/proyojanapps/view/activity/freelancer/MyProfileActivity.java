package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityMyProfileBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;

public class MyProfileActivity extends AppCompatActivity {
    private Freelancer freelancerInfo;
    private MySharedPreparence preparence;
    private ActivityMyProfileBinding binding;
    private FreelancerViewModel freelancerViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this
                , R.layout.activity_my_profile);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });


        binding.btnChangePassword.setOnClickListener(view -> {
            startActivity(new Intent(this, PasswordChangeActivity.class)
                    .putExtra("identity", StaticKey.CHANGE_PASSWORD));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.ivAddProfession.setOnClickListener(view -> {
            startActivity(new Intent(this, AddWorkActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.ivAddEducation.setOnClickListener(view -> {
            startActivity(new Intent(this, AddEducationActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.ivAddSkills.setOnClickListener(view -> {
            startActivity(new Intent(this, AddSkillActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        });
        binding.ivEdit.setOnClickListener(view -> {
            startActivity(new Intent(this, UpdateProfileActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        });

        binding.ivEditProfession.setOnClickListener(view -> {
            startActivity(new Intent(this, AddWorkActivity.class)
                    .putExtra("identity", "update"));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        binding.ivEditEducation.setOnClickListener(view -> {
            startActivity(new Intent(this, AddEducationActivity.class)
                    .putExtra("identity", "update"));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.ivEditSkill.setOnClickListener(view -> {
            startActivity(new Intent(this, AddSkillActivity.class)
                    .putExtra("identity", "update"));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        });
        init();
        setData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();
    }

    private void setData() {
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
        if (freelancerInfo.getMemberFrom() != null)
            binding.tvMemberSinch.setText(" : " + freelancerInfo.getMemberFrom());

        binding.tvName.setText(" : " + freelancerInfo.getName());
        binding.tvEmail.setText(" : " + freelancerInfo.getEmail());
        binding.tvPhone.setText(" : " + freelancerInfo.getPhone());
        if (freelancerInfo.getDateOfBirth() != null) {
            binding.tvDob.setText(" : " + freelancerInfo.getDateOfBirth());
        }
        if (freelancerInfo.getGender() != null) {
            binding.tvGender.setText(" : " + freelancerInfo.getGender());
        }

        if (freelancerInfo.getDivision() != null) {
            binding.tvDivision.setText(" : " + freelancerInfo.getDivision());
        }
        if (freelancerInfo.getDistrict() != null) {
            binding.tvDistrict.setText(" : " + freelancerInfo.getDistrict());
        }
        if (freelancerInfo.getThana() != null) {
            binding.tvThana.setText(" : " + freelancerInfo.getThana());
        }
        if (freelancerInfo.getUnion() != null) {
            binding.tvUnion.setText(" : " + freelancerInfo.getUnion());
        }
        if (freelancerInfo.getBloodGroup() != null) {
            binding.tvBloodGroup.setText(" : " + freelancerInfo.getBloodGroup());
        }
        if (freelancerInfo.getOccupation() != null) {
            binding.tvProfession.setText(" : " + freelancerInfo.getOccupation());
        }
        if (freelancerInfo.getAddress() != null) {
            binding.tvAddress.setText(" : " + freelancerInfo.getAddress());
        }

        if (freelancerInfo.getCompanyName() != null) {
            binding.tvEmptyProfession.setVisibility(View.GONE);
            binding.llProfessionData.setVisibility(View.VISIBLE);
            binding.tvOrganizationName.setText("" + freelancerInfo.getCompanyName());
            binding.tvDesignation.setText("" + freelancerInfo.getJobLocation());
            binding.tvDuraionProf.setText("" + freelancerInfo.getJobFrom());
            if (freelancerInfo.getCurrentJob() != null && freelancerInfo.getCurrentJob().equals("1"))
                binding.tvDuraionProf.setText("" + freelancerInfo.getJobFrom() + " To Present");
            else {
                binding.tvDuraionProf.setText("" + freelancerInfo.getJobFrom() + " To " + freelancerInfo.getJobTo());
            }
        } else {
            binding.llProfessionData.setVisibility(View.GONE);
            binding.tvEmptyProfession.setVisibility(View.VISIBLE);
        }
        if (freelancerInfo.getEducation() != null) {
            binding.tvEmptyEducation.setVisibility(View.GONE);
            binding.llEducationData.setVisibility(View.VISIBLE);
            binding.tvInstituteName.setText("" + freelancerInfo.getInstutationName());
            binding.tvClass.setText("" + freelancerInfo.getEducationLevel());
            if (freelancerInfo.getCurrentStudy() != null && freelancerInfo.getCurrentStudy().equals("1"))
                binding.tvEduDuration.setText("" + freelancerInfo.getEducationFrom() + " To Present");
            else {
                binding.tvEduDuration.setText("" + freelancerInfo.getEducationFrom() + " To " + freelancerInfo.getEducationTo());
            }
        } else {
            binding.tvEmptyEducation.setVisibility(View.VISIBLE);
            binding.llEducationData.setVisibility(View.GONE);
        }
        if (freelancerInfo.getSkill() != null) {
            binding.llSkillsData.setVisibility(View.VISIBLE);
            binding.tvEmptySkill.setVisibility(View.GONE);
            binding.tvSkillName.setText("" + freelancerInfo.getSkill());
//            binding.tvField.setText("" + freelancerInfo.getF());
//            binding.tvSkillDuration.setText("From : " + freelancerInfo.getEducationFrom());
        } else {
            binding.llSkillsData.setVisibility(View.GONE);
            binding.tvEmptySkill.setVisibility(View.VISIBLE);
        }

    }

    private void init() {
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
    }

    private void changePassword() {

    }
}