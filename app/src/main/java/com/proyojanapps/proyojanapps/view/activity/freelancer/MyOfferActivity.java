package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentListBinding;
import com.proyojanapps.proyojanapps.databinding.ActivityMyOfferBinding;
import com.proyojanapps.proyojanapps.model.AgentList;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.view.adapter.AgentListAdapter;
import com.proyojanapps.proyojanapps.view.adapter.MyOfferAdapter;
import com.proyojanapps.proyojanapps.viewModel.AgentViewModel;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;

import java.util.ArrayList;

public class MyOfferActivity extends AppCompatActivity {
    private ActivityMyOfferBinding binding;
    private FreelancerViewModel freelancerViewModel;
    private ArrayList<Freelancer.MyOffers> lists;
    private MyOfferAdapter myOfferAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_offer);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });
        init();
        getMyOffers();
    }

    private void getMyOffers() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerViewModel.myOffer().observe(this, myOffers -> {
            dialog.dismiss();
            if (myOffers != null) {
                lists = (ArrayList<Freelancer.MyOffers>) myOffers;
                initRV();
            }
        });
    }

    private void init() {
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
        lists = new ArrayList<>();
    }

    private void initRV() {
        myOfferAdapter = new MyOfferAdapter(this, lists);
        binding.myofferRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.myofferRV.setAdapter(myOfferAdapter);
    }

}