package com.proyojanapps.proyojanapps.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivitySettingsBinding;
import com.proyojanapps.proyojanapps.databinding.FragmentHomeBinding;
import com.proyojanapps.proyojanapps.model.Slider;
import com.proyojanapps.proyojanapps.model.Testimonial;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.adapter.HomeSecondSliderAdapter;
import com.proyojanapps.proyojanapps.view.adapter.SettingsSliderAdapter;
import com.proyojanapps.proyojanapps.view.adapter.SliderAdapter;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;
import com.proyojanapps.proyojanapps.viewModel.SliderViewModel;
import com.proyojanapps.proyojanapps.viewModel.UtilityViewModel;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity {
    private ActivitySettingsBinding binding;
    private SettingsSliderAdapter settingsSliderAdapter;

    private ArrayList<Testimonial> list;
    private UtilityViewModel utilityViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });
        init();
getTestimonialList();
    }

    private void init() {
        list = new ArrayList<>();
        utilityViewModel = ViewModelProviders.of(this).get(UtilityViewModel.class);

    }

    private void getTestimonialList() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        utilityViewModel.getTestimonialList().observe(this, testimonialList -> {
            dialog.dismiss();
            if (testimonialList != null && testimonialList.size() > 0) {
                list= (ArrayList<Testimonial>) testimonialList;
                initImageSlider();
            } else {
                Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void initImageSlider() {
        settingsSliderAdapter = new SettingsSliderAdapter(this, list);
        binding.imageSlider.setSliderAdapter(settingsSliderAdapter);
        binding.imageSlider.setIndicatorAnimation(IndicatorAnimations.WORM);
        binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        binding.imageSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        binding.imageSlider.setScrollTimeInSec(4);
        binding.imageSlider.startAutoCycle();
    }
}