package com.proyojanapps.proyojanapps.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.CustomMemberIncomeLayoutBinding;
import com.proyojanapps.proyojanapps.model.FreelancerIncomeReport;

import java.util.List;


public class IncomeReportAdapter extends RecyclerView.Adapter<IncomeReportAdapter.ViewHolder> {

    private Context context;
    private List<FreelancerIncomeReport.MemberIncome> memberIncomeList;

    public IncomeReportAdapter(Context context, List<FreelancerIncomeReport.MemberIncome> memberIncomeList) {
        this.context = context;
        this.memberIncomeList = memberIncomeList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomMemberIncomeLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.custom_member_income_layout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        FreelancerIncomeReport.MemberIncome memberIncome = memberIncomeList.get(position);

        viewHolder.binding.tvSerial.setText("" + (position + 1));
        viewHolder.binding.tvMemberName.setText("" + memberIncome.getName());
        viewHolder.binding.tvWorkingIncome.setText(" " + String.format("%.2f",memberIncome.getTotalWorkIncome() !=null?
                Double.valueOf(memberIncome.getTotalWorkIncome()):"0")+" "+context.getString(R.string.ta));
        viewHolder.binding.tvReferIncome.setText(" " + String.format("%.2f", memberIncome.getTotalReferIncome()
                !=null? Double.valueOf(memberIncome.getTotalReferIncome()):"0")+" "+context.getString(R.string.ta));
        viewHolder.binding.tvTotal.setText(" " +String.format("%.2f", Double.valueOf(memberIncome.getTotalBalance()))+" "+context.getString(R.string.ta));


        if ((position + 1) % 2 == 0)
            viewHolder.binding.llRow.setBackgroundColor(context.getResources().getColor(R.color.white));
        else {
            viewHolder.binding.llRow.setBackgroundColor(context.getResources().getColor(R.color.off_white));
        }


        viewHolder.binding.llRow.setOnClickListener(view -> {

        });
    }

    @Override
    public int getItemCount() {
        return memberIncomeList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private CustomMemberIncomeLayoutBinding binding;

        public ViewHolder(CustomMemberIncomeLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private void loadUrl(String url) {
//        Intent intent = new Intent(context, WebViewActivity.class).putExtra("url", url);
//        context.startActivity(intent);
    }
}