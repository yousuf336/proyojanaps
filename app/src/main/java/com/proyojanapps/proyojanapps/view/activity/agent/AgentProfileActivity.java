package com.proyojanapps.proyojanapps.view.activity.agent;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentProfileBinding;
import com.proyojanapps.proyojanapps.databinding.DialogWarningBinding;
import com.proyojanapps.proyojanapps.model.Agent;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.FilePath;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.activity.BaseActivity;
import com.proyojanapps.proyojanapps.view.activity.HelpActivity;
import com.proyojanapps.proyojanapps.view.activity.MainActivity;
import com.proyojanapps.proyojanapps.view.activity.SettingsActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.LoginActivity;
import com.proyojanapps.proyojanapps.view.bottomsheet.SelectImageBottomSheet;
import com.proyojanapps.proyojanapps.viewModel.AgentViewModel;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AgentProfileActivity extends BaseActivity implements SelectImageBottomSheet.BottomSheetListener {
    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int REQUEST_SELECT_PHOTO = 2;
    ActivityAgentProfileBinding binding;
    private boolean isBalanceShow = false;
    private Dialog dialogWarning;
    private DialogWarningBinding warningBinding;
    private Freelancer freelancerInfo;
    private Agent agentInfo;
    private MySharedPreparence preparence;
    private FreelancerViewModel freelancerViewModel;
    private SelectImageBottomSheet selectImageBottomSheet;
    private File imageFile;
    private String currentPhotoPath;
    private AgentViewModel agentViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_agent_profile);
        init();
        agentLogin();
        setData();

        binding.llBalance.setOnClickListener(view -> {
            //showBalance();
            startActivity(new Intent(this, AgentBalanceActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        });
        binding.profileImageCIV.setOnClickListener(view -> {
            if (freelancerInfo != null) {
                openSelectImageBottomSheet();
                //showZoomImageDialog(freelancerInfo.getProfilepic());
            }
        });
        binding.btnSendMoney.setOnClickListener(view -> {
            startActivity(new Intent(this, AgentSendMoneyActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.btnCashOutRequest.setOnClickListener(view -> {
            startActivity(new Intent(this, AgentCashoutRequestActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.btnCashIn.setOnClickListener(view -> {
            startActivity(new Intent(this, AgentCashinActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.btnHistory.setOnClickListener(view -> {
            startActivity(new Intent(this, AgentHistoryActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.btnCashout.setOnClickListener(view -> {
            startActivity(new Intent(this, AgentCashoutActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        if (!TextUtils.isEmpty(agentInfo.getAgentStatus()) && agentInfo.getAgentStatus().equals("0")) {
            showWarningDialog();
        } else if (!TextUtils.isEmpty(freelancerInfo.getAccountpay()) && !freelancerInfo.getAccountpay().equals("0")
                && !TextUtils.isEmpty(freelancerInfo.getStatus()) && freelancerInfo.getAccountpay().equals("0")) {
            showWarningDialog();
        }

        binding.tvLogout.setOnClickListener(view -> {
            preparence.setAgentInfo(null);
            startActivity(new Intent(this, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.tvSettings.setOnClickListener(view -> {
            startActivity(new Intent(this, SettingsActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.tvHelp.setOnClickListener(view -> {
            startActivity(new Intent(this, HelpActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
    }

    private void setData() {
        if (agentInfo != null) {
            binding.tvId.setText("ID : " + agentInfo.getAgentReeferId());
            binding.tvName.setText("Name : " + agentInfo.getName());

            if (agentInfo.getProfilepic() != null && !agentInfo.getProfilepic().equals("")) {
                Glide.with(AgentProfileActivity.this).applyDefaultRequestOptions(new RequestOptions()
                        .placeholder(R.drawable.ic_image_circle)).load(agentInfo.getProfilepic())
                        .into(binding.profileImageCIV);
            }
        }
    }

    private void init() {
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
        agentInfo = new Gson().fromJson(preparence.getAgentInfo(), Agent.class);
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
        agentViewModel = ViewModelProviders.of(this).get(AgentViewModel.class);
    }

    private void showBalance() {
        Animation animation = AnimationUtils.loadAnimation(
                this, isBalanceShow == false ? R.anim.slide_out_right : R.anim.slide_out_left
        );
        binding.tvTaka.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Animation animation1 = AnimationUtils.loadAnimation(
                        AgentProfileActivity.this, isBalanceShow == false ? R.anim.slide_out_left_custome_duration : R.anim.slide_out_right_custome_duration
                );
                binding.tvBalance.startAnimation(animation1);

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(100, LinearLayout.LayoutParams.MATCH_PARENT);

                params.gravity = isBalanceShow == false ? Gravity.END : Gravity.START;
                binding.tvTaka.setLayoutParams(params);


                if (!isBalanceShow) {
                    FrameLayout.LayoutParams params1 = new FrameLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    params1.gravity = Gravity.START;
                    params1.rightMargin = 40;
                    params1.leftMargin = 3;

                    binding.tvBalance.setLayoutParams(params1);
                    if (agentInfo != null)
                        if (agentInfo.getAgentBalance() != null)
                            binding.tvBalance.setText("" + agentInfo.getAgentBalance());
                        else {
                            binding.tvBalance.setText("0");
                        }
                } else {
                    FrameLayout.LayoutParams params1 = new FrameLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    params1.gravity = Gravity.END;
                    params1.rightMargin = 4;
                    params1.leftMargin = 40;

                    binding.tvBalance.setLayoutParams(params1);
                    binding.tvBalance.setText("Check Balance");
                }
                isBalanceShow = !isBalanceShow;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void agentLogin() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        agentViewModel.logIn(freelancerInfo.getPhone(), freelancerInfo.getPassword()).observe(this, agent -> {
            dialog.dismiss();
            if (agent != null) {
                String agentStr = new Gson().toJson(agent);
                agentInfo = agent;
                preparence.setAgentInfo(agentStr);
                setData();
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    protected void showWarningDialog() {
        dialogWarning = new Dialog(this);
        dialogWarning.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_warning, null, false);
        dialogWarning.setContentView(warningBinding.getRoot());

        Window window = dialogWarning.getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        if (!TextUtils.isEmpty(agentInfo.getAgentStatus()) && agentInfo.getAgentStatus().equals("0")) {
            warningBinding.tvMessage.setText(getString(R.string.your_account_is_inactive_contact_with_office));
            warningBinding.btnText.setText(getString(R.string.cancel));
            warningBinding.btnWarning.setOnClickListener(view -> {

                dialogWarning.dismiss();
            });
        }
        dialogWarning.show();
    }


    private void openSelectImageBottomSheet() {
        selectImageBottomSheet = new SelectImageBottomSheet(this);
        selectImageBottomSheet.show(getSupportFragmentManager(), "selectImage");
    }

    @Override
    public void onCameraButtonClicked(int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1030);
            }
        } else {
            dispatchTakePictureIntent();
        }
        selectImageBottomSheet.dismiss();
    }

    @Override
    public void onGalleryButtonClicked(int requestCode) {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(Intent.createChooser(galleryIntent, "Select Photo"), REQUEST_SELECT_PHOTO);
        selectImageBottomSheet.dismiss();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            CropImage.activity(Uri.fromFile(new File(currentPhotoPath)))
                    .setAspectRatio(1, 1)
                    .start(this);
        } else if (requestCode == REQUEST_SELECT_PHOTO && resultCode == RESULT_OK && data != null) {
            Uri imageUrl = data.getData();
            CropImage.activity(imageUrl)
                    .setAspectRatio(1, 1)
                    .start(this);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Uri resultUri = null;
            if (result != null) {
                resultUri = result.getUri();
            }
            getImageFile(FilePath.getPath(this, resultUri));

            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (bitmap != null) {
                setImageToView(bitmap);
            }
            changeProfilePic();
        }
    }

    private void getImageFile(String photoPath) {
        imageFile = new File(photoPath);
    }

    private void setImageToView(Bitmap bitmap) {
        binding.profileImageCIV.setImageBitmap(bitmap);
    }

    private void changeProfilePic() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        if (imageFile != null) {
            freelancerViewModel.changeProfilePic(freelancerInfo.getId(), imageFile).observe(this, new Observer<String>() {
                @Override
                public void onChanged(String s) {
                    dialog.dismiss();
                    if (s != null && s.equals(StaticKey.SUCCESS)) {
                        agentLogin();
                    } else {
                        Toast.makeText(AgentProfileActivity.this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                }
            });


        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ignored) {
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.proyojanapps.proyojanapps.customerfileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }
}