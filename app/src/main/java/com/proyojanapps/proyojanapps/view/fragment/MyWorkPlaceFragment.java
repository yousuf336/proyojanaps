package com.proyojanapps.proyojanapps.view.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.DialogWarningBinding;
import com.proyojanapps.proyojanapps.databinding.FragmentMyWorkPlaceBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.Slider;
import com.proyojanapps.proyojanapps.model.Workplace;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.activity.freelancer.IncomePlansActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.IncomeReportActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.MyNetworkActivity;
import com.proyojanapps.proyojanapps.view.activity.WebViewActivity;
import com.proyojanapps.proyojanapps.view.activity.agent.AgentListActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.UpdateWorkTimeActivity;
import com.proyojanapps.proyojanapps.view.adapter.SliderAdapter;
import com.proyojanapps.proyojanapps.viewModel.FreelancerActivitesViewModel;
import com.proyojanapps.proyojanapps.viewModel.SliderViewModel;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;


public class MyWorkPlaceFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentMyWorkPlaceBinding binding;
    private SliderAdapter sliderAdapter;
    private FreelancerActivitesViewModel freelancerActivitesViewModel;
    private ArrayList<Slider> sliderList;
    private SliderViewModel sliderViewModel;
    private Freelancer freelancerInfo;
    private MySharedPreparence preparence;
    private Workplace workplace;

    public MyWorkPlaceFragment() {
        // Required empty public constructor
    }

    public static MyWorkPlaceFragment newInstance(String param1, String param2) {
        MyWorkPlaceFragment fragment = new MyWorkPlaceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_work_place, container, false);
        init();
        getSliderImageList();
        getworkInfo();
        setTopHeaderData();


        return binding.getRoot();
    }

    private void setData() {
        if (workplace != null) {
            binding.tvLeftWorkDays.setText("You Have " + workplace.getWorkRenewDay() + " Days Left To Work");
            binding.tvDays.setText("" + workplace.getWorkRenewDay() + " days");
            binding.tvPrimaryBalance.setText(": " + workplace.getPrimaryIncome() + " " + getString(R.string.ta));
            binding.tvSecondaryBalance.setText(": " + workplace.getSecondaryIncome() + " " + getString(R.string.ta));
            binding.tvWithdrawalBalance.setText(": " + workplace.getWithdrawIncome() + " " + getString(R.string.ta));
            binding.tvCommissionBalance.setText(": " + workplace.getCommissionIncome() + " " + getString(R.string.ta));

            binding.includeStartWork.tvService.setOnClickListener(view -> {
                if (workplace.getWorkRenewDay() > 0) {
                    String str1 = workplace.getStartWork().replaceFirst("%s", "" + freelancerInfo.getPhone());
                    String str2 = str1.replace("%s", "" + freelancerInfo.getPassword());
                    Log.e("url", "url : " + str2);
                    startActivity(new Intent(getActivity(), WebViewActivity.class)
                            .putExtra("url", str2));
                } else {
                    Toast.makeText(getActivity(), "" + getString(R.string.please_renew_your_account), Toast.LENGTH_SHORT).show();
                }
            });


            binding.includeTransferToWallet.tvService.setOnClickListener(view -> {
                if (workplace.getWithdrawIncome() > 0) {
                    withdrawBalance();
                    //  startActivity(new Intent(getActivity(), MyNetworkActivity.class));
                } else {
                    Toast.makeText(getActivity(), "" + getString(R.string.insufficient_balance), Toast.LENGTH_SHORT).show();
                }
            });


            binding.rlLeftWorkingDays.setOnClickListener(view -> {
                if (workplace.getWorkRenewDay() <= 0) {
                    //  startActivity(new Intent(getActivity(), MyNetworkActivity.class));
                    if (freelancerInfo.getTotalBalance() >= workplace.getWorkRenew()) {
                        showWarningDialog();
                    } else {
                        Toast.makeText(getActivity(), "" + getString(R.string.insufficient_balance), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getActivity(), "You Have " + workplace.getWorkRenewDay() + " Days Left To Work", Toast.LENGTH_SHORT).show();
                }
            });


            binding.includeAgentList.tvService.setOnClickListener(view -> {
                startActivity(new Intent(getActivity(), AgentListActivity.class));
            });
            binding.includeIncomePlans.tvService.setOnClickListener(view -> {
                startActivity(new Intent(getActivity(), IncomePlansActivity.class));
            });

            binding.includeIncomeReport.tvService.setOnClickListener(view -> {
                startActivity(new Intent(getActivity(), IncomeReportActivity.class));
            });

            binding.includeMyNetwork.tvService.setOnClickListener(view -> {

                startActivity(new Intent(getActivity(), MyNetworkActivity.class));
            });

        }
    }


    private void init() {
        sliderList = new ArrayList<>();
        sliderViewModel = ViewModelProviders.of((AppCompatActivity) getActivity()).get(SliderViewModel.class);
        freelancerActivitesViewModel = ViewModelProviders.of((AppCompatActivity) getActivity())
                .get(FreelancerActivitesViewModel.class);
        preparence = new MySharedPreparence(getActivity());
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
    }


    private void setTopHeaderData() {
        binding.includeStartWork.tvService.setText("" + getString(R.string.start_work));
        Drawable img = getContext().getResources().getDrawable(R.drawable.ic_start_work);
        binding.includeStartWork.tvService.setCompoundDrawablesWithIntrinsicBounds(null, img, null, null);

        binding.includeTransferToWallet.tvService.setText("" + getString(R.string.transfer_to_wallet));
        binding.includeTransferToWallet.tvService.setCompoundDrawablesWithIntrinsicBounds(null,
                getContext().getResources().getDrawable(R.drawable.ic_wallet_solid), null, null);

        binding.includeMyNetwork.tvService.setText("" + getString(R.string.my_network));
        binding.includeMyNetwork.tvService.setCompoundDrawablesWithIntrinsicBounds(null,
                getContext().getResources().getDrawable(R.drawable.ic_baseline_my_network_24), null, null);

        binding.includeIncomeReport.tvService.setText("" + getString(R.string.income_report));
        binding.includeIncomeReport.tvService.setCompoundDrawablesWithIntrinsicBounds(null,
                getContext().getResources().getDrawable(R.drawable.ic_baseline_income_report_24), null, null);

        binding.includeIncomePlans.tvService.setText("" + getString(R.string.income_plans));
        binding.includeIncomePlans.tvService.setCompoundDrawablesWithIntrinsicBounds(null,
                getContext().getResources().getDrawable(R.drawable.ic_start_work), null, null);

        binding.includeAgentList.tvService.setText("" + getString(R.string.agent_list));
        binding.includeAgentList.tvService.setCompoundDrawablesWithIntrinsicBounds(null,
                getContext().getResources().getDrawable(R.drawable.ic_baseline_agent_24), null, null);


    }

    private void initImageSlider() {
        sliderAdapter = new SliderAdapter(getActivity(), sliderList);
        binding.imageSlider.setSliderAdapter(sliderAdapter);
        binding.imageSlider.setIndicatorAnimation(IndicatorAnimations.WORM);
        binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        binding.imageSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        binding.imageSlider.setScrollTimeInSec(4);
        binding.imageSlider.startAutoCycle();
    }


    private void getSliderImageList() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(getActivity());
        sliderViewModel.getSliderList().observe(this, sliders -> {
            dialog.dismiss();
            if (sliders != null && sliders.size() > 0) {

                sliderList = (ArrayList<Slider>) sliders;
                //Toast.makeText(getActivity(), "sliders : "+sliderList.size(), Toast.LENGTH_SHORT).show();
                binding.imageSlider.setVisibility(View.VISIBLE);
                initImageSlider();
            } else {
                // Toast.makeText(getActivity(), "sliders : null", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getworkInfo() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(getActivity());
        freelancerActivitesViewModel.myWorkPlace(freelancerInfo.getId()).observe(this, workplace -> {
            dialog.dismiss();
            if (workplace != null) {
                this.workplace = workplace;
                //Toast.makeText(getActivity(), "sliders : "+sliderList.size(), Toast.LENGTH_SHORT).show();
                setData();
            } else {
                // Toast.makeText(getActivity(), "sliders : null", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void withdrawBalance() {
        startActivity(new Intent(getActivity(), UpdateWorkTimeActivity.class)
                .putExtra("amount", workplace.getWithdrawIncome())
                .putExtra("identity", StaticKey.WITHDRAW_BALANCE));
    }

    protected void showWarningDialog() {
        Dialog dialogWarning = new Dialog(getActivity());
        dialogWarning.requestWindowFeature(Window.FEATURE_NO_TITLE);
        DialogWarningBinding warningBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.dialog_warning, null, false);
        dialogWarning.setContentView(warningBinding.getRoot());

        warningBinding.tvMessage.setText("" + getString(R.string.your_worktime_limit_is_over_please_update_your_work_time));
        warningBinding.btnText.setText("" + getString(R.string.update));

        Window window = dialogWarning.getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        warningBinding.btnWarning.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), UpdateWorkTimeActivity.class)
                    .putExtra("amount", workplace.getWorkRenew())
                    .putExtra("identity", StaticKey.RENEW_BALANCE));
            dialogWarning.dismiss();
        });
        dialogWarning.show();
    }


}