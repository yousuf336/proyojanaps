package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityConfirmTransactionBinding;
import com.proyojanapps.proyojanapps.databinding.ActivityFreelancerHistoryBinding;
import com.proyojanapps.proyojanapps.model.AgentList;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.view.adapter.AgentListAdapter;
import com.proyojanapps.proyojanapps.view.adapter.FreelancerHistoryAdapter;
import com.proyojanapps.proyojanapps.viewModel.AgentViewModel;
import com.proyojanapps.proyojanapps.viewModel.FreelancerActivitesViewModel;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;

import java.util.ArrayList;

public class FreelancerHistoryActivity extends AppCompatActivity {
    private ActivityFreelancerHistoryBinding binding;
    private FreelancerViewModel freelancerViewModel;
    private MySharedPreparence preparence;
    private Freelancer freelancerInfo;
    private ArrayList<Freelancer.History> lists;
    private FreelancerHistoryAdapter freelancerHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_freelancer_history);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });
        init();
        getAgentList();
    }

    private void getAgentList() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerViewModel.history(freelancerInfo.getId()).observe(this, historys -> {
            dialog.dismiss();
            lists = (ArrayList<Freelancer.History>) historys;
            initRV();
        });
    }

    private void initRV() {
        freelancerHistoryAdapter = new FreelancerHistoryAdapter(this, lists);
        binding.historyRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.historyRV.setAdapter(freelancerHistoryAdapter);
    }

    private void init() {
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
        lists = new ArrayList<>();
    }
}