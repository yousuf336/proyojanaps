package com.proyojanapps.proyojanapps.view.activity.agent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentLoginBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.view.activity.BaseActivity;
import com.proyojanapps.proyojanapps.viewModel.AgentViewModel;

public class AgentLoginActivity extends BaseActivity {

    private MySharedPreparence preferences;
    private AgentViewModel agentViewModel;
    ActivityAgentLoginBinding binding;
    private Freelancer freelancerInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_agent_login);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });

        init();
        if (freelancerInfo != null)
            binding.etAgentId.setText(freelancerInfo.getPhone());
//        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });

        binding.tvRegistration.setOnClickListener(view -> {
            startActivity(new Intent(AgentLoginActivity.this, AgentSignUpActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        binding.btnLogin.setOnClickListener(view -> {
            agentLogin();
        });

    }

    private void agentLogin() {
        String password = binding.etPassword.getText().toString().trim();
        String agentId = binding.etAgentId.getText().toString().trim();
        if (!TextUtils.isEmpty(password)) {
            final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
            agentViewModel.logIn(agentId, password).observe(this, agent -> {
                dialog.dismiss();
                if (agent != null) {
                    String agentStr = new Gson().toJson(agent);
                    preferences.setAgentInfo(agentStr);

                    startActivity(new Intent(AgentLoginActivity.this, AgentProfileActivity.class));
                    //  .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
        }
        if (TextUtils.isEmpty(binding.etPassword.getText())) {
            binding.etPassword.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
            binding.etPassword.setFocusable(true);
        }
    }

    private void init() {
        agentViewModel = ViewModelProviders.of(this).get(AgentViewModel.class);
        preferences = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preferences.getFreelancerInfo(), Freelancer.class);
    }
}