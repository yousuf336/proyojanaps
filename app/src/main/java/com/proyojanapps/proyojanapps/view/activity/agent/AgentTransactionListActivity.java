package com.proyojanapps.proyojanapps.view.activity.agent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentCashoutRequestBinding;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentTransactionListBinding;
import com.proyojanapps.proyojanapps.model.Agent;
import com.proyojanapps.proyojanapps.model.Transaction;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.adapter.AgentCashinAdapter;
import com.proyojanapps.proyojanapps.view.adapter.AgentCashoutAdapter;
import com.proyojanapps.proyojanapps.view.adapter.AgentSendMoneyAdapter;
import com.proyojanapps.proyojanapps.view.adapter.CashoutRequestListAdapter;
import com.proyojanapps.proyojanapps.viewModel.AgentActivitesViewModel;

import java.util.ArrayList;

public class AgentTransactionListActivity extends AppCompatActivity {

    private ActivityAgentTransactionListBinding binding;
    private AgentActivitesViewModel agentActivitesViewModel;
    private MySharedPreparence preparence;
    private Agent agentInfo;
    private ArrayList<Transaction.AgentCashOut> agentCashOutArrayList;
    private ArrayList<Transaction.AgentSendMoney> agentSendMoneyArrayList;
    private ArrayList<Transaction.AgentCashin> agentCashinArrayList;
    private AgentCashoutAdapter agentCashoutAdapter;
    private AgentCashinAdapter agentCashinAdapter;
    private AgentSendMoneyAdapter agentSendMoneyAdapter;
    private String identity = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_agent_transaction_list);

        init();
        retriveData();

        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        if (identity != null && identity.equals(StaticKey.SEND_MONEY)) {
            binding.tvTitle.setText("" + getString(R.string.send_money));
            sendMoneyList();
        } else if (identity != null && identity.equals(StaticKey.CASH_OUT_AGENT)) {
            binding.tvTitle.setText("" + getString(R.string.cash_out));
            cashOutList();
        } else if (identity != null && identity.equals(StaticKey.CASH_IN_AGENT)) {
            binding.tvTitle.setText("" + getString(R.string.cash_in));
            cashinList();
        }

    }

    private void retriveData() {
        identity = getIntent().getStringExtra("identity");
    }

    private void init() {
        agentCashOutArrayList = new ArrayList<>();
        agentCashinArrayList = new ArrayList<>();
        agentSendMoneyArrayList = new ArrayList<>();
        agentCashOutArrayList = new ArrayList<>();
        agentActivitesViewModel = ViewModelProviders.of(this).get(AgentActivitesViewModel.class);
        preparence = new MySharedPreparence(this);
        agentInfo = new Gson().fromJson(preparence.getAgentInfo(), Agent.class);
    }

    private void initCashoutRV() {
        agentCashoutAdapter = new AgentCashoutAdapter(this, agentCashOutArrayList);
        binding.cashoutListRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.cashoutListRV.setAdapter(agentCashoutAdapter);
    }

    private void initcashInRV() {
        agentCashinAdapter = new AgentCashinAdapter(this, agentCashinArrayList);
        binding.cashoutListRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.cashoutListRV.setAdapter(agentCashinAdapter);
    }

    private void initsendMoneyRV() {
        agentSendMoneyAdapter = new AgentSendMoneyAdapter(this, agentSendMoneyArrayList);
        binding.cashoutListRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.cashoutListRV.setAdapter(agentSendMoneyAdapter);
    }

    private void cashOutList() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        agentActivitesViewModel.cashOutList(agentInfo.getAgentId()).observe(this,
                cashOutList -> {
                    dialog.dismiss();
                    if (cashOutList != null) {
                        this.agentCashOutArrayList = (ArrayList<Transaction.AgentCashOut>) cashOutList;
                        initCashoutRV();
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void cashinList() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        agentActivitesViewModel.cashinList(agentInfo.getAgentId()).observe(this,
                cashinList -> {
                    dialog.dismiss();
                    if (cashinList != null) {
                        this.agentCashinArrayList = (ArrayList<Transaction.AgentCashin>) cashinList;
                        initcashInRV();
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void sendMoneyList() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        agentActivitesViewModel.sendMoneytList(agentInfo.getAgentId()).observe(this,
                sendMoneyList -> {
                    dialog.dismiss();
                    if (sendMoneyList != null) {
                        this.agentSendMoneyArrayList = (ArrayList<Transaction.AgentSendMoney>) sendMoneyList;
                        initsendMoneyRV();
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });

    }
}