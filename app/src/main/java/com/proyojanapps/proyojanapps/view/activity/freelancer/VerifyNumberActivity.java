package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityVerifyNumberBinding;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.view.activity.BaseActivity;
import com.proyojanapps.proyojanapps.view.activity.agent.AgentLoginActivity;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;

public class VerifyNumberActivity extends BaseActivity {

    ActivityVerifyNumberBinding binding;
    private int counter = 1;
    private String verificationCode, number, identity;
    private int id;
    FreelancerViewModel freelancerViewModel;
    private MySharedPreparence preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_verify_number);
        binding.btnVerify.setOnClickListener(
                view -> startActivity(new Intent(VerifyNumberActivity.this, AgentLoginActivity.class)));
        init();
        binding.btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String first = binding.etFirst.getText().toString().trim();
                String second = binding.etSecond.getText().toString().trim();
                String third = binding.etThird.getText().toString().trim();
                String fourth = binding.etFourth.getText().toString().trim();

                if (!TextUtils.isEmpty(first) && !TextUtils.isEmpty(second) && !TextUtils.isEmpty(third) && !TextUtils.isEmpty(fourth)) {
                    String str = first + second + third + fourth;
                    if (str.equals(verificationCode)) {
                        if (identity != null && identity.equals("forgetpassword")) {
                            startActivity(new Intent(VerifyNumberActivity.this, PasswordChangeActivity.class)
                                    .putExtra("id", String.valueOf(id)));
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            finish();
                        } else {
                            submitSecurityPin();
                        }
                    } else {
                        Toast.makeText(VerifyNumberActivity.this, "" + getString(R.string.invalid_code), Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });


        binding.tvChangeNumber.setOnClickListener(view -> {
            showCustomeDialog(this);
            dialogCustomeBinding.tvTitle.setText(getResources().getString(R.string.change_number) + " ?");
            dialogCustomeBinding.tvHeading.setText(getResources().getString(R.string.enter_your_phone_number_here));
            dialogCustomeBinding.etNumber.setHint(getString(R.string.phone));
            dialogCustomeBinding.btnNext.setText(getString(R.string.send_otp));
            dialogCustomeBinding.btnNext.setOnClickListener(view1 -> {
                if (!TextUtils.isEmpty(dialogCustomeBinding.etNumber.getText().toString().trim())) {
                    final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
                    freelancerViewModel.changeNumber(id, dialogCustomeBinding.etNumber.getText().toString().trim())
                            .observe(this, freelancer -> {
                                dialog.dismiss();
                                if (freelancer != null) {
                                    if (!TextUtils.isEmpty(freelancer.getMessage()) && freelancer.getMessage().equals("success")) {
                                        verificationCode = freelancer.getSecurityPin();
                                        number = dialogCustomeBinding.etNumber.getText().toString().trim();
                                        if (number != null)
                                            binding.tvPhone.setText(number);
                                    }
                                } else {
                                    Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                }
                            });
                    dialogCustome.dismiss();
                } else {
                    dialogCustomeBinding.etNumber.setFocusable(true);
                    dialogCustomeBinding.etNumber.setError("Enter Your Phone Number");

                }


            });

        });

        binding.tvResend.setOnClickListener(view -> {
            resendCode();
        });

    }

    private void resendCode() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerViewModel.resendCode(id).observe(this, freelancer -> {
            if (freelancer != null) {
                if (!TextUtils.isEmpty(freelancer.getMessage()) && freelancer.getMessage().equals("success")) {
                    id = freelancer.getId();
                    verificationCode = freelancer.getSecurityPin();
                }
            }
            dialog.dismiss();

        });
    }

    private void submitSecurityPin() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerViewModel.securityPinSubmition(id, verificationCode).observe(this, message -> {
            dialog.dismiss();
            if (message != null) {
                if (!TextUtils.isEmpty(message) && message.equals("success")) {
                    startActivity(new Intent(VerifyNumberActivity.this, LoginActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
            }


        });
    }

    private void init() {
        preferences = new MySharedPreparence(this);
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
        verificationCode = getIntent().getStringExtra("code");
        number = getIntent().getStringExtra("number");
        identity = getIntent().getStringExtra("identity");
        id = getIntent().getIntExtra("id", 0);
        if (number != null)
            binding.tvPhone.setText(number);

        if (identity != null && identity.equals("forgetpassword")) {
            binding.tvChangeNumber.setVisibility(View.GONE);
        } else if (identity != null && identity.equals("phoneverify")) {
            resendCode();
        } else {
            binding.tvChangeNumber.setVisibility(View.VISIBLE);
        }
        binding.etFirst.requestFocus();

        TextWatcher textWatcher1 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    binding.etSecond.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        TextWatcher textWatcher2 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    binding.etThird.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        TextWatcher textWatcher3 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {

                    binding.etFourth.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        TextWatcher textWatcher4 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    binding.etFourth.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        binding.etFirst.addTextChangedListener(textWatcher1);
        binding.etSecond.addTextChangedListener(textWatcher2);
        binding.etThird.addTextChangedListener(textWatcher3);
        binding.etFourth.addTextChangedListener(textWatcher4);

    }

}