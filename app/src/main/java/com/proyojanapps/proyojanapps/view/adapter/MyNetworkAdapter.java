package com.proyojanapps.proyojanapps.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.CustomeRowBinding;
import com.proyojanapps.proyojanapps.databinding.MyNetworkMemberLayoutBinding;
import com.proyojanapps.proyojanapps.model.FreelancerNetwork;
import com.proyojanapps.proyojanapps.model.IncomePlans;

import java.util.List;


public class MyNetworkAdapter extends RecyclerView.Adapter<MyNetworkAdapter.ViewHolder> {

    private Context context;
    private List<FreelancerNetwork> myNetworkList;

    public MyNetworkAdapter(Context context, List<FreelancerNetwork> myNetworkList) {
        this.context = context;
        this.myNetworkList = myNetworkList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MyNetworkMemberLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.my_network_member_layout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        FreelancerNetwork freelancerNetwork = myNetworkList.get(position);

        viewHolder.binding.tvName.setText(freelancerNetwork.getName());
        viewHolder.binding.tvNumber.setText(freelancerNetwork.getPhone());
        viewHolder.binding.tvOwnerRefer.setText("Own Refer : "+freelancerNetwork.getOwnreferid());
        viewHolder.binding.tvRefer.setText("Refer : "+freelancerNetwork.getReferid());
        viewHolder.binding.tvStatus.setText("Status : "+freelancerNetwork.getStatus());
        viewHolder.binding.tvActivationDate.setText("Activation Date : "+freelancerNetwork.getCreated_at());


        viewHolder.binding.llRow.setOnClickListener(view -> {

        });
    }

    @Override
    public int getItemCount() {
        return myNetworkList.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder {

        private MyNetworkMemberLayoutBinding binding;

        public ViewHolder(MyNetworkMemberLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private void loadUrl(String url) {
//        Intent intent = new Intent(context, WebViewActivity.class).putExtra("url", url);
//        context.startActivity(intent);
    }
}