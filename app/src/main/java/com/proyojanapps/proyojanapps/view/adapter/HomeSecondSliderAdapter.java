package com.proyojanapps.proyojanapps.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ModelImageAndTextSliderItemBinding;
import com.proyojanapps.proyojanapps.databinding.ModelImageSliderItemBinding;
import com.proyojanapps.proyojanapps.model.Slider;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.List;


public class HomeSecondSliderAdapter extends SliderViewAdapter<HomeSecondSliderAdapter.ViewHolder> {

    private Context context;
    private List<Slider> sliderList;

    public HomeSecondSliderAdapter(Context context, List<Slider> sliderList) {
        this.context = context;
        this.sliderList = sliderList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        ModelImageAndTextSliderItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.model_image_and_text_slider_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Slider slider = sliderList.get(position);

        Glide.with(context).applyDefaultRequestOptions(new RequestOptions()
                .placeholder(R.drawable.ic_image_black_24dp)).load(RetrofitInstance.BASE_URL+"/" + slider.getSliderImage()).into(viewHolder.binding.bannerImageIV);


        viewHolder.binding.bannerImageIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(context, "" + position, Toast.LENGTH_SHORT).show();
                   // loadUrl(slider.getWeb_link());

            }
        });
    }

    @Override
    public int getCount() {
        return sliderList.size();
    }

    class ViewHolder extends SliderViewAdapter.ViewHolder {

        private ModelImageAndTextSliderItemBinding binding;

        public ViewHolder(ModelImageAndTextSliderItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
    private void loadUrl(String url) {
//        Intent intent = new Intent(context, WebViewActivity.class).putExtra("url", url);
//        context.startActivity(intent);
    }
}