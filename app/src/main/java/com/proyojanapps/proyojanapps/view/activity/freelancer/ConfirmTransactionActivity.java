package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.animation.Animator;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityConfirmTransactionBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.activity.BaseActivity;
import com.proyojanapps.proyojanapps.view.activity.MainActivity;
import com.proyojanapps.proyojanapps.viewModel.FreelancerActivitesViewModel;

public class ConfirmTransactionActivity extends BaseActivity {
    private ActivityConfirmTransactionBinding binding;
    private FreelancerActivitesViewModel freelancerActivitesViewModel;
    private MySharedPreparence preparence;
    private Freelancer freelancerInfo;
    private String identity, amount, agentId, referId, withdrawableamount;
    private double newBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_confirm_transaction);

        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        binding.llConfirm.setOnClickListener(view -> {
            if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.CASH_OUT)) {
                if (newBalance >= 0) {
                    cashOut();
                } else {
                    Toast.makeText(this, "" + getString(R.string.insufficient_balance), Toast.LENGTH_SHORT).show();
                }

            } else if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.TRANSFER)) {
                if (newBalance >= 0) {
                    transfer();
                } else {
                    Toast.makeText(this, "" + getString(R.string.insufficient_balance), Toast.LENGTH_SHORT).show();
                }

            } else if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.RENEW_BALANCE)) {
                if (newBalance >= 0) {
                    workingRenew();
                } else {
                    Toast.makeText(this, "" + getString(R.string.insufficient_balance), Toast.LENGTH_SHORT).show();
                }

            } else if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.WITHDRAW_BALANCE)) {
                if (newBalance >= 0) {
                    withdrawBalance();
                } else {
                    Toast.makeText(this, "" + getString(R.string.insufficient_balance), Toast.LENGTH_SHORT).show();
                }
            }

        });
        init();
        retrieveData();
        setData();


    }

    private void setData() {
        if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.CASH_OUT)) {
            binding.tvAgentId.setText("" + agentId);
        } else if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.TRANSFER)) {
            binding.tvAgentId.setText("" + referId);
            binding.tvCashoutBalanceHeading.setText("Transfer amount");
        } else if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.RENEW_BALANCE)) {
            binding.tvAgentId.setText("Send to proyojan");
            binding.tvCashoutBalanceHeading.setText("Renew amount");
        } else if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.WITHDRAW_BALANCE)) {
            binding.tvAgentId.setText("Transfer To Wallet");
            binding.tvCashoutBalanceHeading.setText("Transfer amount");
        }

        binding.tvCashoutBalance.setText(amount + " " + getString(R.string.ta));
        try {
            if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.WITHDRAW_BALANCE)) {
                newBalance = Double.valueOf(withdrawableamount) - Double.valueOf(amount);
                binding.tvNewBalance.setText("" + (newBalance >= 0 ? newBalance : "0") + " " + getString(R.string.ta));
            } else {
                newBalance = freelancerInfo.getTotalBalance() - Double.valueOf(amount);
                binding.tvNewBalance.setText("" + (newBalance > 0 ? newBalance : "0") + " " + getString(R.string.ta));
            }

        } catch (Exception e) {

        }

    }

    private void cashOut() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerActivitesViewModel.cashOut(freelancerInfo.getId(), agentId, 1, amount).observe(this,
                message -> {
                    dialog.dismiss();
                    if (message != null) {
                        if (message.equals(StaticKey.SUCCESS)) {
                            showSuccessDialog(this);
                            dialogSuccessBinding.animationViewOnline.addAnimatorListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {
                                    // Toast.makeText(context, "onAnimationStart", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {
                                    // Toast.makeText(context, "onAnimationEnd", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {
                                    // Toast.makeText(context, "onAnimationCancel", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {
                                    // Toast.makeText(context, "onAnimationRepeat", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(ConfirmTransactionActivity.this, MainActivity.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    dialogSuccess.cancel();
                                }
                            });

                        } else if (message.equals(StaticKey.INSUFFICIENT_BALANCE)) {
                            Toast.makeText(this, "" + getString(R.string.insufficient_balance), Toast.LENGTH_SHORT).show();
                        } else if (message.equals(StaticKey.REFFER_ID_NOT_VALID)) {
                            Toast.makeText(this, "" + getString(R.string.refer_id_invalid), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void transfer() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerActivitesViewModel.transferAmount(freelancerInfo.getId(), referId, 1, amount).observe(this,
                message -> {
                    dialog.dismiss();
                    if (message != null) {
                        if (message.equals(StaticKey.SUCCESS)) {
                            showSuccessDialog(this);
                            dialogSuccessBinding.animationViewOnline.addAnimatorListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {
                                    // Toast.makeText(context, "onAnimationStart", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {
                                    // Toast.makeText(context, "onAnimationEnd", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {
                                    // Toast.makeText(context, "onAnimationCancel", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {
                                    // Toast.makeText(context, "onAnimationRepeat", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(ConfirmTransactionActivity.this, MainActivity.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    dialogSuccess.cancel();
                                }
                            });

                        } else if (message.equals(StaticKey.INSUFFICIENT_BALANCE)) {
                            Toast.makeText(this, "" + getString(R.string.insufficient_balance), Toast.LENGTH_SHORT).show();
                        } else if (message.equals(StaticKey.REFFER_ID_NOT_VALID)) {
                            Toast.makeText(this, "" + getString(R.string.refer_id_invalid), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void workingRenew() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerActivitesViewModel.workingRenew(freelancerInfo.getId()).observe(this,
                message -> {
                    dialog.dismiss();
                    if (message != null) {
                        if (message.equals(StaticKey.SUCCESS)) {
                            showSuccessDialog(this);
                            dialogSuccessBinding.animationViewOnline.addAnimatorListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {
                                    // Toast.makeText(context, "onAnimationStart", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {
                                    // Toast.makeText(context, "onAnimationEnd", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {
                                    // Toast.makeText(context, "onAnimationCancel", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {
                                    // Toast.makeText(context, "onAnimationRepeat", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(ConfirmTransactionActivity.this, MainActivity.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    dialogSuccess.cancel();
                                }
                            });

                        } else if (message.equals(StaticKey.INSUFFICIENT_BALANCE)) {
                            Toast.makeText(this, "" + getString(R.string.insufficient_balance), Toast.LENGTH_SHORT).show();
                        } else if (message.equals(StaticKey.REFFER_ID_NOT_VALID)) {
                            Toast.makeText(this, "" + getString(R.string.refer_id_invalid), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void withdrawBalance() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerActivitesViewModel.withdrawBalance(freelancerInfo.getId()).observe(this,
                message -> {
                    dialog.dismiss();
                    if (message != null) {
                        if (message.equals(StaticKey.SUCCESS)) {
                            showSuccessDialog(this);
                            dialogSuccessBinding.animationViewOnline.addAnimatorListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {
                                    // Toast.makeText(context, "onAnimationStart", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {
                                    // Toast.makeText(context, "onAnimationEnd", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {
                                    // Toast.makeText(context, "onAnimationCancel", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {
                                    // Toast.makeText(context, "onAnimationRepeat", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(ConfirmTransactionActivity.this, MainActivity.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    dialogSuccess.cancel();
                                }
                            });

                        } else if (message.equals(StaticKey.INSUFFICIENT_BALANCE)) {
                            Toast.makeText(this, "" + getString(R.string.insufficient_balance), Toast.LENGTH_SHORT).show();
                        } else if (message.equals(StaticKey.REFFER_ID_NOT_VALID)) {
                            Toast.makeText(this, "" + getString(R.string.refer_id_invalid), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void retrieveData() {
        referId = getIntent().getStringExtra("referId");
        agentId = getIntent().getStringExtra("agentId");
        amount = getIntent().getStringExtra("amount");
        withdrawableamount = getIntent().getStringExtra("withdrawableamount");
        identity = getIntent().getStringExtra("identity");

    }

    private void init() {
        freelancerActivitesViewModel = ViewModelProviders.of(this).get(FreelancerActivitesViewModel.class);
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
    }
}