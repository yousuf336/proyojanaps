package com.proyojanapps.proyojanapps.view.activity.agent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentSendMoneyBinding;
import com.proyojanapps.proyojanapps.model.Agent;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.activity.BaseActivity;
import com.proyojanapps.proyojanapps.view.activity.agent.AgentConfirmTransectionActivity;
import com.proyojanapps.proyojanapps.viewModel.AgentActivitesViewModel;
import com.proyojanapps.proyojanapps.viewModel.AgentViewModel;

public class AgentSendMoneyActivity extends BaseActivity {
    private ActivityAgentSendMoneyBinding binding;
    private AgentActivitesViewModel agentActivitesViewModel;
    private AgentViewModel agentViewModel;
    private MySharedPreparence preparence;
    private Freelancer freelancerInfo;
    private Agent agentInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_agent_send_money);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        init();
        binding.btnSubmit.setOnClickListener(view -> {
            checkInput();
        });

    }

    private void checkInput() {
        String referId = binding.etReferId.getText().toString();
        String amount = binding.etAmount.getText().toString();
        String password = binding.etPassword.getText().toString();

        if (!TextUtils.isEmpty(referId) && !TextUtils.isEmpty(amount) && !TextUtils.isEmpty(password)) {
            if (freelancerInfo != null && freelancerInfo.getPassword() != null && freelancerInfo.getPassword().equals(password)) {
                agentLogin(referId, amount, password);
            } else {
                binding.etPassword.setFocusable(true);
                binding.etPassword.setError("" + getString(R.string.password_mismatch));
            }
        } else {
            if (TextUtils.isEmpty(binding.etReferId.getText())) {
                binding.etReferId.setFocusable(true);
                binding.etReferId.setError("" + getString(R.string.required_field_cannot_be_empty));
            }
            if (TextUtils.isEmpty(binding.etPassword.getText())) {
                binding.etPassword.setFocusable(true);
                binding.etPassword.setError("" + getString(R.string.required_field_cannot_be_empty));
            }
            if (TextUtils.isEmpty(binding.etAmount.getText())) {
                binding.etAmount.setFocusable(true);
                binding.etAmount.setError("" + getString(R.string.required_field_cannot_be_empty));
            }
            Toast.makeText(this, getResources().getString(R.string.need_required_information), Toast.LENGTH_SHORT).show();
        }


    }

    private void init() {
        agentActivitesViewModel = ViewModelProviders.of(this).get(AgentActivitesViewModel.class);
        agentViewModel = ViewModelProviders.of(this).get(AgentViewModel.class);
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
        agentInfo = new Gson().fromJson(preparence.getAgentInfo(), Agent.class);
    }

    private void agentLogin(String referId, String amount, String password) {
        if (agentInfo != null) {
            final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
            agentViewModel.logIn(freelancerInfo.getPhone(), password).observe(this, agent -> {
                dialog.dismiss();
                if (agent != null) {
                    String agentStr = new Gson().toJson(agent);
                    preparence.setAgentInfo(agentStr);
                    startActivity(new Intent(this, AgentConfirmTransectionActivity.class)
                            .putExtra("amount", amount)
                            .putExtra("password", password)
                            .putExtra("referId", referId)
                            .putExtra("identity", StaticKey.SEND_MONEY));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                } else {
                    Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
        }
    }
}