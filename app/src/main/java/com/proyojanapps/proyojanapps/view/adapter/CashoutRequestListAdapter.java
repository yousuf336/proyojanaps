package com.proyojanapps.proyojanapps.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.CustomeAgentCashOutRequestLayoutBinding;
import com.proyojanapps.proyojanapps.model.Transaction;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.callback.CashoutRequestStatus;

import java.util.List;


public class CashoutRequestListAdapter extends RecyclerView.Adapter<CashoutRequestListAdapter.ViewHolder> {

    private Context context;
    private List<Transaction.AgentCashOutRequest> agentCashOutRequestList;
    private CashoutRequestStatus cashoutRequestStatus;

    public CashoutRequestListAdapter(Context context, List<Transaction.AgentCashOutRequest> agentCashOutRequestList, CashoutRequestStatus cashoutRequestStatus) {
        this.context = context;
        this.agentCashOutRequestList = agentCashOutRequestList;
        this.cashoutRequestStatus = cashoutRequestStatus;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomeAgentCashOutRequestLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.custome_agent_cash_out_request_layout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Transaction.AgentCashOutRequest agentCashOutRequest = agentCashOutRequestList.get(position);

        viewHolder.binding.tvName.setText(agentCashOutRequest.getFreelancerName());
        viewHolder.binding.tvAmount.setText(context.getString(R.string.amount) + " : " + String.format("%.2f", agentCashOutRequest.getAmount() != null ?
                Double.valueOf(agentCashOutRequest.getAmount()) : "0") + " " + context.getString(R.string.ta));
        viewHolder.binding.tvPayableAmount.setText(context.getString(R.string.payable_amount) + " : " + String.format("%.2f", agentCashOutRequest.getPayableAmount() != null ?
                Double.valueOf(agentCashOutRequest.getPayableAmount()) : "0") + " " + context.getString(R.string.ta));
        viewHolder.binding.tvRequestDate.setText(context.getString(R.string.date) + " : " + agentCashOutRequest.getRequest_date());
        viewHolder.binding.tvPaydate.setText(context.getString(R.string.pay_date) + " : " +
                (agentCashOutRequest.getPayDate() != null ? agentCashOutRequest.getPayDate() : "" + context.getString(R.string.unpaid)));
        viewHolder.binding.tvStatus.setText(""+ agentCashOutRequest.getStatus());

        if (agentCashOutRequest.getStatus() != null & agentCashOutRequest.getStatus().equals(StaticKey.INACTIVE)) {
            viewHolder.binding.tvStatus.setTextColor(Color.RED);
            viewHolder.binding.btnPaidInner.setText("" + context.getString(R.string.paid));
            viewHolder.binding.btnPaidInner.setBackgroundResource(R.drawable.button_round_red);
        } else {
            viewHolder.binding.tvStatus.setTextColor(Color.GREEN);
            viewHolder.binding.btnPaidInner.setText("" + context.getString(R.string.unpaid));
            viewHolder.binding.btnPaidInner.setBackgroundResource(R.drawable.button_round);
        }

        viewHolder.binding.btnPaid.setOnClickListener(view -> {
            cashoutRequestStatus.shortClick(agentCashOutRequest);
           // Toast.makeText(context, "btnPaid", Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public int getItemCount() {
        return agentCashOutRequestList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private CustomeAgentCashOutRequestLayoutBinding binding;

        public ViewHolder(CustomeAgentCashOutRequestLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private void loadUrl(String url) {
//        Intent intent = new Intent(context, WebViewActivity.class).putExtra("url", url);
//        context.startActivity(intent);
    }
}