package com.proyojanapps.proyojanapps.view.activity.agent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentBalanceBinding;
import com.proyojanapps.proyojanapps.databinding.ActivityEmployeeBalanceBinding;
import com.proyojanapps.proyojanapps.model.Agent;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.viewModel.AgentViewModel;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;

public class AgentBalanceActivity extends AppCompatActivity {
    private ActivityAgentBalanceBinding binding;
    private MySharedPreparence preparence;
    private AgentViewModel agentViewModel;
    private Agent agentInfo;
    private Freelancer freelancerInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_agent_balance);
        init();
        setData();

        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        });

        agentLogin();
    }


    private void init() {
        preparence = new MySharedPreparence(this);
        agentInfo = new Gson().fromJson(preparence.getAgentInfo(), Agent.class);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
        agentViewModel = ViewModelProviders.of(this).get(AgentViewModel.class);
    }

    private void agentLogin() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        agentViewModel.logIn(freelancerInfo.getPhone(), freelancerInfo.getPassword()).observe(this, agent -> {
            dialog.dismiss();
            if (agent != null) {
                String agentStr = new Gson().toJson(agent);
                agentInfo = agent;
                preparence.setAgentInfo(agentStr);
                setData();
            }
        });
    }

    private void setData() {
        if (agentInfo != null) {
            binding.tvMyBalance.setText("" + agentInfo.getAgentBalance() + " " + getString(R.string.ta));
            binding.tvMyCommissionBalance.setText("" + agentInfo.getAgentCommission() + " " + getString(R.string.ta));
            binding.tvTotalBalance.setText("" + agentInfo.getAgentBalance() + " " + getString(R.string.ta));
        }
    }
}