package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAddSkillBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;

public class AddSkillActivity extends AppCompatActivity {
    private Freelancer freelancerInfo;
    private MySharedPreparence preparence;
    private ActivityAddSkillBinding binding;
    private FreelancerViewModel freelancerViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_skill);
        binding.tvBack.setOnClickListener(view -> onBackPressed());
        init();
        binding.btnSave.setOnClickListener(view -> {
            addSkill();
        });
        binding.btnCancel.setOnClickListener(view -> {
            onBackPressed();
        });
        getIntentData();
    }

    private void getIntentData() {
        String identity = getIntent().getStringExtra("identity");
        if (!TextUtils.isEmpty(identity) && identity.equals("update")) {
            if (!TextUtils.isEmpty(freelancerInfo.getSkill())) {
                String[] str = freelancerInfo.getSkill().split(",");
                if (str != null && str.length == 3) {
                    binding.etMasterSkill.setText(str[0]);
                    binding.etGoodSkill.setText(str[1]);
                    binding.etBasicSkill.setText(str[2]);
                } else if (str != null && str.length == 2) {
                    binding.etMasterSkill.setText(str[0]);
                    binding.etGoodSkill.setText(str[1]);
                } else if (str != null && str.length == 1) {
                    binding.etMasterSkill.setText(str[0]);
                }
            }
            binding.btnMainSave.setText("Update");
        }
    }


    private void addSkill() {

        String masterSkill = binding.etMasterSkill.getText().toString().trim();
        String goodSkill = binding.etGoodSkill.getText().toString().trim();
        String basicSkill = binding.etBasicSkill.getText().toString().trim();
        if (!TextUtils.isEmpty(basicSkill) && !TextUtils.isEmpty(goodSkill)
                && !TextUtils.isEmpty(masterSkill)) {
            Freelancer.Skill skill = new Freelancer.Skill(freelancerInfo.getId(),
                    masterSkill + "," + goodSkill + "," + basicSkill);
            final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
            freelancerViewModel.addSkill(skill).observe(this, freelancer -> {
                dialog.dismiss();
                if (freelancer != null && freelancer.getMessage() != null && freelancer.getMessage().equals(StaticKey.SUCCESS)) {
                    freelancer.setPassword(freelancerInfo.getPassword());
                    String freelancerStr = new Gson().toJson(freelancer);
                    preparence.setFreelancerInfo(freelancerStr);
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            if (TextUtils.isEmpty(binding.etMasterSkill.getText())) {
                binding.etMasterSkill.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etMasterSkill.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etGoodSkill.getText())) {
                binding.etGoodSkill.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etGoodSkill.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etBasicSkill.getText())) {
                binding.etBasicSkill.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etBasicSkill.setFocusable(true);
            }
            Toast.makeText(this, getResources().getString(R.string.need_required_information), Toast.LENGTH_SHORT).show();
        }
    }

    private void init() {
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

}