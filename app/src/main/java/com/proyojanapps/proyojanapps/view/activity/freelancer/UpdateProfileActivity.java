package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityUpdateProfileBinding;
import com.proyojanapps.proyojanapps.databinding.ActivityUpdateWorkTimeBinding;
import com.proyojanapps.proyojanapps.model.District;
import com.proyojanapps.proyojanapps.model.Division;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.Union;
import com.proyojanapps.proyojanapps.model.Upazilla;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.activity.agent.AgentSignUpActivity;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;
import com.proyojanapps.proyojanapps.viewModel.UtilityViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class UpdateProfileActivity extends AppCompatActivity {

    private Freelancer freelancerInfo;
    private MySharedPreparence preparence;
    private ActivityUpdateProfileBinding binding;
    private FreelancerViewModel freelancerViewModel;

    private String[] bloodGroupList;
    private ArrayList<Division> divisionList;
    private ArrayList<District> districtList;
    private ArrayList<Upazilla> upazilaList;
    private ArrayList<Union> unionList;
    private int divisionId, districtId, thanaId, unionId;
    private UtilityViewModel utilityViewModel;
    private String dob, bloodGroupStr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_update_profile);
        binding.tvBack.setOnClickListener(view -> onBackPressed());

        binding.tvDateOfBirth.setOnClickListener(view -> {
            setDataOfBirth();
        });

        binding.btnCancel.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.btnMainSave.setOnClickListener(view -> {
            updateProfile();
        });


        init();
        setData();
        setBloodGroup();
        setAllDivision();
    }


    private void updateProfile() {
        String name = binding.etName.getText().toString().trim();
        String address = binding.etAddress.getText().toString().trim();
        String occupation = binding.etOccupation.getText().toString().trim();

        if (name != null)
            freelancerInfo.setName(name);
        if (address != null)
            freelancerInfo.setAddress(address);
        if (occupation != null)
            freelancerInfo.setOccupation(occupation);
        if (dob != null)
            freelancerInfo.setDateOfBirth(dob);

        if (bloodGroupStr != null)
            freelancerInfo.setBloodGroup(bloodGroupStr);

        freelancerInfo.setGender(getGender());

        if (divisionId > 0)
            freelancerInfo.setDivision(String.valueOf(divisionId));
        if (districtId > 0)
            freelancerInfo.setDistrict(String.valueOf(districtId));
        if (thanaId > 0)
            freelancerInfo.setThana(String.valueOf(thanaId));
        if (unionId > 0)
            freelancerInfo.setUnion(String.valueOf(unionId));
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerViewModel.updateProfile(freelancerInfo).observe(this, freelancer -> {
            dialog.dismiss();
            if (freelancer != null && freelancer.getMessage() != null) {
                if (freelancer.getMessage().equals(StaticKey.SUCCESS)) {
                    freelancer.setPassword(freelancerInfo.getPassword());
                    String freelancerInfo = new Gson().toJson(freelancer);
                    this.freelancerInfo = freelancer;
                    preparence.setFreelancerInfo(freelancerInfo);
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    Toast.makeText(this, getResources().getString(R.string.updated), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
            }
        });


    }


    private void setData() {
        if (!TextUtils.isEmpty(freelancerInfo.getName()))
            binding.etName.setText(freelancerInfo.getName());
        if (!TextUtils.isEmpty(freelancerInfo.getDateOfBirth())) {
            binding.tvDateOfBirth.setText(freelancerInfo.getDateOfBirth());
            dob = freelancerInfo.getDateOfBirth();
        }
        if (!TextUtils.isEmpty(freelancerInfo.getOccupation()))
            binding.etOccupation.setText(freelancerInfo.getOccupation());

        if (!TextUtils.isEmpty(freelancerInfo.getAddress()))
            binding.etAddress.setText(freelancerInfo.getAddress());

        if (!TextUtils.isEmpty(freelancerInfo.getGender())) {
            if (freelancerInfo.getGender().equals("Male")) {
                binding.rbMale.setChecked(true);
            } else {
                binding.rbFemale.setChecked(true);
            }
        }

    }

    private void init() {
        bloodGroupList = getResources().getStringArray(R.array.blood_group);
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
        utilityViewModel = ViewModelProviders.of(this).get(UtilityViewModel.class);
    }

    private void setBloodGroup() {
        int selectedPosition = 0;
        ArrayAdapter bloodGroupAdapter = new ArrayAdapter(this, R.layout.spinner_layout, R.id.tvContent, bloodGroupList);
        binding.spBloodGroup.setAdapter(bloodGroupAdapter);
        if (freelancerInfo.getBloodGroup() != null)
            for (int i = 0; i < bloodGroupList.length; i++) {
                if (bloodGroupList[i].toLowerCase().equals("" + freelancerInfo.getBloodGroup().toLowerCase())) {
                    selectedPosition = i;
                }
            }
        binding.spBloodGroup.setSelection(selectedPosition);
        binding.spBloodGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    bloodGroupStr = bloodGroupList[position];
                } else {
                    bloodGroupStr = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void setDataOfBirth() {
        final Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                dob = sdf.format(myCalendar.getTime());
                binding.tvDateOfBirth.setText(sdf.format(myCalendar.getTime()));
            }

        };
        new DatePickerDialog(this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void setDistrict() {
        int selectedPosition = -1, i = 0;
        ArrayList<String> districtListStr = new ArrayList<>();
        districtListStr.add("Select District*");
        if (districtList != null)
            for (District d : districtList) {
                districtListStr.add(d.getName());
                if (d.getName().equals(freelancerInfo.getDistrict())) {
                    selectedPosition = i;
                }
                i += 1;
            }

        ArrayAdapter districtAdapter = new ArrayAdapter(this, R.layout.spinner_layout, R.id.tvContent, districtListStr);
        binding.spDistrict.setAdapter(districtAdapter);
        if (districtListStr.size() > 1 && selectedPosition >= 0)
            binding.spDistrict.setSelection(selectedPosition + 1);
        binding.spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    districtId = districtList.get(position - 1).getDistrictId();
                    // final Dialog dialog = CustomLoadingDialog.createLoadingDialog(UpdateProfileActivity.this);
                    utilityViewModel.getUpazilaList(districtId).observe(UpdateProfileActivity.this, upList -> {
                        //   dialog.dismiss();
                        if (upList != null && upList.size() > 0) {
                            upazilaList = new ArrayList<>();
                            upazilaList = (ArrayList<Upazilla>) upList;
                            setUpazilaList();
                        } else {

                        }
                    });

                } else {
                    districtId = 0;
                    upazilaList = new ArrayList<>();
                    setUpazilaList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void setUpazilaList() {
        int selectedPosition = 0, i = 0;
        ArrayList<String> upazilatListStr = new ArrayList<>();
        upazilatListStr.add("Select Upazila");
        if (upazilaList != null)
            for (Upazilla upazilla : upazilaList) {
                upazilatListStr.add(upazilla.getName());
                if (upazilla.getName().equals(freelancerInfo.getThana())) {
                    selectedPosition = i;
                }
                i += 1;
            }
        ArrayAdapter upazilaAdapter = new ArrayAdapter(this, R.layout.spinner_layout, R.id.tvContent, upazilatListStr);
        binding.spThana.setAdapter(upazilaAdapter);
        if (upazilatListStr.size() > 1 && selectedPosition >= 0)
            binding.spThana.setSelection(selectedPosition + 1);
        binding.spThana.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    thanaId = UpdateProfileActivity.this.upazilaList.get(position - 1).getUpazillaId();
                    //final Dialog dialog = CustomLoadingDialog.createLoadingDialog(UpdateProfileActivity.this);
                    utilityViewModel.getUnionList(thanaId).observe(UpdateProfileActivity.this, uList -> {
                        //  dialog.dismiss();
                        if (uList != null && uList.size() > 0) {
                            unionList = new ArrayList<>();
                            unionList = (ArrayList<Union>) uList;
                            setUnionList();
                        } else {

                        }
                    });

                } else {
                    thanaId = 0;
                    unionList = new ArrayList<>();
                    setUnionList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setUnionList() {
        int selectedPosition = 0, i = 0;
        ArrayList<String> unionListStr = new ArrayList<>();
        unionListStr.add("Select Union");
        if (unionList != null)
            for (Union union : unionList) {
                unionListStr.add(union.getName());
                if (union.getName().equals(freelancerInfo.getUnion())) {
                    selectedPosition = i;
                }
                i += 1;
            }
        ArrayAdapter unionAdapter = new ArrayAdapter(this, R.layout.spinner_layout, R.id.tvContent, unionListStr);
        binding.spUnion.setAdapter(unionAdapter);
        if (unionListStr.size() > 1 && selectedPosition >= 0)
            binding.spUnion.setSelection(selectedPosition + 1);
        binding.spUnion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    unionId = UpdateProfileActivity.this.unionList.get(position - 1).getId();
                } else {
                    unionId = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setAllDivision() {
        ArrayList<String> divisionListStr = new ArrayList<>();
        divisionListStr.add("Select Division");
        ArrayAdapter divisionAdapter = new ArrayAdapter(this, R.layout.spinner_layout, R.id.tvContent, divisionListStr);
        binding.spDivision.setAdapter(divisionAdapter);
        binding.spDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    divisionId = UpdateProfileActivity.this.divisionList.get(position - 1).getDivisionId();
                    //Toast.makeText(AgentSignUpActivity.this, "divisionId "+divisionId, Toast.LENGTH_SHORT).show();
                    //  final Dialog dialog = CustomLoadingDialog.createLoadingDialog(UpdateProfileActivity.this);
                    utilityViewModel.getDistrictList(divisionId).observe(UpdateProfileActivity.this, distList -> {
                        //  dialog.dismiss();
                        if (distList != null && distList.size() > 0) {
                            districtList = new ArrayList<>();
                            districtList = (ArrayList<District>) distList;
                            setDistrict();
                        } else {

                        }
                    });

                } else {
                    divisionId = 0;
                    districtList = new ArrayList<>();
                    setDistrict();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        utilityViewModel.getAllDivision().observe(this, dList -> {
            dialog.dismiss();
            if (dList != null && dList.size() > 0) {
                divisionList = new ArrayList<>();
                this.divisionList = (ArrayList<Division>) dList;
                //Toast.makeText(this, "divisionList "+divisionList.size(), Toast.LENGTH_SHORT).show();
                int selectedPosition = 0, i = 0;
                for (Division d : divisionList) {
                    divisionListStr.add(d.getName());
                    if (d.getName().equals(freelancerInfo.getDivision())) {
                        selectedPosition = i;
                    }
                    i += 1;
                }

                divisionAdapter.notifyDataSetChanged();
                if (divisionList.size() > 0)
                    binding.spDivision.setSelection(selectedPosition + 1);
            } else {
                // Toast.makeText(this, "divisionList null", Toast.LENGTH_SHORT).show();
                //setDistrict(0);
            }
        });

    }

    private String getGender() {
        String gender = null;
        if (binding.rbMale.isChecked()) {
            gender = "Male";
        } else if (binding.rbFemale.isChecked())
            gender = "Female";
        return gender;
    }
}