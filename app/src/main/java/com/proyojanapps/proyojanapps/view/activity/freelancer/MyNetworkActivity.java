package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityMyNetworkBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.FreelancerNetwork;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.CustomVisibility;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.view.adapter.MyNetworkAdapter;
import com.proyojanapps.proyojanapps.viewModel.FreelancerActivitesViewModel;

import java.util.ArrayList;

public class MyNetworkActivity extends AppCompatActivity {
    private ActivityMyNetworkBinding binding;
    private FreelancerActivitesViewModel freelancerActivitesViewModel;
    private MySharedPreparence preparence;
    private ArrayList<FreelancerNetwork> mainList, levelOneList, levelTwoList, levelThreeList, levelFourList, levelFiveList;
    private MyNetworkAdapter myNetworkAdapter1, myNetworkAdapter2, myNetworkAdapter3, myNetworkAdapter4, myNetworkAdapter5;
    private boolean isLevelOneExpanded, isLevelTwoExpanded, isLevelThreeExpanded, isLevelFourExpanded, isLevelFiveExpanded;
    private Dialog dialog = null;
    private Freelancer freelancerInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_network);

        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });
        init();
        getIncomePlans();

        binding.rlFirstMembers.setOnClickListener(view -> {
            if (isLevelOneExpanded) {
                isLevelOneExpanded = false;
                CustomVisibility.collapse(binding.firstLevelRV, 1000);
            } else {
                isLevelOneExpanded = true;
                initlevelOneRV();
                CustomVisibility.expand(binding.firstLevelRV, 1000);
            }

        });
        binding.rlSecondMembers.setOnClickListener(view -> {
            if (isLevelTwoExpanded) {
                isLevelTwoExpanded = false;
                CustomVisibility.collapse(binding.secondLevelRV, 1000);
            } else {
                isLevelTwoExpanded = true;
                initlevelTwoRV();
                CustomVisibility.expand(binding.secondLevelRV, 1000);
            }
        });
        binding.rlThirdMembers.setOnClickListener(view -> {
            if (isLevelThreeExpanded) {
                isLevelThreeExpanded = false;
                CustomVisibility.collapse(binding.thirdLevelRV, 1000);
            } else {
                isLevelThreeExpanded = true;
                initlevelThreeRV();
                CustomVisibility.expand(binding.thirdLevelRV, 1000);
            }
        });
        binding.rlFourthMembers.setOnClickListener(view -> {
            if (isLevelFourExpanded) {
                isLevelFourExpanded = false;
                CustomVisibility.collapse(binding.fourthLevelRV, 1000);
            } else {
                isLevelFourExpanded = true;
                initlevelFourRV();
                CustomVisibility.expand(binding.fourthLevelRV, 1000);
            }
        });
        binding.rlFiveMembers.setOnClickListener(view -> {
            if (isLevelFiveExpanded) {
                isLevelFiveExpanded = false;
                CustomVisibility.collapse(binding.fiveLevelRV, 1000);
            } else {
                isLevelFiveExpanded = true;
                initlevelFiveRV();
                CustomVisibility.expand(binding.fiveLevelRV, 1000);
            }
        });
    }

    private void getIncomePlans() {
        dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerActivitesViewModel.myNetwork(freelancerInfo.getId())
                .observe(this, networks -> {

                    if (networks != null) {
                        mainList.clear();
                        mainList = (ArrayList<FreelancerNetwork>) networks;
                        // initIncomePlaneRV();
                        distinguishArrayAccordingToLevel();

                        //  myNetworkAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void init() {
        mainList = new ArrayList<>();
        levelOneList = new ArrayList<>();
        levelTwoList = new ArrayList<>();
        levelThreeList = new ArrayList<>();
        levelFourList = new ArrayList<>();
        levelFiveList = new ArrayList<>();
        freelancerActivitesViewModel = ViewModelProviders.of(this).get(FreelancerActivitesViewModel.class);
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
    }

    private void initlevelOneRV() {
        myNetworkAdapter1 = new MyNetworkAdapter(this, levelOneList);
        binding.firstLevelRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.firstLevelRV.setAdapter(myNetworkAdapter1);
    }

    private void initlevelTwoRV() {
        myNetworkAdapter2 = new MyNetworkAdapter(this, levelTwoList);
        binding.secondLevelRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.secondLevelRV.setAdapter(myNetworkAdapter2);
    }

    private void initlevelThreeRV() {
        myNetworkAdapter3 = new MyNetworkAdapter(this, levelThreeList);
        binding.thirdLevelRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.thirdLevelRV.setAdapter(myNetworkAdapter3);
    }

    private void initlevelFourRV() {
        myNetworkAdapter4 = new MyNetworkAdapter(this, levelFourList);
        binding.fourthLevelRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.fourthLevelRV.setAdapter(myNetworkAdapter4);
    }

    private void initlevelFiveRV() {
        myNetworkAdapter5 = new MyNetworkAdapter(this, levelFiveList);
        binding.fiveLevelRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.fiveLevelRV.setAdapter(myNetworkAdapter5);
    }

    private void distinguishArrayAccordingToLevel() {
        levelOneList.clear();
        levelTwoList.clear();
        levelThreeList.clear();
        levelFourList.clear();
        levelFiveList.clear();
        for (FreelancerNetwork f : mainList) {
            switch (f.getLevel()) {
                case "first_level":
                    levelOneList.add(f);
                    break;
                case "second_level":
                    levelTwoList.add(f);
                    break;
                case "third_level":
                    levelThreeList.add(f);
                    break;
                case "fourth_level":
                    levelFourList.add(f);
                    break;
                case "five_level":
                    levelFiveList.add(f);
                    break;
            }
        }
        dialog.dismiss();
        setCounterData();
        initlevelOneRV();
        initlevelTwoRV();
        initlevelThreeRV();
        initlevelFourRV();
        initlevelFiveRV();

    }

    private void setCounterData() {
        binding.tvCounterFirst.setText(""+levelOneList.size());
        binding.tvCounterSecond.setText(""+levelTwoList.size());
        binding.tvCounterThird.setText(""+levelThreeList.size());
        binding.tvCounterFourth.setText(""+levelFourList.size());
        binding.tvCounterFive.setText(""+levelFiveList.size());
    }
}