package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityIncomePlansBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.IncomePlans;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.view.adapter.IncomePlansAdapter;
import com.proyojanapps.proyojanapps.view.adapter.MyIncomePlanAdapter;
import com.proyojanapps.proyojanapps.view.adapter.MyOfferAdapter;
import com.proyojanapps.proyojanapps.viewModel.FreelancerActivitesViewModel;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;

import java.util.ArrayList;

public class IncomePlansActivity extends AppCompatActivity {

    private ActivityIncomePlansBinding binding;
    private FreelancerActivitesViewModel freelancerActivitesViewModel;
    private MySharedPreparence preparence;
    private ArrayList<IncomePlans> list;
    private IncomePlansAdapter incomePlansAdapter;
    private FreelancerViewModel freelancerViewModel;
    private Freelancer freelancerInfo;
    private ArrayList<Freelancer.MyIncomePlan> myIncomePlanArrayList;
    private MyIncomePlanAdapter myIncomePlanAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_income_plans);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        init();
        initIncomePlaneRV();
        getIncomePlans();
        getMyInComePlan();

    }
    private void getMyInComePlan() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerViewModel.myIncomePlan().observe(this, myIncomePlans -> {
            dialog.dismiss();
            if (myIncomePlans != null) {
                myIncomePlanArrayList = (ArrayList<Freelancer.MyIncomePlan>) myIncomePlans;
                initMyIncomeplanRV();
            }
        });
    }

    private void initMyIncomeplanRV() {
        myIncomePlanAdapter = new MyIncomePlanAdapter(this, myIncomePlanArrayList);
        binding.myIncomePlanSecondRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.myIncomePlanSecondRV.setAdapter(myIncomePlanAdapter);
    }


    //    private void getMyNetwork(){
//        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
//        freelancerActivitesViewModel.myNetwork(freelancerInfo.getId())
//                .observe(this, freelancerNetworkList -> {
//                    dialog.dismiss();
//
//                });
//    }
//
//    private void getIncomeReport(){
//        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
//        freelancerActivitesViewModel.incomeReport(freelancerInfo.getId())
//                .observe(this, freelancerIncomeReport -> {
//                    dialog.dismiss();
//
//                });
//    }
    private void getIncomePlans() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerActivitesViewModel.getIncomePlans()
                .observe(this, incomePlans -> {
                    dialog.dismiss();
                    if (incomePlans != null) {
                        list.clear();
                        list = (ArrayList<IncomePlans>) incomePlans;
                       initIncomePlaneRV();
                        //incomePlansAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void init() {
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
        myIncomePlanArrayList = new ArrayList<>();
        list = new ArrayList<>();
        freelancerActivitesViewModel = ViewModelProviders.of(this).get(FreelancerActivitesViewModel.class);
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
    }

    private void initIncomePlaneRV() {
        incomePlansAdapter = new IncomePlansAdapter(this, list);
        binding.incomePlanRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.incomePlanRV.setAdapter(incomePlansAdapter);
    }

}