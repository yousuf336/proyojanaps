package com.proyojanapps.proyojanapps.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.CustomeAgentLayoutBinding;
import com.proyojanapps.proyojanapps.databinding.MyNetworkMemberLayoutBinding;
import com.proyojanapps.proyojanapps.model.AgentList;
import com.proyojanapps.proyojanapps.model.FreelancerNetwork;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;

import java.util.List;


public class AgentListAdapter extends RecyclerView.Adapter<AgentListAdapter.ViewHolder> {

    private Context context;
    private List<AgentList> agentLists;

    public AgentListAdapter(Context context, List<AgentList> agentLists) {
        this.context = context;
        this.agentLists = agentLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomeAgentLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.custome_agent_layout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        AgentList agentList = agentLists.get(position);

        viewHolder.binding.tvName.setText(agentList.getName());
        viewHolder.binding.tvNumber.setText(agentList.getPhone());
        viewHolder.binding.tvOwnRefer.setText("Own Refer : " + agentList.getReeferId());
        viewHolder.binding.tvTotalTransactions.setText(context.getString(R.string.total_transactions) + " : " + agentList.getTransation());
        if (agentList.getProfilepic() != null)
            Glide.with(context).applyDefaultRequestOptions(new RequestOptions()
                    .placeholder(R.drawable.ic_image_black_24dp)).load(agentList.getProfilepic()).into(viewHolder.binding.profileImageCIV);


        viewHolder.binding.llRow.setOnClickListener(view -> {

        });
    }

    @Override
    public int getItemCount() {
        return agentLists.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private CustomeAgentLayoutBinding binding;

        public ViewHolder(CustomeAgentLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private void loadUrl(String url) {
//        Intent intent = new Intent(context, WebViewActivity.class).putExtra("url", url);
//        context.startActivity(intent);
    }
}