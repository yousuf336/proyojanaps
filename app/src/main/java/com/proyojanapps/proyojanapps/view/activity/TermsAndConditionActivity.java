package com.proyojanapps.proyojanapps.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Html;
import android.widget.Toast;

import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityTermsAndConditionBinding;
import com.proyojanapps.proyojanapps.model.Upazilla;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;
import com.proyojanapps.proyojanapps.viewModel.UtilityViewModel;

import java.util.ArrayList;

import okhttp3.internal.Util;

public class TermsAndConditionActivity extends AppCompatActivity {

    ActivityTermsAndConditionBinding binding;
    UtilityViewModel utilityViewModel;
    MySharedPreparence preparence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_terms_and_condition);
        init();
        binding.imageViewBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        termsAndCondition();
    }

    private void termsAndCondition() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        utilityViewModel.getTermsAndConditions().observe(this, termsAndCondition -> {
            dialog.dismiss();
            if (termsAndCondition != null) {
                binding.tvContent.setText("" + Html.fromHtml(termsAndCondition.getDescription()));
            } else {
                Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void init() {
        utilityViewModel = ViewModelProviders.of(this).get(UtilityViewModel.class);
        preparence = new MySharedPreparence(this);

    }
}