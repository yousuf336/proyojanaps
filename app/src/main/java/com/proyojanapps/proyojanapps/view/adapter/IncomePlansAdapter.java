package com.proyojanapps.proyojanapps.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.CustomeRowBinding;
import com.proyojanapps.proyojanapps.databinding.ModelImageSliderItemBinding;
import com.proyojanapps.proyojanapps.model.IncomePlans;
import com.proyojanapps.proyojanapps.model.Slider;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.List;


public class IncomePlansAdapter extends RecyclerView.Adapter<IncomePlansAdapter.ViewHolder> {

    private Context context;
    private List<IncomePlans> incomePlansList;

    public IncomePlansAdapter(Context context, List<IncomePlans> incomePlansList) {
        this.context = context;
        this.incomePlansList = incomePlansList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomeRowBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.custome_row, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        IncomePlans incomePlans = incomePlansList.get(position);

        viewHolder.binding.tvLeft.setText(incomePlans.getName());
        viewHolder.binding.tvRight.setText(incomePlans.getPayment());


        if ((position+1)%2==0)
        viewHolder.binding.llRow.setBackgroundColor(context.getResources().getColor(R.color.off_white));
        else {
            viewHolder.binding.llRow.setBackgroundColor(context.getResources().getColor(R.color.white));
        }


        viewHolder.binding.llRow.setOnClickListener(view -> {

        });
    }

    @Override
    public int getItemCount() {
        return incomePlansList.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder {

        private CustomeRowBinding binding;

        public ViewHolder(CustomeRowBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private void loadUrl(String url) {
//        Intent intent = new Intent(context, WebViewActivity.class).putExtra("url", url);
//        context.startActivity(intent);
    }
}