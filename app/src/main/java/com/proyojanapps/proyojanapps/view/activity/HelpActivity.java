package com.proyojanapps.proyojanapps.view.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityHelpBinding;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.viewModel.UtilityViewModel;

import java.util.HashMap;
import java.util.Map;

public class HelpActivity extends FragmentActivity implements OnMapReadyCallback {
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final Float DEFAULT_ZOOM = 14.5f;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private ActivityHelpBinding binding;
    private FragmentManager fragmentManager;
    private GoogleMapOptions googleMapOptions;
    private GoogleMap map;
    private Boolean locationPermissionsGranted = false;
    private UtilityViewModel utilityViewModel;
    private MySharedPreparence preparence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_help);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        init();
        getLocationPermission();

        binding.btnSubmit.setOnClickListener(view -> {
            submitMessage();
        });

    }

    private void init() {
        fragmentManager = getSupportFragmentManager();
        utilityViewModel = ViewModelProviders.of(this).get(UtilityViewModel.class);
        preparence = new MySharedPreparence(this);
    }

    private void initializeMap() {
        GoogleMapOptions googleMapOptions = new GoogleMapOptions();
        SupportMapFragment supportMapFragment = SupportMapFragment.newInstance(googleMapOptions);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.llMap, supportMapFragment);
        fragmentTransaction.commitAllowingStateLoss();
        supportMapFragment.getMapAsync(this);
    }

    private void submitMessage() {
        String name = binding.etName.getText().toString();
        String email = binding.etEmail.getText().toString();
        String phone = binding.etPhone.getText().toString();
        String subject = binding.etSubject.getText().toString();
        String message = binding.etMessage.getText().toString();

        if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(phone) &&
                !TextUtils.isEmpty(subject) && !TextUtils.isEmpty(message)) {
            Map<String, Object> map = new HashMap<>();
            map.put("name", name);
            map.put("contact_subject", subject);
            map.put("contact_phone", phone);
            map.put("contact_email", email);
            map.put("contact_text", message);

            final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
            utilityViewModel.submitMessage(map).observe(this, response -> {
                dialog.dismiss();
                if (response != null) {
                    if (response.equals(StaticKey.SUCCESS)) {
                        Toast.makeText(this, getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            if (TextUtils.isEmpty(binding.etName.getText())) {
                binding.etName.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etName.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etEmail.getText())) {
                binding.etEmail.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etEmail.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etPhone.getText())) {
                binding.etPhone.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etPhone.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etMessage.getText())) {
                binding.etMessage.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etMessage.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etSubject.getText())) {
                binding.etSubject.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etSubject.setFocusable(true);
            }
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        changeMapStyle(map);

        if (locationPermissionsGranted) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            animateCamera(new LatLng(23.4607, 91.1809), 9);


            map.setMyLocationEnabled(true);
            map.getUiSettings().setMapToolbarEnabled(false);
            map.getUiSettings().setMyLocationButtonEnabled(false);

        }
    }

    private void animateCamera(LatLng latLng, float zoom) {
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    private void changeMapStyle(GoogleMap map) {
        try {
            map.setMapStyle(MapStyleOptions.loadRawResourceStyle(
                    this, R.raw.map_style));


        } catch (Resources.NotFoundException e) {

        }
    }

    private void getLocationPermission() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                        COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionsGranted = true;
                    initializeMap();
                } else {
                    ActivityCompat.requestPermissions(this,
                            permissions,
                            LOCATION_PERMISSION_REQUEST_CODE);
                }
            } else {
                ActivityCompat.requestPermissions(this,
                        permissions,
                        LOCATION_PERMISSION_REQUEST_CODE);
            }
        } else {
            locationPermissionsGranted = true;
            initializeMap();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        locationPermissionsGranted = false;

        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            locationPermissionsGranted = false;
                            return;
                        }
                    }
                    locationPermissionsGranted = true;
                    //initialize our map
                    initializeMap();
                }
            }
        }
    }

}