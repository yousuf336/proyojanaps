package com.proyojanapps.proyojanapps.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAboutusBinding;
import com.proyojanapps.proyojanapps.model.AboutUs;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.viewModel.FreelancerActivitesViewModel;
import com.proyojanapps.proyojanapps.viewModel.UtilityViewModel;

public class AboutusActivity extends AppCompatActivity {
    ActivityAboutusBinding binding;
    private UtilityViewModel utilityViewModel;
    private AboutUs aboutUs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_aboutus);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        utilityViewModel = ViewModelProviders.of(this).get(UtilityViewModel.class);
        aboutUs();
    }

    private void aboutUs() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        utilityViewModel.aboutUst()
                .observe(this, aboutUs -> {
                    dialog.dismiss();
                    if (aboutUs != null) {
                        this.aboutUs = aboutUs;
                        setData();
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void setData() {
        binding.tvAboutUsTitle.setText("" + aboutUs.getTitle());
        binding.tvDescription.setText("" + aboutUs.getDescription());
        if (aboutUs.getImage() != null)
            Glide.with(this).applyDefaultRequestOptions(new RequestOptions()
                    .placeholder(R.drawable.ic_image_black_24dp)).load(aboutUs.getImage()).into(binding.imageView);

    }

}