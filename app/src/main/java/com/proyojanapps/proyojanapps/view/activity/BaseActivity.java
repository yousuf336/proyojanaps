package com.proyojanapps.proyojanapps.view.activity;

import android.animation.Animator;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;


import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.DialogConfirmationBinding;
import com.proyojanapps.proyojanapps.databinding.DialogCustomeBinding;
import com.proyojanapps.proyojanapps.databinding.DialogSuccessBinding;
import com.proyojanapps.proyojanapps.view.fragment.ZoomImageDialog;

import java.util.Locale;

public class BaseActivity extends AppCompatActivity {

    protected Dialog dialogConfirmation,dialogCustome;
    protected DialogConfirmationBinding dialogConfirmationBinding;
    protected DialogCustomeBinding dialogCustomeBinding;
    protected     DialogSuccessBinding dialogSuccessBinding;
    protected Dialog dialogSuccess;


    protected void setLocalLanguage(String type) {
        Locale locale = new Locale(type);
        Locale.setDefault(locale);
        Configuration configuration = getResources().getConfiguration();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
            configuration.setLocale(locale);
            createConfigurationContext(configuration);
        }
        configuration.locale = locale;
        configuration.setLayoutDirection(locale);
        getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
    }
    protected void showConfirmationDialog(Context context) {
        dialogConfirmation = new Dialog(context);
        dialogConfirmation.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogConfirmationBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_confirmation, null, false);
        dialogConfirmation.setContentView(dialogConfirmationBinding.getRoot());

        Window window = dialogConfirmation.getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        dialogConfirmation.show();
    }
    protected void showCustomeDialog(Context context) {
        dialogCustome = new Dialog(context);
        dialogCustome.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCustomeBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_custome, null, false);
        dialogCustome.setContentView(dialogCustomeBinding.getRoot());

        Window window = dialogCustome.getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        dialogCustome.show();
    }
    protected void showSuccessDialog(final Context context) {
        dialogSuccess = new Dialog(context, R.style.FullScreenDialogStyle);
        dialogSuccess.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSuccessBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_success, null, false);
        dialogSuccess.setContentView(dialogSuccessBinding.getRoot());

        Window window = dialogSuccess.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        dialogSuccessBinding.animationViewOnline.addAnimatorListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animator) {
//                // Toast.makeText(context, "onAnimationStart", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animator) {
//                // Toast.makeText(context, "onAnimationEnd", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animator) {
//                // Toast.makeText(context, "onAnimationCancel", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animator) {
//                // Toast.makeText(context, "onAnimationRepeat", Toast.LENGTH_SHORT).show();
//                dialogSuccess.cancel();
//            }
//        });
        dialogSuccess.show();
    }

    protected void showZoomImageDialog(String imageUrl) {
        Bundle bundle = new Bundle();
        bundle.putString("imageUrl", imageUrl);
        ZoomImageDialog dialog = new ZoomImageDialog();
        dialog.setArguments(bundle);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        dialog.show(ft, "TAG");
    }
    protected void changeStatusBarIconColor(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN|View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            // edited here
            getWindow().setStatusBarColor(Color.WHITE);
        }
    }
    public void setStatusBarColorGreen(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#168306"));
        }
    }
}
