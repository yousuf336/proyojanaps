package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityIncomePlansBinding;
import com.proyojanapps.proyojanapps.databinding.ActivityIncomeReportBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.FreelancerIncomeReport;
import com.proyojanapps.proyojanapps.model.IncomePlans;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.view.activity.BaseActivity;
import com.proyojanapps.proyojanapps.view.adapter.IncomePlansAdapter;
import com.proyojanapps.proyojanapps.view.adapter.IncomeReportAdapter;
import com.proyojanapps.proyojanapps.view.adapter.MyNetworkAdapter;
import com.proyojanapps.proyojanapps.viewModel.FreelancerActivitesViewModel;

import java.util.ArrayList;

public class IncomeReportActivity extends BaseActivity {
    private ActivityIncomeReportBinding binding;
    private FreelancerActivitesViewModel freelancerActivitesViewModel;
    private MySharedPreparence preparence;
    private ArrayList<FreelancerIncomeReport.MemberIncome> list;
    private FreelancerIncomeReport freelancerIncomeReport;
    private IncomeReportAdapter incomeReportAdapter;
    private Freelancer freelancerInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_income_report);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        init();
        getIncomeReport();
        initIncomeReportRV();
        setData();
    }

    private void init() {

        list = new ArrayList<>();
        freelancerActivitesViewModel = ViewModelProviders.of(this).get(FreelancerActivitesViewModel.class);
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
    }

    private void getIncomeReport() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerActivitesViewModel.incomeReport(freelancerInfo.getId())
                .observe(this, freelancerIncomeReport -> {
                    dialog.dismiss();
                    if (freelancerIncomeReport != null) {
                        this.freelancerIncomeReport = freelancerIncomeReport;
                        initIncomeReportRV();
                        setData();
                    } else {
                        Toast.makeText(this, ""+getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }

                });
    }

    private void setData() {
        binding.includeTotalWorkingIncome.tvLeft.setText("" + getString(R.string.total_working_income));
        binding.includeTotalReferIncome.tvLeft.setText("" + getString(R.string.total_refer_income));
        binding.includeTotal.tvLeft.setText("" + getString(R.string.total));

        if (freelancerIncomeReport != null) {
            binding.includeTotalWorkingIncome.tvRight.setText(" " + String.format("%.2f", freelancerIncomeReport.getTotalWorkIncome() != null ?
                    Double.valueOf(freelancerIncomeReport.getTotalWorkIncome()) : "0") + " " + getString(R.string.ta));
            binding.includeTotalReferIncome.tvRight.setText(" " + String.format("%.2f", freelancerIncomeReport.getTotalReferIncome()
                    != null ? Double.valueOf(freelancerIncomeReport.getTotalReferIncome()) : "0") + " " + getString(R.string.ta));
            binding.includeTotal.tvRight.setText(" " + String.format("%.2f", Double.valueOf(freelancerIncomeReport.getTotalBalance()))
                    + " " + getString(R.string.ta));
        }

    }

    private void initIncomeReportRV() {
        if (freelancerIncomeReport != null && freelancerIncomeReport.getMemberIncomesList() != null) {
            incomeReportAdapter = new IncomeReportAdapter(this, freelancerIncomeReport.getMemberIncomesList());
            binding.imemberIncomeRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
            binding.imemberIncomeRV.setAdapter(incomeReportAdapter);
        }
    }

}