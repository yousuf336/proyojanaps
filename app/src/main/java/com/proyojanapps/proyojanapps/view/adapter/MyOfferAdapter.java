package com.proyojanapps.proyojanapps.view.adapter;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.CustomeMyofferLayoutBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.IncomePlans;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;

import java.util.List;


public class MyOfferAdapter extends RecyclerView.Adapter<MyOfferAdapter.ViewHolder> {

    private Context context;
    private List<Freelancer.MyOffers> myOffersList;

    public MyOfferAdapter(Context context, List<Freelancer.MyOffers> myOffersList) {
        this.context = context;
        this.myOffersList = myOffersList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomeMyofferLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.custome_myoffer_layout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Freelancer.MyOffers myOffers = myOffersList.get(position);

        viewHolder.binding.tvPublicationDate.setText("Publication Date : " + myOffers.getPublicationDate());
        viewHolder.binding.tvValidationDate.setText("Validation Date : " + myOffers.getValidationDate());

        if (myOffers.getImage() != null)
            Glide.with(context).applyDefaultRequestOptions(new RequestOptions()
                    .placeholder(R.drawable.ic_image_black_24dp)).load(RetrofitInstance.BASE_URL + "/" + myOffers.getImage()).into(viewHolder.binding.imageView);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
           viewHolder.binding.tvDescription .setText(Html.fromHtml(""+myOffers.getDescription(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            viewHolder.binding.tvDescription .setText(Html.fromHtml(""+myOffers.getDescription()));
        }
        if ((position + 1) % 2 == 0)
            viewHolder.binding.llRow.setBackgroundColor(context.getResources().getColor(R.color.off_white));
        else {
            viewHolder.binding.llRow.setBackgroundColor(context.getResources().getColor(R.color.white));
        }


        viewHolder.binding.llRow.setOnClickListener(view -> {

        });
    }

    @Override
    public int getItemCount() {
        return myOffersList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private CustomeMyofferLayoutBinding binding;

        public ViewHolder(CustomeMyofferLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private void loadUrl(String url) {
//        Intent intent = new Intent(context, WebViewActivity.class).putExtra("url", url);
//        context.startActivity(intent);
    }
}