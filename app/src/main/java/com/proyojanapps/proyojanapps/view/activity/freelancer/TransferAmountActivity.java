package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityTransferAmountBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.activity.freelancer.ConfirmTransactionActivity;
import com.proyojanapps.proyojanapps.viewModel.FreelancerActivitesViewModel;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;

public class TransferAmountActivity extends AppCompatActivity {
    private ActivityTransferAmountBinding binding;
    private FreelancerActivitesViewModel freelancerActivitesViewModel;
    private MySharedPreparence preparence;
    private Freelancer freelancerInfo;
    private FreelancerViewModel freelancerViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_transfer_amount);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        init();
        binding.btnSubmit.setOnClickListener(view -> {
            checkInput();
        });
    }

    private void checkInput() {
        String referId = binding.etReferId.getText().toString();
        String amount = binding.etAmount.getText().toString();
        String password = binding.etPassword.getText().toString();

        if (!TextUtils.isEmpty(referId) && !TextUtils.isEmpty(amount) && !TextUtils.isEmpty(password)) {
            if (freelancerInfo != null && freelancerInfo.getPassword() != null && freelancerInfo.getPassword().equals(password)) {
                freelancerLogin(amount, referId);
            } else {
                binding.etPassword.setFocusable(true);
                binding.etPassword.setError("" + getString(R.string.password_mismatch));
            }
        } else {
            if (TextUtils.isEmpty(binding.etReferId.getText())) {
                binding.etReferId.setFocusable(true);
                binding.etReferId.setError("" + getString(R.string.required_field_cannot_be_empty));
            }
            if (TextUtils.isEmpty(binding.etPassword.getText())) {
                binding.etPassword.setFocusable(true);
                binding.etPassword.setError("" + getString(R.string.required_field_cannot_be_empty));
            }
            if (TextUtils.isEmpty(binding.etAmount.getText())) {
                binding.etAmount.setFocusable(true);
                binding.etAmount.setError("" + getString(R.string.required_field_cannot_be_empty));
            }
            Toast.makeText(this, getResources().getString(R.string.need_required_information), Toast.LENGTH_SHORT).show();
        }


    }

    private void init() {
        freelancerActivitesViewModel = ViewModelProviders.of(this).get(FreelancerActivitesViewModel.class);
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
    }

    private void freelancerLogin(String amount, String referId) {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerViewModel.logIn(freelancerInfo.getPhone(), freelancerInfo.getPassword()).observe(this, freelancer -> {
            dialog.dismiss();
            if (freelancer != null && freelancer.getMessage() != null) {
                if (freelancer.getMessage().equals("success")) {
                    freelancer.setPassword(freelancerInfo.getPassword());
                    String freelancerInfo = new Gson().toJson(freelancer);
                    preparence.setFreelancerInfo(freelancerInfo);

                    startActivity(new Intent(this, ConfirmTransactionActivity.class)
                            .putExtra("amount", amount)
                            .putExtra("referId", referId)
                            .putExtra("identity", StaticKey.TRANSFER));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                } else {
                    Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();

            }
        });
    }
}