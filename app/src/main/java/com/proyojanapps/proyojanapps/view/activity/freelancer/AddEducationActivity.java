package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAddEducationBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AddEducationActivity extends AppCompatActivity {
    private Freelancer freelancerInfo;
    private MySharedPreparence preparence;
    private ActivityAddEducationBinding binding;
    private FreelancerViewModel freelancerViewModel;
    private String strFrom, strTo, strCurrentEducation = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_education);

        binding.tvBack.setOnClickListener(view -> onBackPressed());

        init();
        binding.tvFrom.setOnClickListener(view -> {
            showCalender(StaticKey.FROM);
        });
        binding.tvTo.setOnClickListener(view -> {
            showCalender(StaticKey.TO);
        });

        binding.cbCurrentEducation.setOnClickListener(view -> {
            if (binding.cbCurrentEducation.isChecked()) {
                strCurrentEducation = "1";
                binding.tvTo.setEnabled(false);
            } else {
                binding.tvTo.setEnabled(true);
                strCurrentEducation = "0";
            }

        });
        binding.btnSave.setOnClickListener(view -> {
            addEducation();
        });
        binding.btnCancel.setOnClickListener(view -> {
            onBackPressed();
        });
        getIntentData();
    }

    private void getIntentData() {
        String identity = getIntent().getStringExtra("identity");
        if (!TextUtils.isEmpty(identity) && identity.equals("update")) {
            binding.etSchoolName.setText("" + freelancerInfo.getInstutationName());
            binding.etLevel.setText("" + freelancerInfo.getEducationLevel());
            binding.etCityTown.setText("" + freelancerInfo.getInstutationLocation());
            strFrom = freelancerInfo.getEducationFrom();
            strTo = freelancerInfo.getEducationTo();
            strCurrentEducation = freelancerInfo.getCurrentStudy();
            binding.tvFrom.setText(" : "+freelancerInfo.getEducationFrom());
            if (!TextUtils.isEmpty(freelancerInfo.getEducationTo())) {
                binding.tvTo.setText(" : "+freelancerInfo.getEducationTo());
            }
            if (!TextUtils.isEmpty(freelancerInfo.getCurrentStudy()) && freelancerInfo.getCurrentStudy().equals("1")) {
                binding.cbCurrentEducation.setChecked(true);
                strCurrentEducation = "1";
            }
            binding.btnMainSave.setText("Update");
        }
    }


    private void addEducation() {

        String instutation_name = binding.etSchoolName.getText().toString().trim();
        String education_level = binding.etLevel.getText().toString().trim();
        String instutation_location = binding.etCityTown.getText().toString().trim();

        if (!TextUtils.isEmpty(instutation_name) && !TextUtils.isEmpty(education_level)
                && !TextUtils.isEmpty(instutation_location) && !TextUtils.isEmpty(strFrom)
                && (!TextUtils.isEmpty(strTo) || strCurrentEducation.equals("1"))) {
            if (strCurrentEducation.equals("1")) {
                strTo = null;
            }
            Freelancer.Education education = new Freelancer.Education(freelancerInfo.getId(), instutation_name,
                    education_level, instutation_location, strFrom, strTo, strCurrentEducation);
            final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
            freelancerViewModel.addEducation(education).observe(this, freelancer -> {
                dialog.dismiss();
                if (freelancer != null && freelancer.getMessage() != null && freelancer.getMessage().equals(StaticKey.SUCCESS)) {
                    freelancer.setPassword(freelancerInfo.getPassword());
                    String freelancerStr = new Gson().toJson(freelancer);
                    preparence.setFreelancerInfo(freelancerStr);
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            if (TextUtils.isEmpty(binding.etSchoolName.getText())) {
                binding.etSchoolName.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etSchoolName.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etLevel.getText())) {
                binding.etLevel.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etLevel.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etCityTown.getText())) {
                binding.etCityTown.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etCityTown.setFocusable(true);
            }
            Toast.makeText(this, getResources().getString(R.string.need_required_information), Toast.LENGTH_SHORT).show();
        }
    }

    private void init() {
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void showCalender(String purpose) {
        final Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                if (purpose.equals(StaticKey.FROM)) {
                    strFrom = sdf.format(myCalendar.getTime());
                    binding.tvFrom.setText(" : " + strFrom);
                } else {
                    strTo = sdf.format(myCalendar.getTime());
                    binding.tvTo.setText(" : " + strTo);
                }
            }

        };
        new DatePickerDialog(this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }
}