package com.proyojanapps.proyojanapps.view.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.view.MyBounceInterpolator;
import com.proyojanapps.proyojanapps.view.activity.freelancer.LoginActivity;

public class SplashActivity extends AppCompatActivity {
    private Handler handler;
    private MySharedPreparence preparence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        preparence = new MySharedPreparence(this);
        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce_animation_top_to_bottom);
        final Animation myAnim1 = AnimationUtils.loadAnimation(this, R.anim.bounce_animation_bootom_to_up);
        final LinearLayout llMainLayout = findViewById(R.id.llMainLayout);
        final LinearLayout llMainLayout1 = findViewById(R.id.llMainLayout1);
        final TextView textView = findViewById(R.id.tvAnimation);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.1, 15);
        myAnim.setInterpolator(interpolator);
        myAnim1.setInterpolator(interpolator);
        llMainLayout.startAnimation(myAnim);
        llMainLayout1.startAnimation(myAnim1);

        handler = new Handler();
        handler.postDelayed(this::checkNextActivity, 5000);

        llMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // llMainLayout.startAnimation(myAnim);
                // llMainLayout1.startAnimation(myAnim1);
                // startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            }
        });
    }


    private void checkNextActivity() {
        if (preparence.getFreelancerInfo()!=null) {
            startActivity(new Intent(this, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }else {
            startActivity(new Intent(this, LoginActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
        finish();
    }

}