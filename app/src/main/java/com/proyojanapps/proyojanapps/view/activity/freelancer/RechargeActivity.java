package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityHelpBinding;
import com.proyojanapps.proyojanapps.databinding.ActivityRechargeBinding;
import com.proyojanapps.proyojanapps.model.Contact;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.view.adapter.ContactListAdapter;
import com.proyojanapps.proyojanapps.view.adapter.MyOfferAdapter;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;
import com.proyojanapps.proyojanapps.viewModel.UtilityViewModel;

import java.util.ArrayList;
import java.util.List;

import okhttp3.internal.Util;

public class RechargeActivity extends AppCompatActivity {
    private ActivityRechargeBinding binding;
    private ContactListAdapter contactListAdapter;
    private List<Contact> list;
    UtilityViewModel utilityViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_recharge);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_recharge);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });
        init();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        } else {
            getContactList();
        }
    }

    private void init() {
        utilityViewModel = ViewModelProviders.of(this).get(UtilityViewModel.class);
        list = new ArrayList<>();
    }


    private void initRV() {
        contactListAdapter = new ContactListAdapter(this, list);
        binding.contactRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.contactRV.setAdapter(contactListAdapter);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {
                Toast.makeText(this, "Contact read permission needed. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", getPackageName(), null));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(intent, 789);
                return false;
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 123);
                return false;
            }
        } else {
            getContactList();
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            getContactList();
        }
    }


    private void getContactList() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        utilityViewModel.getAllContact(this).observe(this, contacts -> {
            dialog.dismiss();
            if (contacts!=null) {
                list = (ArrayList<Contact>) contacts;
                initRV();
            }
        });
    }
}