package com.proyojanapps.proyojanapps.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.CustomeAgentCashOutInSendLayoutBinding;
import com.proyojanapps.proyojanapps.model.Transaction;
import com.proyojanapps.proyojanapps.util.StaticKey;

import java.util.List;


public class AgentSendMoneyAdapter extends RecyclerView.Adapter<AgentSendMoneyAdapter.ViewHolder> {

    private Context context;
    private List<Transaction.AgentSendMoney> agentSendMoneyList;

    public AgentSendMoneyAdapter(Context context, List<Transaction.AgentSendMoney> agentSendMoneyList) {
        this.context = context;
        this.agentSendMoneyList = agentSendMoneyList;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomeAgentCashOutInSendLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.custome_agent_cash_out_in_send_layout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Transaction.AgentSendMoney sendMoney = agentSendMoneyList.get(position);

        viewHolder.binding.tvName.setText(sendMoney.getName());
        viewHolder.binding.tvAmount.setText(context.getString(R.string.amount) + " : " + String.format("%.2f", sendMoney.getAccountpay() != null ?
                Double.valueOf(sendMoney.getAccountpay()) : "0") + " " + context.getString(R.string.ta));
        viewHolder.binding.tvNumber.setText(context.getString(R.string.number) + " : " + sendMoney.getPhone());
        viewHolder.binding.tvDate.setText(context.getString(R.string.date) + " : " + sendMoney.getActiveDate());
        viewHolder.binding.tvNote.setText(context.getString(R.string.refer_id) + " : " + sendMoney.getOwnreferid());
        viewHolder.binding.tvStatus.setText("" + sendMoney.getStatus());
        if (sendMoney.getStatus() != null && sendMoney.getStatus().equals(StaticKey.UNPAID)) {
            viewHolder.binding.tvStatus.setTextColor(Color.parseColor("#E81505"));
        } else {
            viewHolder.binding.tvStatus.setTextColor(Color.parseColor("#2CA01B"));
        }

    }

    @Override
    public int getItemCount() {
        return agentSendMoneyList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private CustomeAgentCashOutInSendLayoutBinding binding;

        public ViewHolder(CustomeAgentCashOutInSendLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private void loadUrl(String url) {
//        Intent intent = new Intent(context, WebViewActivity.class).putExtra("url", url);
//        context.startActivity(intent);
    }
}