package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityUpdateWorkTimeBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.activity.BaseActivity;

public class UpdateWorkTimeActivity extends BaseActivity {
    private Freelancer freelancerInfo;
    private MySharedPreparence preparence;
    private ActivityUpdateWorkTimeBinding binding;
    private double amount;
    private String identity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_update_work_time);
        binding.tvBack.setOnClickListener(view -> onBackPressed());
        amount = getIntent().getDoubleExtra("amount", 0);
        identity = getIntent().getStringExtra("identity");
        init();
        if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.WITHDRAW_BALANCE)) {
            binding.etMoney.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
        } else {
            binding.etMoney.setText(amount + " " + getString(R.string.ta));
            binding.etMoney.setEnabled(false);
        }

        binding.btnSave.setOnClickListener(view -> {
            if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.WITHDRAW_BALANCE)) {
                withdrawBalance();
            } else {
                submit();
            }
        });
    }

    private void withdrawBalance() {
        String money = binding.etMoney.getText().toString().trim();
        String password = binding.etPassword.getText().toString().trim();

        if (!TextUtils.isEmpty(password) && !TextUtils.isEmpty(money)) {
            if (password.equals(freelancerInfo.getPassword())) {
                double newBalance = amount - Double.valueOf(money);
                if (newBalance >= 0) {
                    startActivity(new Intent(this, ConfirmTransactionActivity.class)
                            .putExtra("amount", money)
                            .putExtra("withdrawableamount", "" + amount)
                            .putExtra("identity", StaticKey.WITHDRAW_BALANCE));
                } else {
                    binding.etMoney.setError(getString(R.string.insufficient_balance));
                }
            } else {
                binding.etPassword.setError(getString(R.string.invalid));
            }

        } else {
            if (TextUtils.isEmpty(money)) {
                binding.etPassword.setError(getString(R.string.required_field_cannot_be_empty));
            }
            if (TextUtils.isEmpty(password)) {
                binding.etMoney.setError(getString(R.string.required_field_cannot_be_empty));
            }
            Toast.makeText(this, "" + getString(R.string.invalid), Toast.LENGTH_SHORT).show();
        }
    }

    private void submit() {
        String password = binding.etPassword.getText().toString().trim();

        if (!TextUtils.isEmpty(password) && password.equals(freelancerInfo.getPassword())) {
            startActivity(new Intent(this, ConfirmTransactionActivity.class)
                    .putExtra("amount", "" + amount)
                    .putExtra("identity", StaticKey.RENEW_BALANCE));
        } else {
            binding.etPassword.setError(getString(R.string.invalid));
            Toast.makeText(this, "" + getString(R.string.invalid), Toast.LENGTH_SHORT).show();
        }
    }

    private void init() {
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
    }
}