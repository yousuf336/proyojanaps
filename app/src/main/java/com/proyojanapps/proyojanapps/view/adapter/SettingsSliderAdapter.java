package com.proyojanapps.proyojanapps.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ModelSettingsSliderItemBinding;
import com.proyojanapps.proyojanapps.model.Slider;
import com.proyojanapps.proyojanapps.model.Testimonial;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.List;


public class SettingsSliderAdapter extends SliderViewAdapter<SettingsSliderAdapter.ViewHolder> {

    private Context context;
    private List<Testimonial> testimonialList;

    public SettingsSliderAdapter(Context context, List<Testimonial> testimonialList) {
        this.context = context;
        this.testimonialList = testimonialList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        ModelSettingsSliderItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.model_settings_slider_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Testimonial testimonial = testimonialList.get(position);

        viewHolder.binding.tvName.setText("" + testimonial.getName());
        viewHolder.binding.tvDescription.setText("" + testimonial.getDescription());
        Glide.with(context).applyDefaultRequestOptions(new RequestOptions()
                .placeholder(R.drawable.ic_image_black_24dp)).load(RetrofitInstance.BASE_URL + "/" +
                testimonial.getImage()).into(viewHolder.binding.profileImageCIV);
    }

    @Override
    public int getCount() {
        return testimonialList.size();
    }

    class ViewHolder extends SliderViewAdapter.ViewHolder {

        private ModelSettingsSliderItemBinding binding;

        public ViewHolder(ModelSettingsSliderItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private void loadUrl(String url) {
//        Intent intent = new Intent(context, WebViewActivity.class).putExtra("url", url);
//        context.startActivity(intent);
    }
}