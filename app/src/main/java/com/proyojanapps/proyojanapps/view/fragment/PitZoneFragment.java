package com.proyojanapps.proyojanapps.view.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.FragmentHomeBinding;
import com.proyojanapps.proyojanapps.databinding.FragmentPitZoneBinding;
import com.proyojanapps.proyojanapps.view.activity.MessageActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PitZoneFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PitZoneFragment extends Fragment {
    private FragmentPitZoneBinding binding;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PitZoneFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PitZoneFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PitZoneFragment newInstance(String param1, String param2) {
        PitZoneFragment fragment = new PitZoneFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pit_zone, container, false);

        setOnClickListener();

        return binding.getRoot();
    }

    private void setOnClickListener() {

        binding.llAdvertising.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.llDoctorAppointment.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.llPitSheba.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.llInfotainment.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.llMakingCareer.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.llResellerOnline.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.llFoodService.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
     binding.llPitRideSharing.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });

        binding.llTourTraveling.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });

        binding.llEntrepreneurClud.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });

        binding.llVolunteerClub.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });

        binding.llBusinessClub.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });

        binding.llJobs.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });

        binding.llPitStore.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });

        binding.llShopping.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });


    }
}