package com.proyojanapps.proyojanapps.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityVerifyAgentNumberBinding;
import com.proyojanapps.proyojanapps.view.activity.agent.AgentProfileActivity;

public class VerifyAgentNumberActivity extends AppCompatActivity {


    ActivityVerifyAgentNumberBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_verify_agent_number);
        binding.btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(VerifyAgentNumberActivity.this, AgentProfileActivity.class));
            }
        });
    }
}