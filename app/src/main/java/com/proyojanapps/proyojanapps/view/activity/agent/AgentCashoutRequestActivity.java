package com.proyojanapps.proyojanapps.view.activity.agent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentCashoutRequestBinding;
import com.proyojanapps.proyojanapps.model.Agent;
import com.proyojanapps.proyojanapps.model.Transaction;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.adapter.CashoutRequestListAdapter;
import com.proyojanapps.proyojanapps.view.callback.CashoutRequestStatus;
import com.proyojanapps.proyojanapps.viewModel.AgentActivitesViewModel;

import java.util.ArrayList;

public class AgentCashoutRequestActivity extends AppCompatActivity implements CashoutRequestStatus {

    private ActivityAgentCashoutRequestBinding binding;
    private AgentActivitesViewModel agentActivitesViewModel;
    private MySharedPreparence preparence;
    private Agent agentInfo;
    private ArrayList<Transaction.AgentCashOutRequest> list;
    private CashoutRequestListAdapter cashoutRequestListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_agent_cashout_request);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        init();
        cashOutRequestList();
        initRV();
    }

    @Override
    public void shortClick(Transaction.AgentCashOutRequest cashOut) {
        if (cashOut != null && cashOut.getStatus() != null && cashOut.getStatus().equals(StaticKey.ACTIVE)) {
            cashOutUnPaid(cashOut);
        } else {
            cashOutPaid(cashOut);
        }
    }

    private void init() {
        list = new ArrayList<>();
        agentActivitesViewModel = ViewModelProviders.of(this).get(AgentActivitesViewModel.class);
        preparence = new MySharedPreparence(this);
        agentInfo = new Gson().fromJson(preparence.getAgentInfo(), Agent.class);
    }

    private void initRV() {
        cashoutRequestListAdapter = new CashoutRequestListAdapter(this, list, this);
        binding.cashoutListRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.cashoutListRV.setAdapter(cashoutRequestListAdapter);
    }

    private void cashOutRequestList() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        agentActivitesViewModel.cashOutRequestList(agentInfo.getAgentReeferId()).observe(this,
                cashOutList -> {
                    dialog.dismiss();
                    if (cashOutList != null) {
                        this.list = (ArrayList<Transaction.AgentCashOutRequest>) cashOutList;
                        initRV();
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void cashOutPaid(Transaction.AgentCashOutRequest cashOutRequest) {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        agentActivitesViewModel.cashoutPaid(agentInfo.getAgentId(), cashOutRequest.getId()).observe(this,
                message -> {
                    dialog.dismiss();
                    if (message != null && message.equals(StaticKey.SUCCESS)) {
                        cashOutRequestList();
                    } else {

                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });


    }

    private void cashOutUnPaid(Transaction.AgentCashOutRequest cashOutRequest) {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        agentActivitesViewModel.cashoutUnPaid(agentInfo.getAgentId(), cashOutRequest.getId()).observe(this,
                message -> {
                    dialog.dismiss();
                    if (message != null && message.equals(StaticKey.SUCCESS)) {
                        cashOutRequestList();
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });

    }


}