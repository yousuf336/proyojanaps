package com.proyojanapps.proyojanapps.view.fragment;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.FragmentHomeBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.Slider;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.view.activity.MessageActivity;
import com.proyojanapps.proyojanapps.view.activity.MainActivity;
import com.proyojanapps.proyojanapps.view.activity.agent.AgentListActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.CashoutActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.MyProfileActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.WalletActivity;
import com.proyojanapps.proyojanapps.view.adapter.HomeSecondSliderAdapter;
import com.proyojanapps.proyojanapps.view.adapter.SliderAdapter;
import com.proyojanapps.proyojanapps.viewModel.SliderViewModel;
import com.proyojanapps.proyojanapps.viewModel.UtilityViewModel;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.visuality.f32.temperature.TemperatureUnit;
import com.visuality.f32.weather.data.entity.Weather;
import com.visuality.f32.weather.manager.WeatherManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import mumayank.com.airlocationlibrary.AirLocation;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    private SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    private FragmentHomeBinding binding;
    private SliderAdapter sliderAdapter;
    private HomeSecondSliderAdapter homeSecondSliderAdapter;
    private ArrayList<Slider> sliderList;
    private SliderViewModel sliderViewModel;
    private UtilityViewModel utilityViewModel;
    private Freelancer freelancer;
    private MySharedPreparence preparence;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private AirLocation airLocation;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HomeFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);

        init();
        //initImageSlider();
        setTopHeaderData();
        setMainServiceData();
        getSliderImageList();
        getNotice();
        setOnClickEvent();
        getCurrentLocation();
        getCurrentWeather(23.8103, 90.4125);
        //getCurrentWeather(37.0902,95.7129);
        return binding.getRoot();
    }

    private void getCurrentLocation() {
        try {
            airLocation = new AirLocation(getActivity(), new AirLocation.Callback() {
                @Override
                public void onSuccess(ArrayList<Location> arrayList) {
                    if (arrayList != null && arrayList.size() > 0)
                        getCurrentWeather(arrayList.get(0).getLatitude(), arrayList.get(0).getLongitude());

                }

                @Override
                public void onFailure(AirLocation.LocationFailedEnum locationFailedEnum) {
                    Log.e("message", "onFailure : ");
                }
            }, true, 5000, "Need location permission");
            airLocation.start();
        } catch (Exception e) {
            Log.e("message", "Exception : ");
        }
    }

    private void getCurrentWeather(double let, double longe) {
        try {
            new WeatherManager("" + getString(R.string.weather_api_key)).getCurrentWeatherByCoordinates(
                    let, // latitude
                    longe, // longitude
                    new WeatherManager.CurrentWeatherHandler() {
                        @Override
                        public void onReceivedCurrentWeather(WeatherManager manager, Weather weather) {
                            // Handle current weather information

                            double currentTemperatureInKelvin = weather.getTemperature().getCurrent()
                                    .getValue(TemperatureUnit.CELCIUS);
                            binding.tvTemparature.setText("" + (int) Math.round(currentTemperatureInKelvin) + "°C");
                        }

                        @Override
                        public void onFailedToReceiveCurrentWeather(WeatherManager manager) {
                            // Handle error
                            Log.e("message", "onFailedToReceiveCurrentWeather : ");
                        }
                    }
            );
        } catch (Exception e) {

        }
    }

    private void setOnClickEvent() {

        if (freelancer.getProfilepic() != null && !freelancer.getProfilepic().equals("")) {
            Glide.with(this).applyDefaultRequestOptions(new RequestOptions()
                    .placeholder(R.drawable.ic_image_circle)).load(freelancer.getProfilepic())
                    .into(binding.profileCIV);
        }
        binding.profileCIV.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MyProfileActivity.class));
        });
        binding.llStory.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.includeStartWork.tvService.setOnClickListener(view -> {
            ((MainActivity) getActivity()).binding.appBarMain.tabs.getTabAt(1).select();
        });
        binding.includeCashOut.tvService.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), CashoutActivity.class));

        });
        binding.includeAgentList.tvService.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), AgentListActivity.class));
        });
        binding.includeBuyAndSellSecond.tvService.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.includeShoppingSecond.tvService.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.includeOutlateSecond.tvService.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });

        binding.includeBuyAndSell.llRoot.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.includeOutlate.llRoot.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.includeNewsportal.llRoot.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.includeGames.llRoot.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.includeWallet.llRoot.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), WalletActivity.class));
        });
        binding.includeShopping.llRoot.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.includeEducation.llRoot.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });
        binding.includeHelpZoone.llRoot.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        });


    }

    private void init() {
        preparence = new MySharedPreparence(getActivity());
        freelancer = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
        binding.tvDate.setText("" + fmt.format(new Date()));
        sliderList = new ArrayList<>();
        sliderViewModel = ViewModelProviders.of((AppCompatActivity) getActivity()).get(SliderViewModel.class);
        utilityViewModel = ViewModelProviders.of((AppCompatActivity) getActivity()).get(UtilityViewModel.class);
    }

    private void setTopHeaderData() {
        binding.tvStoreName.setText("" + getString(R.string.add_story));
        binding.includeGames.tvName.setText("" + getString(R.string.games));
        Glide.with(this).applyDefaultRequestOptions(new RequestOptions()
                .placeholder(R.drawable.ic_image_circle)).load(R.drawable.ic_baseline_games_24)
                .into(binding.includeGames.imageviewCIV);
        binding.includeWallet.tvName.setText("" + getString(R.string.wallet));
        Glide.with(this).applyDefaultRequestOptions(new RequestOptions()
                .placeholder(R.drawable.ic_image_circle)).load(R.drawable.ic_wallet_solid)
                .into(binding.includeWallet.imageviewCIV);
        binding.includeShopping.tvName.setText("" + getString(R.string.shopping));
        Glide.with(this).applyDefaultRequestOptions(new RequestOptions()
                .placeholder(R.drawable.ic_image_circle)).load(R.drawable.ic_baseline_shopping_basket_24)
                .into(binding.includeShopping.imageviewCIV);
        binding.includeOutlate.tvName.setText("" + getString(R.string.outlate));
        Glide.with(this).applyDefaultRequestOptions(new RequestOptions()
                .placeholder(R.drawable.ic_image_circle)).load(R.drawable.ic_baseline_shopping_basket_24)
                .into(binding.includeOutlate.imageviewCIV);
        binding.includeBuyAndSell.tvName.setText("" + getString(R.string.buy_and_sell));
        Glide.with(this).applyDefaultRequestOptions(new RequestOptions()
                .placeholder(R.drawable.ic_image_circle)).load(R.drawable.ic_baseline_shopping_cart_24)
                .into(binding.includeBuyAndSell.imageviewCIV);

        binding.includeEducation.tvName.setText("" + getString(R.string.education));
        Glide.with(this).applyDefaultRequestOptions(new RequestOptions()
                .placeholder(R.drawable.ic_image_circle)).load(R.drawable.ic_education)
                .into(binding.includeEducation.imageviewCIV);
        binding.includeHelpZoone.tvName.setText("" + getString(R.string.p_it_help_zoon));
        Glide.with(this).applyDefaultRequestOptions(new RequestOptions()
                .placeholder(R.drawable.ic_image_circle)).load(R.drawable.ic_baseline_help_24)
                .into(binding.includeHelpZoone.imageviewCIV);
        binding.includeNewsportal.tvName.setText("" + getString(R.string.news_portal));
        Glide.with(this).applyDefaultRequestOptions(new RequestOptions()
                .placeholder(R.drawable.ic_image_circle)).load(R.drawable.ic_news_portal)
                .into(binding.includeNewsportal.imageviewCIV);
    }

    private void setMainServiceData() {
        binding.includeStartWork.tvService.setText("" + getString(R.string.start_work));
        binding.includeCashOut.tvService.setText("" + getString(R.string.cash_out));
        binding.includeAgentList.tvService.setText("" + getString(R.string.agent_list));
        binding.includeBuyAndSellSecond.tvService.setText("" + getString(R.string.buy_and_sell));
        binding.includeShoppingSecond.tvService.setText("" + getString(R.string.shopping));
        binding.includeOutlateSecond.tvService.setText("" + getString(R.string.outlate));

        binding.includeStartWork.tvService.setCompoundDrawablesWithIntrinsicBounds(0,
                R.drawable.ic_baseline_my_work_place_24, 0, 0);
        binding.includeCashOut.tvService.setCompoundDrawablesWithIntrinsicBounds(0,
                R.drawable.cash_in, 0, 0);
        binding.includeAgentList.tvService.setCompoundDrawablesWithIntrinsicBounds(0,
                R.drawable.ic_baseline_agent_24, 0, 0);
        binding.includeBuyAndSellSecond.tvService.setCompoundDrawablesWithIntrinsicBounds(0,
                R.drawable.ic_baseline_shopping_cart_24, 0, 0);
        binding.includeShoppingSecond.tvService.setCompoundDrawablesWithIntrinsicBounds(0,
                R.drawable.ic_baseline_shopping_basket_24, 0, 0);
        binding.includeOutlateSecond.tvService.setCompoundDrawablesWithIntrinsicBounds(0,
                R.drawable.cash_in, 0, 0);

    }

    private void initImageSlider() {
        sliderAdapter = new SliderAdapter(getActivity(), sliderList);
        binding.imageSlider.setSliderAdapter(sliderAdapter);
        binding.imageSlider.setIndicatorAnimation(IndicatorAnimations.WORM);
        binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        binding.imageSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        binding.imageSlider.setScrollTimeInSec(4);
        binding.imageSlider.startAutoCycle();
    }

    private void initImageSecondSlider() {
        homeSecondSliderAdapter = new HomeSecondSliderAdapter(getActivity(), sliderList);
        binding.imageSliderSecond.setSliderAdapter(homeSecondSliderAdapter);
        binding.imageSliderSecond.setIndicatorAnimation(IndicatorAnimations.WORM);
        binding.imageSliderSecond.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        binding.imageSliderSecond.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        binding.imageSliderSecond.setScrollTimeInSec(4);
        binding.imageSliderSecond.startAutoCycle();
    }

    private void getSliderImageList() {
        sliderViewModel.getSliderList().observe(this, sliders -> {
            if (sliders != null && sliders.size() > 0) {

                sliderList = (ArrayList<Slider>) sliders;
                //Toast.makeText(getActivity(), "sliders : "+sliderList.size(), Toast.LENGTH_SHORT).show();
                binding.imageSlider.setVisibility(View.VISIBLE);
                binding.imageSliderSecond.setVisibility(View.VISIBLE);
                initImageSlider();
                initImageSecondSlider();
            } else {
                // Toast.makeText(getActivity(), "sliders : null", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        airLocation.onActivityResult(requestCode, resultCode, data);// ADD THIS LINE INSIDE onActivityResult

        Log.e("message", "onactivity result : ");
        //getCurrentLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        airLocation.onRequestPermissionsResult(requestCode, permissions, grantResults); // ADD THIS LINE INSIDE onRequestPermissionResult
        //getCurrentLocation();
        Log.e("message", "onRequestPermissionsResult : ");
    }

    private void getNotice() {
        utilityViewModel.getNotice().observe(this, notice -> {
            if (notice != null) {
                binding.tvBreakingNews.setText("" + notice);
                binding.tvBreakingNews.setSelected(true);
            } else {
                // Toast.makeText(getActivity(), "sliders : null", Toast.LENGTH_SHORT).show();
            }
        });

    }
}