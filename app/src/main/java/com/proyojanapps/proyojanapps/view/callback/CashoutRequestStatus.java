package com.proyojanapps.proyojanapps.view.callback;

import com.proyojanapps.proyojanapps.model.Transaction;

public interface CashoutRequestStatus {
    void shortClick(Transaction.AgentCashOutRequest cashOut);
}
