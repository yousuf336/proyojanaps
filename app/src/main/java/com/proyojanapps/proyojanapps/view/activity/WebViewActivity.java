package com.proyojanapps.proyojanapps.view.activity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.databinding.DataBindingUtil;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityWebViewBinding;


public class WebViewActivity extends BaseActivity {

    private ActivityWebViewBinding binding;

    private String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_web_view);
        changeStatusBarIconColor();

        if (getIntent().getExtras() != null) {
            url = getIntent().getStringExtra("url");
        }
        binding.swipeRefreshL.setRefreshing(true);


        loadUr();

        binding.backBtnCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.swipeRefreshL.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadUr();
            }
        });
    }


    private ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListener;

    @Override
    public void onStart() {
        super.onStart();

        binding.swipeRefreshL.getViewTreeObserver().addOnScrollChangedListener(mOnScrollChangedListener =
                new ViewTreeObserver.OnScrollChangedListener() {
                    @Override
                    public void onScrollChanged() {
                        if (binding.webViewId.getScrollY() == 0)
                            binding.swipeRefreshL.setEnabled(true);
                        else
                            binding.swipeRefreshL.setEnabled(false);
                    }
                });
    }

    @Override
    public void onStop() {
        binding.swipeRefreshL.getViewTreeObserver().removeOnScrollChangedListener(mOnScrollChangedListener);
        super.onStop();
    }

    private void loadUr() {
//        String urlWithData = yourUrl + "?data=" + theString;
//        webView.loadUrl(urlWithData);
//

        binding.webViewId.loadUrl(url);
        binding.webViewId.getSettings().setJavaScriptEnabled(true);
        binding.webViewId.getSettings().setDisplayZoomControls(true);
        binding.webViewId.getSettings().setLoadsImagesAutomatically(true);
        binding.webViewId.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        binding.webViewId.setWebViewClient(new MyWebViewClient());
    }

    public class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            binding.swipeRefreshL.setRefreshing(false);
        }
    }

    @Override
    public void onBackPressed() {
        if (binding.webViewId.canGoBack()) {
            binding.webViewId.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
