package com.proyojanapps.proyojanapps.view.activity.agent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentSignUpBinding;
import com.proyojanapps.proyojanapps.model.Agent;
import com.proyojanapps.proyojanapps.model.District;
import com.proyojanapps.proyojanapps.model.Division;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.Union;
import com.proyojanapps.proyojanapps.model.Upazilla;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.FilePath;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.view.activity.BaseActivity;
import com.proyojanapps.proyojanapps.view.activity.TermsAndConditionActivity;
import com.proyojanapps.proyojanapps.view.bottomsheet.SelectImageBottomSheet;
import com.proyojanapps.proyojanapps.viewModel.AgentViewModel;
import com.proyojanapps.proyojanapps.viewModel.UtilityViewModel;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AgentSignUpActivity extends BaseActivity implements SelectImageBottomSheet.BottomSheetListener {

    private SelectImageBottomSheet selectImageBottomSheet;
    private static final int REQUEST_TAKE_PHOTO_NID_FONT = 1;
    private static final int REQUEST_SELECT_PHOTO_NID_FONT = 2;
    private static final int REQUEST_TAKE_PHOTO_NID_BACK = 3;
    private static final int REQUEST_SELECT_PHOTO_NID_BACK = 4;
    private static final int REQUEST_TAKE_PHOTO_BANK_SLEEP = 5;
    private static final int REQUEST_SELECT_PHOTO_BANK_SLEEP = 6;
    private int CROP_IMAGE_CODE;
    private ActivityAgentSignUpBinding binding;
    private File nidFontImageFile, nidBackImageFile, bankSleepImageFile;
    private String nidFontPhotoPath, nidBackPhotoPath, bankSleepPhotoPath;

    private MySharedPreparence preferences;
    private AgentViewModel agentViewModel;
    private Freelancer freelancerInfo;
    private ArrayList<Division> divisionList;
    private ArrayList<District> districtList;
    private ArrayList<Upazilla> upazilaList;
    private ArrayList<Union> unionList;
    private int divisionId, districtId, thanaId, unionId;
    private UtilityViewModel utilityViewModel;
    private String dob;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_agent_sign_up);
        init();
        binding.imageViewBack.setOnClickListener(view -> {
            onBackPressed();
        });
        freelancerInfo = new Gson().fromJson(preferences.getFreelancerInfo(), Freelancer.class);
        setData();
        binding.btnSignUp.setOnClickListener(view -> {
                    agentSignUp();
                    // startActivity(new Intent(AgentSignUpActivity.this, VerifyAgentNumberActivity.class))
                }
        );

        binding.tvTermsOfConditions.setOnClickListener(view -> {
            startActivity(new Intent(this, TermsAndConditionActivity.class));
            // .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.tvNidFont.setOnClickListener(view -> openSelectImageBottomSheet(REQUEST_TAKE_PHOTO_NID_FONT, REQUEST_SELECT_PHOTO_NID_FONT));
        binding.tvNidBack.setOnClickListener(view -> openSelectImageBottomSheet(REQUEST_TAKE_PHOTO_NID_BACK, REQUEST_SELECT_PHOTO_NID_BACK));
        binding.tvBankTranjectionSleep.setOnClickListener(view -> openSelectImageBottomSheet(REQUEST_TAKE_PHOTO_BANK_SLEEP, REQUEST_SELECT_PHOTO_BANK_SLEEP));
        binding.cbTermsAndCondition.setOnClickListener(view -> {
            if (binding.cbTermsAndCondition.isChecked()) {
                binding.btnSignUp.setAlpha((float) 1.0);
                binding.btnSignUp.setEnabled(true);
            } else {
                binding.btnSignUp.setAlpha((float) 0.5);
                binding.btnSignUp.setEnabled(false);
            }
        });

        binding.tvDateOfBirth.setOnClickListener(view -> {
            setDataOfBirth();
        });
        setAllDivision();
    }


    private void setData() {
        binding.etFullName.setText("" + freelancerInfo.getName());
        binding.etEmail.setText("" + freelancerInfo.getEmail());
        binding.etPhone.setText("" + freelancerInfo.getPhone());

    }

    private void init() {
        agentViewModel = ViewModelProviders.of(this).get(AgentViewModel.class);
        utilityViewModel = ViewModelProviders.of(this).get(UtilityViewModel.class);
        preferences = new MySharedPreparence(this);


    }

    private void agentSignUp() {
        //   freelancer_id  , dateofbirth, gender, occupation, address, division, district, thana, union, nidfront, nidback, sendernumber,transectionid,trans_slip,amount

        String occupation = binding.etOccupation.getText().toString().trim();
        String address = binding.etAddress.getText().toString().trim();
        String sendernumber = binding.etSenderNumber.getText().toString().trim();
        String amount = binding.etAmount.getText().toString().trim();
        String transectionid = binding.etPaymentTransactionId.getText().toString().trim();

        if (!TextUtils.isEmpty(dob) && !TextUtils.isEmpty(occupation) && !TextUtils.isEmpty(address)
                && !TextUtils.isEmpty(sendernumber) && !TextUtils.isEmpty(transectionid) && !TextUtils.isEmpty(amount)
                && divisionId > 0 && districtId > 0 && thanaId > 0&&binding.cbTermsAndCondition.isChecked()
                && nidFontImageFile!=null&& nidBackImageFile!=null) {
            Agent a = new Agent(freelancerInfo.getId(), dob, getGender(), occupation, address, ""+divisionId, ""+districtId, ""+thanaId, ""+unionId,
                    sendernumber, transectionid, amount, nidFontImageFile, nidBackImageFile, bankSleepImageFile);
            final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
            agentViewModel.agentRegistration(a).observe(this, message -> {
                dialog.dismiss();
                    if (message!=null&&message.toLowerCase().equals("success")) {

                        startActivity(new Intent(AgentSignUpActivity.this, AgentLoginActivity.class));
                        // .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    } else {
                        Toast.makeText(this, "" + getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
            });
        }else {
            if (TextUtils.isEmpty(binding.etOccupation.getText())) {
                binding.etOccupation.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etOccupation.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etAddress.getText())) {
                binding.etAddress.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etAddress.setFocusable(true);
            } if (TextUtils.isEmpty(binding.etPaymentTransactionId.getText())) {
                binding.etPaymentTransactionId.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etPaymentTransactionId.setFocusable(true);
            }if (TextUtils.isEmpty(binding.etSenderNumber.getText())) {
                binding.etSenderNumber.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etSenderNumber.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etAmount.getText())) {
                binding.etAmount.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etAmount.setFocusable(true);
            }
//            if (TextUtils.isEmpty(binding.etNidFontCopy.getText())) {
//                binding.etNidFontCopy.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
//                binding.etNidFontCopy.setFocusable(true);
//            }
            Toast.makeText(this, getResources().getString(R.string.need_required_information), Toast.LENGTH_SHORT).show();
        }



    }

    private void setDataOfBirth() {
        final Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                dob = sdf.format(myCalendar.getTime());
                binding.tvDateOfBirth.setText(sdf.format(myCalendar.getTime()));
            }

        };
        new DatePickerDialog(this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void setDistrict() {
        ArrayList<String> districtListStr = new ArrayList<>();
        districtListStr.add("Select District*");
        if (districtList != null)
            for (District d : districtList) {
                districtListStr.add(d.getName());
            }
        ArrayAdapter districtAdapter = new ArrayAdapter(this, R.layout.spinner_layout, R.id.tvContent, districtListStr);
        binding.spDistrict.setAdapter(districtAdapter);
        binding.spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position > 0) {
                    districtId = AgentSignUpActivity.this.districtList.get(position - 1).getDistrictId();
                    final Dialog dialog = CustomLoadingDialog.createLoadingDialog(AgentSignUpActivity.this);
                    utilityViewModel.getUpazilaList(districtId).observe(AgentSignUpActivity.this, upList -> {
                        dialog.dismiss();
                        if (upList != null && upList.size() > 0) {
                            upazilaList = new ArrayList<>();
                            upazilaList = (ArrayList<Upazilla>) upList;
                            setUpazilaList();
                        } else {

                        }
                    });

                } else {
                    districtId = 0;
                    upazilaList = new ArrayList<>();
                    setUpazilaList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void setUpazilaList() {
        ArrayList<String> upazilatListStr = new ArrayList<>();
        upazilatListStr.add("Select Upazila");
        if (upazilaList != null)
            for (Upazilla upazilla : upazilaList) {
                upazilatListStr.add(upazilla.getName());
            }
        ArrayAdapter upazilaAdapter = new ArrayAdapter(this, R.layout.spinner_layout, R.id.tvContent, upazilatListStr);
        binding.spThana.setAdapter(upazilaAdapter);
        binding.spThana.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    thanaId = AgentSignUpActivity.this.upazilaList.get(position - 1).getUpazillaId();
                    final Dialog dialog = CustomLoadingDialog.createLoadingDialog(AgentSignUpActivity.this);
                    utilityViewModel.getUnionList(thanaId).observe(AgentSignUpActivity.this, uList -> {
                        dialog.dismiss();
                        if (uList != null && uList.size() > 0) {
                            unionList = new ArrayList<>();
                            unionList = (ArrayList<Union>) uList;
                            setUnionList();
                        } else {

                        }
                    });

                } else {
                    thanaId = 0;
                    unionList = new ArrayList<>();
                    setUnionList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void setUnionList() {
        ArrayList<String> unionListStr = new ArrayList<>();
        unionListStr.add("Select Union");
        if (unionList != null)
            for (Union union : unionList) {
                unionListStr.add(union.getName());
            }
        ArrayAdapter unionAdapter = new ArrayAdapter(this, R.layout.spinner_layout, R.id.tvContent, unionListStr);
        binding.spUnion.setAdapter(unionAdapter);
        binding.spUnion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    unionId = AgentSignUpActivity.this.unionList.get(position - 1).getId();
                } else {
                    unionId = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setAllDivision() {
        ArrayList<String> divisionListStr = new ArrayList<>();
        divisionListStr.add("Select Division");
        ArrayAdapter divisionAdapter = new ArrayAdapter(this, R.layout.spinner_layout, R.id.tvContent, divisionListStr);
        binding.spDivision.setAdapter(divisionAdapter);
        binding.spDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    divisionId = AgentSignUpActivity.this.divisionList.get(position - 1).getDivisionId();
                    //Toast.makeText(AgentSignUpActivity.this, "divisionId "+divisionId, Toast.LENGTH_SHORT).show();
                    final Dialog dialog = CustomLoadingDialog.createLoadingDialog(AgentSignUpActivity.this);
                    utilityViewModel.getDistrictList(divisionId).observe(AgentSignUpActivity.this, distList -> {
                        dialog.dismiss();
                        if (distList != null && distList.size() > 0) {
                            districtList = new ArrayList<>();
                            districtList = (ArrayList<District>) distList;
                            setDistrict();
                        } else {

                        }
                    });

                } else {
                    divisionId = 0;
                    districtList = new ArrayList<>();
                    setDistrict();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        utilityViewModel.getAllDivision().observe(this, dList -> {
            dialog.dismiss();
            if (dList != null && dList.size() > 0) {
                divisionList = new ArrayList<>();
                this.divisionList = (ArrayList<Division>) dList;
                //Toast.makeText(this, "divisionList "+divisionList.size(), Toast.LENGTH_SHORT).show();
                for (Division d : divisionList) {
                    divisionListStr.add(d.getName());
                }
                divisionAdapter.notifyDataSetChanged();
            } else {
                // Toast.makeText(this, "divisionList null", Toast.LENGTH_SHORT).show();
                //setDistrict(0);
            }
        });

    }

    private String getGender() {
        String gender = null;
        if (binding.rbMale.isChecked()) {
            return "Male";
        } else
            return "Female";

    }

    private void openSelectImageBottomSheet(int requestCodeCamera, int requestCodeGallery) {
        selectImageBottomSheet = new SelectImageBottomSheet(this, requestCodeCamera, requestCodeGallery);
        selectImageBottomSheet.show(getSupportFragmentManager(), "selectImage");
    }

    @Override
    public void onCameraButtonClicked(int requestCodeCamera) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent(requestCodeCamera);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, requestCodeCamera);
            }
        } else {
            dispatchTakePictureIntent(requestCodeCamera);
        }
        selectImageBottomSheet.dismiss();
    }

    @Override
    public void onGalleryButtonClicked(int requestCodeGallery) {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(Intent.createChooser(galleryIntent, "Select Photo"), requestCodeGallery);
        selectImageBottomSheet.dismiss();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_TAKE_PHOTO_NID_FONT || requestCode == REQUEST_TAKE_PHOTO_NID_BACK ||
                requestCode == REQUEST_TAKE_PHOTO_BANK_SLEEP) {
            onCameraButtonClicked(requestCode);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == REQUEST_TAKE_PHOTO_NID_FONT || requestCode == REQUEST_TAKE_PHOTO_NID_BACK ||
                requestCode == REQUEST_TAKE_PHOTO_BANK_SLEEP) && resultCode == RESULT_OK) {

            CROP_IMAGE_CODE = requestCode;
            switch (requestCode) {
                case REQUEST_TAKE_PHOTO_NID_FONT:
                    CropImage.activity(Uri.fromFile(new File(nidFontPhotoPath)))
                            .setAspectRatio(1, 1)
                            .start(this);
                    break;

                case REQUEST_TAKE_PHOTO_NID_BACK:
                    CropImage.activity(Uri.fromFile(new File(nidBackPhotoPath)))
                            .setAspectRatio(1, 1)
                            .start(this);
                    break;
                case REQUEST_TAKE_PHOTO_BANK_SLEEP:
                    CropImage.activity(Uri.fromFile(new File(bankSleepPhotoPath)))
                            .setAspectRatio(1, 1)
                            .start(this);
                    break;
            }
        } else if ((requestCode == REQUEST_SELECT_PHOTO_NID_FONT || requestCode == REQUEST_SELECT_PHOTO_NID_BACK ||
                requestCode == REQUEST_SELECT_PHOTO_BANK_SLEEP && resultCode == RESULT_OK) && data != null) {
            CROP_IMAGE_CODE = requestCode;
            Uri imageUrl = data.getData();
            CropImage.activity(imageUrl)
                    .setAspectRatio(1, 1)
                    .start(this);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Uri resultUri = null;
            if (result != null) {
                resultUri = result.getUri();
            }
            getImageFile(FilePath.getPath(this, resultUri));

            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (bitmap != null) {
                setImageToView(bitmap, CROP_IMAGE_CODE);
            }

        }
    }

    private void setImageToView(Bitmap bitmap, int code) {
        String[] imageNameFont = new String[10], imageNameBack = new String[10], imageNameBank = new String[10];
        switch (code) {
            case REQUEST_SELECT_PHOTO_NID_FONT:
            case REQUEST_TAKE_PHOTO_NID_FONT:
                binding.imageNidFont.setImageBitmap(bitmap);
                if (nidFontImageFile != null)
                    imageNameFont = nidFontImageFile.toString().split("/");
                if (imageNameFont != null && imageNameFont.length > 0)
                    binding.etNidFontCopy.setText("" + imageNameFont[imageNameFont.length - 1]);
                break;
            case REQUEST_SELECT_PHOTO_NID_BACK:
            case REQUEST_TAKE_PHOTO_NID_BACK:
                binding.imageNidBack.setImageBitmap(bitmap);
                if (nidBackImageFile != null)
                    imageNameBack = nidBackImageFile.toString().split("/");
                if (imageNameBack != null && imageNameBack.length > 0)
                    binding.etNidBackCopy.setText("" + imageNameBack[imageNameBack.length - 1]);
                break;
            case REQUEST_SELECT_PHOTO_BANK_SLEEP:
            case REQUEST_TAKE_PHOTO_BANK_SLEEP:
                binding.imageBank.setImageBitmap(bitmap);
                if (bankSleepImageFile != null)
                    imageNameBank = bankSleepImageFile.toString().split("/");
                if (imageNameBank != null && imageNameBank.length > 0)
                    binding.etBankTransactionSlip.setText("" + imageNameBank[imageNameBank.length - 1]);
        }

    }

    private void getImageFile(String photoPath) {
        switch (CROP_IMAGE_CODE) {
            case REQUEST_SELECT_PHOTO_NID_FONT:
            case REQUEST_TAKE_PHOTO_NID_FONT:
                nidFontImageFile = new File(photoPath);
                break;
            case REQUEST_SELECT_PHOTO_NID_BACK:
            case REQUEST_TAKE_PHOTO_NID_BACK:
                nidBackImageFile = new File(photoPath);
                break;
            case REQUEST_SELECT_PHOTO_BANK_SLEEP:
            case REQUEST_TAKE_PHOTO_BANK_SLEEP:
                bankSleepImageFile = new File(photoPath);
                break;
        }
    }


    private File createImageFile(int code) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
        switch (code) {
            case REQUEST_TAKE_PHOTO_NID_FONT:
                nidFontPhotoPath = image.getAbsolutePath();
                break;

            case REQUEST_TAKE_PHOTO_NID_BACK:
                nidBackPhotoPath = image.getAbsolutePath();
                break;
            case REQUEST_TAKE_PHOTO_BANK_SLEEP:
                bankSleepPhotoPath = image.getAbsolutePath();
                break;
        }
        return image;
    }

    private void dispatchTakePictureIntent(int requestCodeCamera) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile(requestCodeCamera);
            } catch (IOException ignored) {
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.proyojanapps.proyojanapps.customerfileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, requestCodeCamera);
            }
        }
    }
}