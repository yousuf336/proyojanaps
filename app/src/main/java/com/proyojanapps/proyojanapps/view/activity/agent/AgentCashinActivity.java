package com.proyojanapps.proyojanapps.view.activity.agent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentCashinBinding;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentCashoutBinding;
import com.proyojanapps.proyojanapps.model.Agent;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.activity.BaseActivity;
import com.proyojanapps.proyojanapps.viewModel.AgentActivitesViewModel;
import com.proyojanapps.proyojanapps.viewModel.AgentViewModel;

public class AgentCashinActivity extends BaseActivity {

    private ActivityAgentCashinBinding binding;
    private AgentActivitesViewModel agentActivitesViewModel;
    private AgentViewModel agentViewModel;
    private MySharedPreparence preparence;
    private Freelancer freelancerInfo;
    private Agent agentInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_agent_cashin);

        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        init();
        binding.btnSubmit.setOnClickListener(view -> {
            checkInput();
        });
    }


    private void checkInput() {
        String senderNum = binding.etSenderNumber.getText().toString();
        String amount = binding.etAmount.getText().toString();
        String note = binding.etNote.getText().toString();
        String transNum = binding.etTransNumber.getText().toString();
        String password = binding.etPassword.getText().toString();

        if (!TextUtils.isEmpty(senderNum) && !TextUtils.isEmpty(amount) && !TextUtils.isEmpty(password)
                && !TextUtils.isEmpty(note)   && !TextUtils.isEmpty(transNum)) {
            if (freelancerInfo != null && freelancerInfo.getPassword() != null && freelancerInfo.getPassword().equals(password)) {
                agentLogin(senderNum,transNum ,amount, note,password);
            } else {
                binding.etPassword.setFocusable(true);
                binding.etPassword.setError("" + getString(R.string.password_mismatch));
            }
        } else {
            if (TextUtils.isEmpty(binding.etSenderNumber.getText())) {
                binding.etSenderNumber.setFocusable(true);
                binding.etSenderNumber.setError("" + getString(R.string.required_field_cannot_be_empty));
            }
            if (TextUtils.isEmpty(binding.etPassword.getText())) {
                binding.etPassword.setFocusable(true);
                binding.etPassword.setError("" + getString(R.string.required_field_cannot_be_empty));
            }
            if (TextUtils.isEmpty(binding.etAmount.getText())) {
                binding.etAmount.setFocusable(true);
                binding.etAmount.setError("" + getString(R.string.required_field_cannot_be_empty));
            }
            if (TextUtils.isEmpty(binding.etNote.getText())) {
                binding.etNote.setFocusable(true);
                binding.etNote.setError("" + getString(R.string.required_field_cannot_be_empty));
            }
            if (TextUtils.isEmpty(binding.etTransNumber.getText())) {
                binding.etTransNumber.setFocusable(true);
                binding.etTransNumber.setError("" + getString(R.string.required_field_cannot_be_empty));
            }
            Toast.makeText(this, getResources().getString(R.string.need_required_information), Toast.LENGTH_SHORT).show();
        }
    }

    private void init() {
        agentActivitesViewModel = ViewModelProviders.of(this).get(AgentActivitesViewModel.class);
        agentViewModel = ViewModelProviders.of(this).get(AgentViewModel.class);
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
        agentInfo = new Gson().fromJson(preparence.getAgentInfo(), Agent.class);
    }

    private void agentLogin(String number, String trans_number,String amount, String note,String password) {
        if (agentInfo != null) {
            final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
            agentViewModel.logIn(freelancerInfo.getPhone(), password).observe(this, agent -> {
                dialog.dismiss();
                if (agent != null) {
                    String agentStr = new Gson().toJson(agent);
                    preparence.setAgentInfo(agentStr);
                    startActivity(new Intent(this, AgentConfirmTransectionActivity.class)
                            .putExtra("amount", amount)
                            .putExtra("password", password)
                            .putExtra("number", number)
                            .putExtra("transNumber", trans_number)
                            .putExtra("note", note)
                            .putExtra("identity", StaticKey.CASH_IN_AGENT));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                } else {
                    Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
        }
    }
}