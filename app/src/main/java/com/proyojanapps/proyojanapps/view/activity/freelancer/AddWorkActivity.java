package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAddWorkBinding;
import com.proyojanapps.proyojanapps.databinding.ActivityMyProfileBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AddWorkActivity extends AppCompatActivity {
    private Freelancer freelancerInfo;
    private MySharedPreparence preparence;
    private ActivityAddWorkBinding binding;
    private FreelancerViewModel freelancerViewModel;
    private String strFrom, strTo, strCurrentJob = "0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_work);
        binding.tvBack.setOnClickListener(view -> onBackPressed());

        init();
        binding.tvFrom.setOnClickListener(view -> {
            showCalender(StaticKey.FROM);
        });
        binding.tvTo.setOnClickListener(view -> {
            showCalender(StaticKey.TO);
        });

        binding.cbCurrentWork.setOnClickListener(view -> {
            if (binding.cbCurrentWork.isChecked()) {
                strCurrentJob = "1";
                binding.tvTo.setEnabled(false);
            } else {
                binding.tvTo.setEnabled(true);
                strCurrentJob = "0";
            }

        });
        binding.btnSave.setOnClickListener(view -> {
            addWork();
        });
        binding.btnCancel.setOnClickListener(view -> {
            onBackPressed();
        });

        getIntentData();

    }

    private void getIntentData() {
        String identity = getIntent().getStringExtra("identity");
        if (!TextUtils.isEmpty(identity) && identity.equals("update")) {
            binding.etCompanyName.setText("" + freelancerInfo.getCompanyName());
            binding.etJobTitle.setText("" + freelancerInfo.getJobTitle());
            binding.etCityTown.setText("" + freelancerInfo.getJobLocation());
            binding.etJobDescription.setText("" + freelancerInfo.getJobDescription());
            strFrom = freelancerInfo.getJobFrom();
            strTo = freelancerInfo.getJobTo();
            strCurrentJob = freelancerInfo.getCurrentJob();
            binding.tvFrom.setText(" : "+ freelancerInfo.getJobFrom());
            if (!TextUtils.isEmpty(freelancerInfo.getJobTo())) {
                binding.tvTo.setText(" : "+freelancerInfo.getJobTo());
            }
            if (!TextUtils.isEmpty(freelancerInfo.getCurrentJob()) && freelancerInfo.getCurrentJob().equals("1")) {
                binding.cbCurrentWork.setChecked(true);
                strCurrentJob = "1";
            }
            binding.btnMainSave.setText("Update");
        }
    }

    private void addWork() {

        String company_name = binding.etCompanyName.getText().toString().trim();
        String job_title = binding.etJobTitle.getText().toString().trim();
        String job_location = binding.etCityTown.getText().toString().trim();
        String job_description = binding.etJobDescription.getText().toString().trim();

        if (!TextUtils.isEmpty(company_name) && !TextUtils.isEmpty(job_title)
                && !TextUtils.isEmpty(job_location) && !TextUtils.isEmpty(job_description) && !TextUtils.isEmpty(strFrom)
                && (!TextUtils.isEmpty(strTo) || strCurrentJob.equals("1"))) {
            if (strCurrentJob.equals("1")) {
                strTo = null;
            }
            Freelancer.Work work = new Freelancer.Work(freelancerInfo.getId(), company_name, job_title,
                    job_location, job_description, strFrom, strTo, strCurrentJob);
            final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
            freelancerViewModel.addWork(work).observe(this, freelancer -> {
                dialog.dismiss();
                if (freelancer != null && freelancer.getMessage() != null && freelancer.getMessage().equals(StaticKey.SUCCESS)) {
                    freelancer.setPassword(freelancerInfo.getPassword());
                    String freelancerStr = new Gson().toJson(freelancer);
                    preparence.setFreelancerInfo(freelancerStr);
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            if (TextUtils.isEmpty(binding.etCompanyName.getText())) {
                binding.etCompanyName.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etCompanyName.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etJobTitle.getText())) {
                binding.etJobTitle.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etJobTitle.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etCityTown.getText())) {
                binding.etCityTown.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etCityTown.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etJobDescription.getText())) {
                binding.etJobDescription.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etJobDescription.setFocusable(true);
            }
            Toast.makeText(this, getResources().getString(R.string.need_required_information), Toast.LENGTH_SHORT).show();
        }
    }

    private void init() {
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void showCalender(String purpose) {
        final Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                if (purpose.equals(StaticKey.FROM)) {
                    strFrom = sdf.format(myCalendar.getTime());
                    binding.tvFrom.setText(" : " + strFrom);
                } else {
                    strTo = sdf.format(myCalendar.getTime());
                    binding.tvTo.setText(" : " + strTo);
                }
            }

        };
        new DatePickerDialog(this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }
}