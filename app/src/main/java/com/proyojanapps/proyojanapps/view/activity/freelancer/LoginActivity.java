package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityLoginBinding;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.view.activity.BaseActivity;
import com.proyojanapps.proyojanapps.view.activity.MainActivity;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;

public class LoginActivity extends BaseActivity {


    ActivityLoginBinding binding;
    MySharedPreparence preferences;
    FreelancerViewModel freelancerViewModel;
    private boolean passwordVisibilityFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        init();

        binding.btnLogin.setOnClickListener(view -> {
            String userName = binding.etPhoneOrEmail.getText().toString().trim();
            String password = binding.etPassword.getText().toString().trim();
            freelancerLogin(userName, password);

        });
        binding.ivPasswordVisibility.setOnClickListener(view -> {
            if (passwordVisibilityFlag) {
                passwordVisibilityFlag = false;
                binding.ivPasswordVisibility.setImageResource(R.drawable.ic_baseline_visibility_off_24);
                binding.etPassword.setInputType(0x00000081);


            } else {
                binding.ivPasswordVisibility.setImageResource(R.drawable.show_password_icon_24);
                passwordVisibilityFlag = true;
                binding.etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            }
        });

        binding.tvRegistration.setOnClickListener(view -> {
            startActivity(new Intent(this, SignUpActivity.class));
            //  .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        binding.tvForgetPassword.setOnClickListener(view -> {
            showCustomeDialog(this);
            dialogCustomeBinding.tvHeading.setText(getResources().getString(R.string.enter_your_phone_number_here));
            dialogCustomeBinding.etNumber.setHint(getString(R.string.phone));
            dialogCustomeBinding.btnNext.setText(getString(R.string.send_otp));
            dialogCustomeBinding.btnNext.setOnClickListener(view1 -> {
                if (!TextUtils.isEmpty(dialogCustomeBinding.etNumber.getText().toString().trim())) {
                    final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
                    freelancerViewModel.forgetPhoneverify(dialogCustomeBinding.etNumber.getText().toString().trim())
                            .observe(this, freelancer -> {
                                dialog.dismiss();
                                if (freelancer != null) {
                                    if (!TextUtils.isEmpty(freelancer.getMessage()) && freelancer.getMessage().equals("success")) {
                                        startActivity(new Intent(LoginActivity.this, VerifyNumberActivity.class)
                                                .putExtra("id", freelancer.getId())
                                                .putExtra("identity", "forgetpassword")
                                                .putExtra("code", freelancer.getSecurityPin())
                                                .putExtra("number", dialogCustomeBinding.etNumber.getText().toString().trim()));
                                        dialogCustome.dismiss();
                                    } else {
                                        Toast.makeText(this, "" + getString(R.string.invalid_number),
                                                Toast.LENGTH_SHORT).show();
                                        dialogCustomeBinding.etNumber.setError("" + getString(R.string.invalid_number));
                                    }
                                } else {
                                }
                            });
                } else {
                    dialogCustomeBinding.etNumber.setFocusable(true);
                    dialogCustomeBinding.etNumber.setError("Enter Your Phone Number");

                }
            });

        });
    }

    private void init() {
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
        preferences = new MySharedPreparence(this);

    }

    private void freelancerLogin(String username, String password) {
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
            freelancerViewModel.logIn(username, password).observe(this, freelancer -> {
                dialog.dismiss();
                if (freelancer != null && freelancer.getMessage() != null) {
                    if (freelancer.getMessage().equals("success")) {
                        freelancer.setPassword(password);
                        String freelancerInfo = new Gson().toJson(freelancer);
                        preferences.setFreelancerInfo(freelancerInfo);
                        if (freelancer.getSecurityCheck() == null && !TextUtils.isEmpty(freelancer.getStatus()) && freelancer.getStatus().equals("0")) {
                            startActivity(new Intent(LoginActivity.this, VerifyNumberActivity.class)
                                    .putExtra("id", freelancer.getId())
                                    .putExtra("code", freelancer.getSecurityPin())
                                    .putExtra("identity", "phoneverify")
                                    .putExtra("number", freelancer.getPhone()));
                            // .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        } else {
                            startActivity(new Intent(LoginActivity.this, MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.invalid_credentials), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            if (TextUtils.isEmpty(binding.etPhoneOrEmail.getText())) {
                binding.etPhoneOrEmail.setFocusable(true);
                binding.etPhoneOrEmail.setError("Enter Phone Or Email");
            }
            if (TextUtils.isEmpty(binding.etPassword.getText())) {
                binding.etPassword.setFocusable(true);
                binding.etPassword.setError("Enter Password");
            }
            Toast.makeText(this, getResources().getString(R.string.need_required_information), Toast.LENGTH_SHORT).show();
        }
    }


}