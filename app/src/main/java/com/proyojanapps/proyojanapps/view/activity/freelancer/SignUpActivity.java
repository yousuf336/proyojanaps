package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivitySignUpBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.activity.TermsAndConditionActivity;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;

public class SignUpActivity extends AppCompatActivity {

    ActivitySignUpBinding binding;
    MySharedPreparence preferences;
    FreelancerViewModel freelancerViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        init();
        binding.imageViewBack.setOnClickListener(view -> {
            onBackPressed();
        });
//        binding.btnSignUp.setOnClickListener(view -> startActivity(
//                new Intent(SignUpActivity.this, VerifyNumberActivity.class)));


        binding.btnSignUp.setOnClickListener(view -> {

            freelancerLoginSignUp();

        });
        binding.tvTermsOfConditions.setOnClickListener(view -> {
            startActivity(new Intent(SignUpActivity.this, TermsAndConditionActivity.class));
            // .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        binding.cbTermsAndCondition.setOnClickListener(view -> {
            if (binding.cbTermsAndCondition.isChecked()) {
                binding.btnSignUp.setAlpha((float) 1.0);
                binding.btnSignUp.setEnabled(true);
            } else {
                binding.btnSignUp.setAlpha((float) 0.5);
                binding.btnSignUp.setEnabled(false);
            }
        });

    }

    private void init() {
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
        preferences = new MySharedPreparence(this);

    }

    private void freelancerLoginSignUp() {
        String userName = binding.etFullName.getText().toString().trim();
        String phone = binding.etPhone.getText().toString().trim();
        String email = binding.etEmail.getText().toString().trim();
        String refferId = binding.etReferr.getText().toString().trim();
        String password = binding.etPassword.getText().toString().trim();
        String confirmPassword = binding.etConfirmPassword.getText().toString().trim();
        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(phone) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(refferId)
                && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(confirmPassword) && binding.cbTermsAndCondition.isChecked()) {
            if (password.equals(confirmPassword)) {
                Freelancer f = new Freelancer(userName, email, phone, refferId, password, StaticKey.TERMS_AND_CONDITION);


                final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
                freelancerViewModel.frelancerRegistration(f).observe(this, freelancer -> {
                    boolean flag = true;
                    if (freelancer != null) {
                        if (!TextUtils.isEmpty(freelancer.getEmailExist()) && freelancer.getEmailExist().equals(StaticKey.EMAIL_EXIST)) {
                            binding.etEmail.setError("" + getResources().getString(R.string.email_already_exist));
                            flag = false;
                        }
                        if (!TextUtils.isEmpty(freelancer.getPhoneExist()) && freelancer.getPhoneExist().equals(StaticKey.PHONE_EXIST)) {
                            binding.etPhone.setError("" + getResources().getString(R.string.phone_already_exist));
                            flag = false;
                        }
                        if (!TextUtils.isEmpty(freelancer.getRefereeIdValidation()) && freelancer.getRefereeIdValidation().equals(StaticKey.REFFER_ID_NOT_VALID)) {
                            binding.etReferr.setError("" + getResources().getString(R.string.referral_id_invalid));
                            flag = false;
                        }
                        if (flag) {
                            freelancer.setEmail(f.getEmail());
                            freelancer.setPhoneExist(f.getPhone());
                            freelancer.setPassword(f.getPassword());
                            freelancer.setReferid(f.getReferid());
                            freelancer.setName(f.getName());
                            String freelancerInfo = new Gson().toJson(freelancer);
                            preferences.setFreelancerInfo(freelancerInfo);
                            startActivity(new Intent(SignUpActivity.this, VerifyNumberActivity.class)
                                    .putExtra("id",freelancer.getId())
                                    .putExtra("code",freelancer.getSecurityPin())
                                    .putExtra("number",freelancer.getPhone()));
                                   // .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        } else {
                            Toast.makeText(this, "" + getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();
                       // Toast.makeText(this, "freelancer null " , Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                binding.etConfirmPassword.setError(getResources().getString(R.string.password_mismatch));
            }
        } else {
            if (TextUtils.isEmpty(binding.etPhone.getText())) {
                binding.etPhone.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etPhone.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etFullName.getText())) {
                binding.etFullName.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etFullName.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etReferr.getText())) {
                binding.etReferr.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etReferr.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etEmail.getText())) {
                binding.etEmail.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etEmail.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etPassword.getText())) {
                binding.etPassword.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etPassword.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etConfirmPassword.getText())) {
                binding.etConfirmPassword.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etConfirmPassword.setFocusable(true);
            }
            Toast.makeText(this, getResources().getString(R.string.need_required_information), Toast.LENGTH_SHORT).show();
        }
    }
}