package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityEmployeeBalanceBinding;
import com.proyojanapps.proyojanapps.databinding.ActivityIncomeReportBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.FreelancerIncomeReport;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.adapter.IncomeReportAdapter;
import com.proyojanapps.proyojanapps.viewModel.FreelancerActivitesViewModel;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;

import java.util.ArrayList;

public class EmployeeBalanceActivity extends AppCompatActivity {
    private ActivityEmployeeBalanceBinding binding;
    private MySharedPreparence preparence;
    private FreelancerViewModel freelancerViewModel;
    private Freelancer freelancerInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_employee_balance);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        init();
        freelancerLoginData();
    }

    private void freelancerLoginData() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerViewModel.logIn(freelancerInfo.getPhone(), freelancerInfo.getPassword()).observe(this, freelancer -> {
            dialog.dismiss();
            if (freelancer != null && freelancer.getMessage() != null) {
                if (freelancer.getMessage().equals(StaticKey.SUCCESS)) {
                    freelancer.setPassword(freelancerInfo.getPassword());
                    String freelancerInfo = new Gson().toJson(freelancer);
                    this.freelancerInfo = freelancer;
                    preparence.setFreelancerInfo(freelancerInfo);
                    setData();
                } else {
                    //Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
                }
            } else {
                // Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void setData() {
        if (freelancerInfo != null) {
            binding.tvMyBalance.setText("" + freelancerInfo.getTotalBalance() + " " + getString(R.string.ta));
            binding.tvMyWorkBalance.setText("" + freelancerInfo.getTotalWorkIncome() + " " + getString(R.string.ta));
            binding.tvReferralBalance.setText("" + freelancerInfo.getTotalReferIncome() + " " + getString(R.string.ta));
            binding.tvTotalBalance.setText("" + freelancerInfo.getTotalBalance() + " " + getString(R.string.ta));
        }
    }

    private void init() {
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
    }
}