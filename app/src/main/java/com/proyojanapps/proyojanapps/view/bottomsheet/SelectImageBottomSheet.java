package com.proyojanapps.proyojanapps.view.bottomsheet;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.BottomSheetSelectImageBinding;


public class SelectImageBottomSheet extends BottomSheetDialogFragment {

    private BottomSheetListener mListener;
    private BottomSheetSelectImageBinding binding;
    private int requestCodeCamera,requestCodeGallery;

    public SelectImageBottomSheet(BottomSheetListener mListener) {
        this.mListener = mListener;
    }

    @SuppressLint("ValidFragment")
    public SelectImageBottomSheet(BottomSheetListener mListener,int requestCodeCamera,int requestCodeGallery) {
        this.mListener = mListener;
        this.requestCodeCamera=requestCodeCamera;
        this.requestCodeGallery=requestCodeGallery;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_select_image, container, false);

        binding.cameraImageL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onCameraButtonClicked(requestCodeCamera);
            }
        });
        binding.galleryImageL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onGalleryButtonClicked(requestCodeGallery);
            }
        });
        return binding.getRoot();
    }


    public interface BottomSheetListener {
        void onCameraButtonClicked(int requestCode);

        void onGalleryButtonClicked(int requestCode);
    }
}
