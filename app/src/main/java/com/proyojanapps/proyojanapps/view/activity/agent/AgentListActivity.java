package com.proyojanapps.proyojanapps.view.activity.agent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentListBinding;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentLoginBinding;
import com.proyojanapps.proyojanapps.model.AgentList;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.view.activity.BaseActivity;
import com.proyojanapps.proyojanapps.view.adapter.AgentListAdapter;
import com.proyojanapps.proyojanapps.view.adapter.MyNetworkAdapter;
import com.proyojanapps.proyojanapps.viewModel.AgentViewModel;

import java.util.ArrayList;

public class AgentListActivity extends BaseActivity {

    private MySharedPreparence preferences;
    private AgentViewModel agentViewModel;
    private ActivityAgentListBinding binding;
    private Freelancer freelancerInfo;
    private ArrayList<AgentList> lists;
    private AgentListAdapter agentListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_agent_list);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });
        init();
        getAgentList();
    }


    private void getAgentList() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        agentViewModel.getAgentList().observe(this, agentList -> {
            dialog.dismiss();
            lists = (ArrayList<AgentList>) agentList;
            initRV();
        });
    }

    private void init() {
        agentViewModel = ViewModelProviders.of(this).get(AgentViewModel.class);
        preferences = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preferences.getFreelancerInfo(), Freelancer.class);
        lists = new ArrayList<>();
    }

    private void initRV() {
        agentListAdapter = new AgentListAdapter(this, lists);
        binding.agentListRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.agentListRV.setAdapter(agentListAdapter);
    }
}