package com.proyojanapps.proyojanapps.view.activity.agent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentSignUpRollsBinding;
import com.proyojanapps.proyojanapps.view.activity.BaseActivity;

public class AgentSignUpRollsActivity extends BaseActivity {

    ActivityAgentSignUpRollsBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil. setContentView(this,R.layout.activity_agent_sign_up_rolls);
        binding.btnContenue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AgentSignUpRollsActivity.this, AgentSignUpActivity.class));
            }
        });
    }
}