package com.proyojanapps.proyojanapps.view.activity.freelancer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityPasswordChangeBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;

public class PasswordChangeActivity extends AppCompatActivity {

    ActivityPasswordChangeBinding binding;
    MySharedPreparence preferences;
    FreelancerViewModel freelancerViewModel;
    private String freelancer_id;
    private Freelancer freelancerInfo = null;
    private String identity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_password_change);
        binding.imageViewBack.setOnClickListener(view -> {
            onBackPressed();
        });
        init();
        getIntentData();
        binding.btnChangePassword.setOnClickListener(view -> {
            if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.CHANGE_PASSWORD)) {
                changePassword();
            } else
                forgetPassword();
        });
        getIntentData();
    }

    private void getIntentData() {
        if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.CHANGE_PASSWORD)) {
            freelancerInfo = new Gson().fromJson(preferences.getFreelancerInfo(), Freelancer.class);
            binding.llOldPassword.setVisibility(View.VISIBLE);
            binding.tvPasswordHeading.setText(R.string.change_password);
            binding.btnMainChangePassword.setText(R.string.change_password);
        }

    }

    private void init() {
        identity = getIntent().getStringExtra("identity");
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
        preferences = new MySharedPreparence(this);
        freelancer_id = getIntent().getStringExtra("id");

    }

    private void forgetPassword() {
        String pass = binding.etPassword.getText().toString().trim();
        String rePass = binding.etConfirmPassword.getText().toString().trim();
        if (!TextUtils.isEmpty(pass) && !TextUtils.isEmpty(rePass)) {
            if (pass.equals(rePass)) {
                final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
                freelancerViewModel.forgetConfirm(freelancer_id, pass)
                        .observe(this, message -> {
                            dialog.dismiss();
                            if (!TextUtils.isEmpty(message) && message.equals("success")) {
                                startActivity(new Intent(PasswordChangeActivity.this, LoginActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            } else {
                                Toast.makeText(this, "" + getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();

                            }
                        });
            } else {
                binding.etConfirmPassword.setError(getResources().getString(R.string.password_mismatch));
            }
        } else {
            if (TextUtils.isEmpty(binding.etPassword.getText())) {
                binding.etPassword.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etPassword.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etConfirmPassword.getText())) {
                binding.etConfirmPassword.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etConfirmPassword.setFocusable(true);
            }
        }

    }


    private void changePassword() {
        String oldPassword = binding.etOldPassword.getText().toString().trim();
        String newPassword = binding.etPassword.getText().toString().trim();
        String rePass = binding.etConfirmPassword.getText().toString().trim();
        if (!TextUtils.isEmpty(newPassword) && !TextUtils.isEmpty(rePass) && !TextUtils.isEmpty(oldPassword)) {
            if (oldPassword.equals(freelancerInfo.getPassword())) {
                if (newPassword.equals(rePass)) {
                    final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
                    freelancerViewModel.changePassword(freelancerInfo.getId(), oldPassword, newPassword)
                            .observe(this, message -> {
                                dialog.dismiss();
                                if (!TextUtils.isEmpty(message) && message.equals(StaticKey.SUCCESS)) {
                                    preferences.setFreelancerInfo(null);
                                    preferences.setFreelancerLoginStatus(false);
                                    preferences.setFreelancerId(0);
                                    preferences.setAgentInfo(null);
                                    startActivity(new Intent(PasswordChangeActivity.this, LoginActivity.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                } else {
                                    Toast.makeText(this, "" + getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();

                                }
                            });
                } else {
                    binding.etConfirmPassword.setError(getResources().getString(R.string.password_mismatch));
                    Toast.makeText(this, "" + getResources().getString(R.string.password_mismatch), Toast.LENGTH_SHORT).show();


                }
            } else {
                binding.etOldPassword.setError(getResources().getString(R.string.invalid_password));
                Toast.makeText(this, "" + getResources().getString(R.string.invalid_password), Toast.LENGTH_SHORT).show();

            }
        } else {
            if (TextUtils.isEmpty(binding.etOldPassword.getText())) {
                binding.etOldPassword.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etOldPassword.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etPassword.getText())) {
                binding.etPassword.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etPassword.setFocusable(true);
            }
            if (TextUtils.isEmpty(binding.etConfirmPassword.getText())) {
                binding.etConfirmPassword.setError("" + getResources().getString(R.string.required_field_cannot_be_empty));
                binding.etConfirmPassword.setFocusable(true);
            }
        }

    }

}