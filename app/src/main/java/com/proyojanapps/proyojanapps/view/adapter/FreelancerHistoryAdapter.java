package com.proyojanapps.proyojanapps.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.CustomFreelancerHistoryLayoutBinding;
import com.proyojanapps.proyojanapps.databinding.CustomeAgentLayoutBinding;
import com.proyojanapps.proyojanapps.model.AgentList;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.StaticKey;

import java.util.List;


public class FreelancerHistoryAdapter extends RecyclerView.Adapter<FreelancerHistoryAdapter.ViewHolder> {

    private Context context;
    private List<Freelancer.History> historyList;

    public FreelancerHistoryAdapter(Context context, List<Freelancer.History> historyList) {
        this.context = context;
        this.historyList = historyList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomFreelancerHistoryLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.custom_freelancer_history_layout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Freelancer.History history = historyList.get(position);

        viewHolder.binding.tvRequestType.setText(history.getRequestType());
        viewHolder.binding.tvStatus.setText(history.getStatus());
        viewHolder.binding.tvAmount.setText(context.getString(R.string.amount) + " : " + String.format("%.2f", history.getAmount() != null ?
                Double.valueOf(history.getAmount()) : "0") + " " + context.getString(R.string.ta));
        viewHolder.binding.tvPaymentMethod.setText(history.getPaymentMethod());

        if (history.getStatus() != null && history.getStatus().toLowerCase().equals(StaticKey.PAID.toLowerCase())) {
            viewHolder.binding.tvRequestType.setTextColor(Color.BLACK);
            viewHolder.binding.tvStatus.setTextColor(Color.parseColor("#2CA01B"));
            viewHolder.binding.tvStatusTitle.setTextColor(Color.parseColor("#2CA01B"));
            viewHolder.binding.llImageBackground.setBackgroundResource(R.drawable.ic_baseline_circle_green_24);
        } else {
            viewHolder.binding.tvRequestType.setTextColor(Color.RED);
            viewHolder.binding.tvStatus.setTextColor(Color.RED);
            viewHolder.binding.tvStatusTitle.setTextColor(Color.RED);
            viewHolder.binding.llImageBackground.setBackgroundResource(R.drawable.ic_baseline_circle_red_24);
        }

        viewHolder.binding.llRow.setOnClickListener(view -> {

        });
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private CustomFreelancerHistoryLayoutBinding binding;

        public ViewHolder(CustomFreelancerHistoryLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private void loadUrl(String url) {
//        Intent intent = new Intent(context, WebViewActivity.class).putExtra("url", url);
//        context.startActivity(intent);
    }
}