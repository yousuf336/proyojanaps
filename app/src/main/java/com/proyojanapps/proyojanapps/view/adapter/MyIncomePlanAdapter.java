package com.proyojanapps.proyojanapps.view.adapter;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.CustomeMyIncomePlanLayoutBinding;
import com.proyojanapps.proyojanapps.databinding.CustomeMyofferLayoutBinding;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;

import java.util.List;


public class MyIncomePlanAdapter extends RecyclerView.Adapter<MyIncomePlanAdapter.ViewHolder> {

    private Context context;
    private List<Freelancer.MyIncomePlan> myIncomePlanList;

    public MyIncomePlanAdapter(Context context, List<Freelancer.MyIncomePlan> myIncomePlanList) {
        this.context = context;
        this.myIncomePlanList = myIncomePlanList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomeMyIncomePlanLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.custome_my_income_plan_layout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Freelancer.MyIncomePlan myIncomePlan = myIncomePlanList.get(position);

        viewHolder.binding.tvPublicationDate.setText("Publication Date : " + myIncomePlan.getPublicationDate());
        viewHolder.binding.tvValidationDate.setText("Validation Date : " + myIncomePlan.getValidationDate());

        if (myIncomePlan.getImage() != null)
            Glide.with(context).applyDefaultRequestOptions(new RequestOptions()
                    .placeholder(R.drawable.ic_image_black_24dp)).load(RetrofitInstance.BASE_URL + "/" + myIncomePlan.getImage()).into(viewHolder.binding.imageView);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
           viewHolder.binding.tvDescription .setText(Html.fromHtml(""+myIncomePlan.getDescription(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            viewHolder.binding.tvDescription .setText(Html.fromHtml(""+myIncomePlan.getDescription()));
        }
        if ((position + 1) % 2 == 0)
            viewHolder.binding.llRow.setBackgroundColor(context.getResources().getColor(R.color.off_white));
        else {
            viewHolder.binding.llRow.setBackgroundColor(context.getResources().getColor(R.color.white));
        }


        viewHolder.binding.llRow.setOnClickListener(view -> {

        });
    }

    @Override
    public int getItemCount() {
        return myIncomePlanList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private CustomeMyIncomePlanLayoutBinding binding;

        public ViewHolder(CustomeMyIncomePlanLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private void loadUrl(String url) {
//        Intent intent = new Intent(context, WebViewActivity.class).putExtra("url", url);
//        context.startActivity(intent);
    }
}