package com.proyojanapps.proyojanapps.view.activity.agent;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.animation.Animator;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentConfirmTransectionBinding;
import com.proyojanapps.proyojanapps.model.Agent;
import com.proyojanapps.proyojanapps.model.Transaction;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.activity.BaseActivity;
import com.proyojanapps.proyojanapps.viewModel.AgentActivitesViewModel;

public class AgentConfirmTransectionActivity extends BaseActivity {
    private ActivityAgentConfirmTransectionBinding binding;
    private AgentActivitesViewModel agentActivitesViewModel;
    private MySharedPreparence preparence;
    private Agent agentInfo;
    private String identity, amount, agentId, referId, password, number, transNumber, note;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_agent_confirm_transection);
        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.llConfirm.setOnClickListener(view -> {
            if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.CASH_OUT_AGENT)) {
                cashout();
            } else if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.CASH_IN_AGENT)) {
                cashin();
            } else if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.SEND_MONEY)) {
                sendMoney();
            }

        });
        init();
        retrieveData();
        setData();
    }

    private void init() {
        agentActivitesViewModel = ViewModelProviders.of(this).get(AgentActivitesViewModel.class);
        preparence = new MySharedPreparence(this);
        agentInfo = new Gson().fromJson(preparence.getAgentInfo(), Agent.class);
    }

    private void retrieveData() {
        //.....................cashin cash out...............................
        number = getIntent().getStringExtra("number");
        transNumber = getIntent().getStringExtra("transNumber");
        note = getIntent().getStringExtra("note");
        //.....................send money cashin cash out...............................
        referId = getIntent().getStringExtra("referId");
        agentId = getIntent().getStringExtra("agentId");
        amount = getIntent().getStringExtra("amount");
        password = getIntent().getStringExtra("password");
        identity = getIntent().getStringExtra("identity");

    }

    private void setData() {
        if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.CASH_OUT_AGENT)) {
            binding.tvReferId.setText("" + number);
            binding.tvAmountTitle.setText("" + getString(R.string.cash_out));
        } else if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.CASH_IN_AGENT)) {
            binding.tvReferId.setText("" + number);
            binding.tvAmountTitle.setText("" + getString(R.string.cash_in));
        } else if (!TextUtils.isEmpty(identity) && identity.equals(StaticKey.SEND_MONEY)) {
            binding.tvReferId.setText("" + referId);
            binding.tvAmountTitle.setText("" + getString(R.string.send_money));
        }

        binding.tvCashoutBalance.setText("" + amount + " " + getString(R.string.ta));
        try {
            double newBalance = agentInfo.getAgentBalance() != null ? Double.valueOf(agentInfo.getAgentBalance()) - Double.valueOf(amount)
                    : 0 - Double.valueOf(amount);
            binding.tvNewBalance.setText(" " + (newBalance > 0 ? newBalance : "0") + " " + getString(R.string.ta));

        } catch (Exception e) {

        }

    }

    private void sendMoney() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        agentActivitesViewModel.sendMoney(agentInfo.getAgentId(), referId, amount, password).observe(this,
                message -> {
                    dialog.dismiss();
                    if (message != null) {
                        if (message.equals(StaticKey.SUCCESS)) {
                            successDialogin();
                        } else if (message.equals(StaticKey.INSUFFICIENT_BALANCE)) {
                            Toast.makeText(this, "" + getString(R.string.insufficient_balance), Toast.LENGTH_SHORT).show();
                        } else if (message.equals(StaticKey.REFFER_ID_NOT_VALID)) {
                            Toast.makeText(this, "" + getString(R.string.refer_id_invalid), Toast.LENGTH_SHORT).show();
                        } else if (message.equals(StaticKey.FREELANCER_ALREADY_ACTIVE)) {
                            Toast.makeText(this, "" + getString(R.string.freelancer_already_active), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void cashout() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        Transaction.AgentCashOut cashOut = new Transaction.AgentCashOut(number, amount, note, password, agentInfo.getAgentId());
        agentActivitesViewModel.submitCashout(cashOut).observe(this,
                message -> {
                    dialog.dismiss();
                    if (message != null) {
                        if (message.equals(StaticKey.SUCCESS)) {
                            successDialogin();
                        } else if (message.equals(StaticKey.INSUFFICIENT_BALANCE)) {
                            Toast.makeText(this, "" + getString(R.string.insufficient_balance), Toast.LENGTH_SHORT).show();
                        } else if (message.equals(StaticKey.REFFER_ID_NOT_VALID)) {
                            Toast.makeText(this, "" + getString(R.string.refer_id_invalid), Toast.LENGTH_SHORT).show();
                        } else if (message.equals(StaticKey.FREELANCER_ALREADY_ACTIVE)) {
                            Toast.makeText(this, "" + getString(R.string.freelancer_already_active), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void cashin() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        Transaction.AgentCashin cashin = new Transaction.AgentCashin(number, transNumber, amount, note, password, agentInfo.getAgentId());
        agentActivitesViewModel.submitCashin(cashin).observe(this,
                message -> {
                    dialog.dismiss();
                    if (message != null) {
                        if (message.equals(StaticKey.SUCCESS)) {
                            successDialogin();
                        } else if (message.equals(StaticKey.INSUFFICIENT_BALANCE)) {
                            Toast.makeText(this, "" + getString(R.string.insufficient_balance), Toast.LENGTH_SHORT).show();
                        } else if (message.equals(StaticKey.REFFER_ID_NOT_VALID)) {
                            Toast.makeText(this, "" + getString(R.string.refer_id_invalid), Toast.LENGTH_SHORT).show();
                        } else if (message.equals(StaticKey.FREELANCER_ALREADY_ACTIVE)) {
                            Toast.makeText(this, "" + getString(R.string.freelancer_already_active), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void successDialogin() {
        showSuccessDialog(this);
        dialogSuccessBinding.animationViewOnline.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                // Toast.makeText(context, "onAnimationStart", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                // Toast.makeText(context, "onAnimationEnd", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAnimationCancel(Animator animator) {
                // Toast.makeText(context, "onAnimationCancel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
                // Toast.makeText(context, "onAnimationRepeat", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(AgentConfirmTransectionActivity.this, AgentProfileActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                dialogSuccess.cancel();
            }
        });

    }

}