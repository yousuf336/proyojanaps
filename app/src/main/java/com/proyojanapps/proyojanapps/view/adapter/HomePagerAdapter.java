package com.proyojanapps.proyojanapps.view.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.view.fragment.HomeFragment;
import com.proyojanapps.proyojanapps.view.fragment.MyWorkPlaceFragment;

public class HomePagerAdapter extends FragmentPagerAdapter {

    private static final String[] TAB_TITLES =
            new String[]{"", "", "",""};
    private static final int[] TAB_ICONS =
            new int[]{R.drawable.ic_baseline_home_24, R.drawable.ic_baseline_my_work_place_24,
                    R.drawable.ic_whatsapp_square_brands,R.drawable.ic_baseline_call_24};
    private final Context mContext;

    public HomePagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return HomeFragment.newInstance("Address", "");
            case 1:
                return MyWorkPlaceFragment.newInstance("My Work Placce", "");
            case 2:
                return HomeFragment.newInstance("Product Specification", "");
            case 3:
                return HomeFragment.newInstance("Product Specification", "");
            default:
                return null;
        }
    }

//    @Nullable
//    @Override
//    public CharSequence getPageTitle(int position) {
//        return TAB_TITLES[position];
//    }

//        @Override
//    public CharSequence getPageTitle(int position) {
//        Drawable image = ContextCompat.getDrawable(mContext, TAB_ICONS[position]);
//        image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
//        SpannableString sb = new SpannableString(" ");
//        ImageSpan imageSpan = new ImageSpan(image);
//        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        return sb;
//    }
    @Override
    public int getCount() {
        // Show 3 total pages.
        return 4;
    }
}

