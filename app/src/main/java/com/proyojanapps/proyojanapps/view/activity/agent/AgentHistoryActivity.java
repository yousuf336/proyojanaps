package com.proyojanapps.proyojanapps.view.activity.agent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentHistoryBinding;
import com.proyojanapps.proyojanapps.databinding.ActivityAgentTransactionListBinding;
import com.proyojanapps.proyojanapps.model.Agent;
import com.proyojanapps.proyojanapps.model.Transaction;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.adapter.CashoutRequestListAdapter;
import com.proyojanapps.proyojanapps.viewModel.AgentActivitesViewModel;

import java.util.ArrayList;

public class AgentHistoryActivity extends AppCompatActivity {
    private ActivityAgentHistoryBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_agent_history);


        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        binding.btnSend.setOnClickListener(view -> {
            startActivity(new Intent(this, AgentTransactionListActivity.class)
                    .putExtra("identity", StaticKey.SEND_MONEY));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.btnCashOut.setOnClickListener(view -> {
            startActivity(new Intent(this, AgentTransactionListActivity.class)
                    .putExtra("identity", StaticKey.CASH_OUT_AGENT));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.btnCashin.setOnClickListener(view -> {
            startActivity(new Intent(this, AgentTransactionListActivity.class)
                    .putExtra("identity", StaticKey.CASH_IN_AGENT));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
    }


}