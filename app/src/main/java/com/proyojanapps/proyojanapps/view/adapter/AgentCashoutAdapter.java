package com.proyojanapps.proyojanapps.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.CustomeAgentCashOutInSendLayoutBinding;
import com.proyojanapps.proyojanapps.databinding.CustomeAgentCashOutRequestLayoutBinding;
import com.proyojanapps.proyojanapps.model.Transaction;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.callback.CashoutRequestStatus;

import java.util.List;


public class AgentCashoutAdapter extends RecyclerView.Adapter<AgentCashoutAdapter.ViewHolder> {

    private Context context;
    private List<Transaction.AgentCashOut> agentCashOutList;

    public AgentCashoutAdapter(Context context, List<Transaction.AgentCashOut> agentCashOutList) {
        this.context = context;
        this.agentCashOutList = agentCashOutList;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomeAgentCashOutInSendLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.custome_agent_cash_out_in_send_layout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Transaction.AgentCashOut cashOut = agentCashOutList.get(position);

        viewHolder.binding.tvName.setText(cashOut.getAgent_name());
        viewHolder.binding.tvAmount.setText(context.getString(R.string.amount) + " : " + String.format("%.2f", cashOut.getAmount() != null ?
                Double.valueOf(cashOut.getAmount()) : "0") + " " + context.getString(R.string.ta));
        viewHolder.binding.tvNumber.setText(context.getString(R.string.receiver_number) + " : " + cashOut.getReceiverNumber());
        viewHolder.binding.tvDate.setText(context.getString(R.string.date) + " : " + cashOut.getRequest_date());
        viewHolder.binding.tvNote.setText(context.getString(R.string.note) + " : " + cashOut.getNote());
        viewHolder.binding.tvStatus.setText("" + cashOut.getStatus());

        if (cashOut.getStatus() != null && cashOut.getStatus().equals(StaticKey.UNPAID)) {
            viewHolder.binding.tvStatus.setTextColor(Color.parseColor("#E81505"));
        } else {
            viewHolder.binding.tvStatus.setTextColor(Color.parseColor("#2CA01B"));
        }

    }

    @Override
    public int getItemCount() {
        return agentCashOutList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private CustomeAgentCashOutInSendLayoutBinding binding;

        public ViewHolder(CustomeAgentCashOutInSendLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private void loadUrl(String url) {
//        Intent intent = new Intent(context, WebViewActivity.class).putExtra("url", url);
//        context.startActivity(intent);
    }
}