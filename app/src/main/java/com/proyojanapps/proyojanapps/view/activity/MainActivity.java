package com.proyojanapps.proyojanapps.view.activity;


import android.Manifest;
import android.animation.Animator;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;

import androidx.appcompat.widget.Toolbar;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.proyojanapps.proyojanapps.R;
import com.proyojanapps.proyojanapps.databinding.ActivityMainBinding;
import com.proyojanapps.proyojanapps.databinding.DialogWarningBinding;
import com.proyojanapps.proyojanapps.databinding.NavHeaderMainBinding;
import com.proyojanapps.proyojanapps.model.AboutUs;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.util.CustomLoadingDialog;
import com.proyojanapps.proyojanapps.util.CustomVisibility;
import com.proyojanapps.proyojanapps.util.FilePath;
import com.proyojanapps.proyojanapps.util.MySharedPreparence;
import com.proyojanapps.proyojanapps.util.StaticKey;
import com.proyojanapps.proyojanapps.view.activity.agent.AgentListActivity;
import com.proyojanapps.proyojanapps.view.activity.agent.AgentLoginActivity;
import com.proyojanapps.proyojanapps.view.activity.agent.AgentProfileActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.CashoutActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.EmployeeBalanceActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.FreelancerHistoryActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.IncomePlansActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.IncomeReportActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.LoginActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.MyNetworkActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.MyOfferActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.MyProfileActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.RechargeActivity;
import com.proyojanapps.proyojanapps.view.activity.freelancer.TransferAmountActivity;
import com.proyojanapps.proyojanapps.view.bottomsheet.SelectImageBottomSheet;
import com.proyojanapps.proyojanapps.view.fragment.HomeFragment;
import com.proyojanapps.proyojanapps.view.fragment.MyCircleFragment;
import com.proyojanapps.proyojanapps.view.fragment.MyWorkPlaceFragment;
import com.proyojanapps.proyojanapps.view.fragment.PitZoneFragment;
import com.proyojanapps.proyojanapps.viewModel.FreelancerActivitesViewModel;
import com.proyojanapps.proyojanapps.viewModel.FreelancerViewModel;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, SelectImageBottomSheet.BottomSheetListener {


    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int REQUEST_SELECT_PHOTO = 2;
    Menu menu;
    public ActivityMainBinding binding;
    private NavHeaderMainBinding headerBinding;

    private Dialog dialogWarning;
    private DialogWarningBinding warningBinding;
    private boolean isMyInfoExpanded;
    private boolean isBalanceShow = false;
    private SelectImageBottomSheet selectImageBottomSheet;
    private File imageFile;
    private String currentPhotoPath;
    private Freelancer freelancerInfo;
    private MySharedPreparence preparence;
    private FreelancerViewModel freelancerViewModel;
    private FreelancerActivitesViewModel freelancerActivitesViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        changeStatusBarIconColor();
        headerBinding = NavHeaderMainBinding.bind(binding.navView.getHeaderView(0));

        init();

        if (preparence.getFreelancerInfo()==null){
            startActivity(new Intent(this, LoginActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
        freelancerLoginData();
        freelancerLoginData();
        setNavigtationItem();
        setTabLayout();
        setData();

    }

    private void init() {
        preparence = new MySharedPreparence(this);
        freelancerInfo = new Gson().fromJson(preparence.getFreelancerInfo(), Freelancer.class);
        freelancerViewModel = ViewModelProviders.of(this).get(FreelancerViewModel.class);
        freelancerActivitesViewModel = ViewModelProviders.of(this).get(FreelancerActivitesViewModel.class);
    }

    private void setData() {
        if (freelancerInfo != null) {
            headerBinding.tvReferalId.setText("ID : " + freelancerInfo.getOwnReferId());
            headerBinding.tvName.setText("Name : " + freelancerInfo.getName());

            if (freelancerInfo.getProfilepic() != null && !freelancerInfo.getProfilepic().equals("")) {
                Glide.with(this).applyDefaultRequestOptions(new RequestOptions()
                        .placeholder(R.drawable.ic_image_circle)).load(freelancerInfo.getProfilepic())
                        .into(headerBinding.profileImageCIV);
            }
        }
    }

    private void showBalance() {
        Animation animation = AnimationUtils.loadAnimation(
                this, isBalanceShow == false ? R.anim.slide_out_right : R.anim.slide_out_left
        );
        headerBinding.tvTaka.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Animation animation1 = AnimationUtils.loadAnimation(
                        MainActivity.this, isBalanceShow == false ? R.anim.slide_out_left_custome_duration : R.anim.slide_out_right_custome_duration
                );
                headerBinding.tvBalance.startAnimation(animation1);

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(100, LinearLayout.LayoutParams.MATCH_PARENT);

                params.gravity = isBalanceShow == false ? Gravity.END : Gravity.START;
                headerBinding.tvTaka.setLayoutParams(params);


                if (!isBalanceShow) {
                    FrameLayout.LayoutParams params1 = new FrameLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    params1.gravity = Gravity.START;
                    params1.rightMargin = 40;
                    params1.leftMargin = 3;

                    headerBinding.tvBalance.setLayoutParams(params1);
                    if (freelancerInfo != null)
                        headerBinding.tvBalance.setText("" + freelancerInfo.getTotalBalance());
                } else {
                    FrameLayout.LayoutParams params1 = new FrameLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    params1.gravity = Gravity.END;
                    params1.rightMargin = 4;
                    params1.leftMargin = 40;

                    headerBinding.tvBalance.setLayoutParams(params1);
                    headerBinding.tvBalance.setText("Check Balance");
                }
                isBalanceShow = !isBalanceShow;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void setNavigtationItem() {
        TextView tvToolbarTitle = (TextView) findViewById(R.id.tvToolbarTitle);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // getSupportActionBar().setTitle(AppController.getInstance().getQuizeBookTitle());
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        final Menu menu = navigationView.getMenu();
        //menu.getItem(R.id.bangladesh).setIcon(R.drawable.camera);


        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View view, float v) {

            }

            @Override
            public void onDrawerOpened(View view) {
                setStatusBarColorGreen();
            }

            @Override
            public void onDrawerClosed(View view) {
                // your refresh code can be called from here
               changeStatusBarIconColor();
            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });


        // View headerview = navigationView.getHeaderView(0);
        headerBinding.llMyInfo.setOnClickListener(view -> {
            if (isMyInfoExpanded) {
                isMyInfoExpanded = false;
                headerBinding.imageviewMyInfo.setImageResource(R.drawable.ic_baseline_keyboard_arrow_right_24);
                CustomVisibility.collapse(headerBinding.llMyInfoChild, 1000);
            } else {
                CustomVisibility.expand(headerBinding.llMyInfoChild, 1000);
                headerBinding.imageviewMyInfo.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24);
                isMyInfoExpanded = true;
            }
        });
        headerBinding.llBalance.setOnClickListener(view -> {
            // showBalance();
            startActivity(new Intent(this, EmployeeBalanceActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        });
        headerBinding.profileImageCIV.setOnClickListener(view -> {
            if (freelancerInfo != null) {
                openSelectImageBottomSheet();
                //showZoomImageDialog(freelancerInfo.getProfilepic());
            }
        });
        this.menu = navigationView.getMenu();


        if (!TextUtils.isEmpty(freelancerInfo.getAccountpay()) && freelancerInfo.getAccountpay().equals("0")) {
            showWarningDialog();
        } else if (!TextUtils.isEmpty(freelancerInfo.getAccountpay()) && !freelancerInfo.getAccountpay().equals("0")
                && !TextUtils.isEmpty(freelancerInfo.getStatus()) && freelancerInfo.getAccountpay().equals("0")) {
            showWarningDialog();
        }
        headerBinding.rlIncomePlans.setOnClickListener(view -> {
            startActivity(new Intent(this, IncomePlansActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        headerBinding.rlMyProfile.setOnClickListener(view -> {
            startActivity(new Intent(this, MyProfileActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        headerBinding.rlMyNetwork.setOnClickListener(view -> {
            startActivity(new Intent(this, MyNetworkActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        headerBinding.rlInComeReport.setOnClickListener(view -> {
            startActivity(new Intent(this, IncomeReportActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        headerBinding.rlHistory.setOnClickListener(view -> {
            startActivity(new Intent(this, FreelancerHistoryActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        headerBinding.rlMyWorkPlace.setOnClickListener(view -> {
            DrawerLayout d = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (d.isDrawerOpen(GravityCompat.START)) {
                d.closeDrawer(GravityCompat.START);
            }
            binding.appBarMain.tabs.getTabAt(1).select();
        });
        headerBinding.rlMyCircle.setOnClickListener(view -> {
            DrawerLayout d = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (d.isDrawerOpen(GravityCompat.START)) {
                d.closeDrawer(GravityCompat.START);
            }
            binding.appBarMain.tabs.getTabAt(2).select();
        });
        headerBinding.btnCashOut.setOnClickListener(view -> {
            startActivity(new Intent(this, CashoutActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        headerBinding.btnTransfer.setOnClickListener(view -> {
            startActivity(new Intent(this, TransferAmountActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        headerBinding.tvRecharge.setOnClickListener(view -> {
            startActivity(new Intent(this, RechargeActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        headerBinding.rlAgent.setOnClickListener(view -> {
            if (preparence.getAgentInfo() != null) {
                startActivity(new Intent(this, AgentProfileActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                startActivity(new Intent(this, AgentLoginActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        headerBinding.rlAgentList.setOnClickListener(view -> {
            startActivity(new Intent(this, AgentListActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        headerBinding.rlMyOffer.setOnClickListener(view -> {
            startActivity(new Intent(this, MyOfferActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        headerBinding.rlInvestment.setOnClickListener(view -> {
            startActivity(new Intent(this, MessageActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        headerBinding.rlPitLive.setOnClickListener(view -> {
            startActivity(new Intent(this, MessageActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        headerBinding.rlPremiumClub.setOnClickListener(view -> {
            startActivity(new Intent(this, MessageActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        headerBinding.rlPrivacyPolicy.setOnClickListener(view -> {
            startActivity(new Intent(this, TermsAndConditionActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        headerBinding.rlAboutUs.setOnClickListener(view -> {
            startActivity(new Intent(this, AboutusActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        headerBinding.llHelp.setOnClickListener(view -> {
            startActivity(new Intent(this, HelpActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        headerBinding.llSettings.setOnClickListener(view -> {
            startActivity(new Intent(this, SettingsActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        headerBinding.tvLogout.setOnClickListener(view -> {
            preparence.setAgentInfo(null);
            preparence.setFreelancerInfo(null);
            preparence.setFreelancerId(0);
            preparence.setFreelancerLoginStatus(false);
            startActivity(new Intent(this, LoginActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_toolbar_icon,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.notification:
                startActivity(new Intent(this, MessageActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
        }

        return true;
    }

    private void setTabLayout() {
        //  HomePagerAdapter confirmPagerAdapter = new HomePagerAdapter(this, getSupportFragmentManager());
        //   binding.appBarMain.contentMain.viewPager.setAdapter(confirmPagerAdapter);
        // TabLayout tabs = findViewById(R.id.tabs);
        binding.appBarMain.tabs.addTab(binding.appBarMain.tabs.newTab().setIcon(R.drawable.ic_baseline_home_24));
        binding.appBarMain.tabs.addTab(binding.appBarMain.tabs.newTab().setIcon(R.drawable.ic_start_work));
        binding.appBarMain.tabs.addTab(binding.appBarMain.tabs.newTab().setIcon(R.drawable.ic_whatsapp_square_brands));
        binding.appBarMain.tabs.addTab(binding.appBarMain.tabs.newTab().setIcon(R.drawable.ic_baseline_dashboard_24));
        //  binding.appBarMain.contentMain.tabs.setupWithViewPager(binding.appBarMain.contentMain.viewPager);
        //
        replaceFragment(new HomeFragment());


        binding.appBarMain.tabs.addOnTabSelectedListener(
                new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        switch (tab.getPosition()) {
                            case 0:
                                replaceFragment(new HomeFragment());
                                // binding.tvConfirmOrder.setText("Continue");
                                binding.appBarMain.tvToolbarTitle.setText("" + getString(R.string.home));
                                break;
                            case 1:
                                replaceFragment(new MyWorkPlaceFragment());
                                binding.appBarMain.tvToolbarTitle.setText("" + getString(R.string.my_work_place));
                                //binding.tvConfirmOrder.setText("Continue");
                                break;
                            case 2:
                                replaceFragment(new MyCircleFragment());
                                binding.appBarMain.tvToolbarTitle.setText("" + getString(R.string.my_circle));
                                //binding.tvConfirmOrder.setText("Confirm Order");
                                break;
                            case 3:
                                replaceFragment(new PitZoneFragment());
                                binding.appBarMain.tvToolbarTitle.setText("" + getString(R.string.pit_zone));
                                //binding.tvConfirmOrder.setText("Confirm Order");
                                break;
                        }
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                }
        );
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.llFragment, fragment);
        ft.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStart() {
        //setNavigtationItem();
        super.onStart();
    }


    private void freelancerLoginData() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        freelancerViewModel.logIn(freelancerInfo.getPhone(), freelancerInfo.getPassword()).observe(this, freelancer -> {
            dialog.dismiss();
            if (freelancer != null && freelancer.getMessage() != null) {
                if (freelancer.getMessage().equals(StaticKey.SUCCESS)) {
                    freelancer.setPassword(freelancerInfo.getPassword());
                    String freelancerInfo = new Gson().toJson(freelancer);
                    this.freelancerInfo = freelancer;
                    preparence.setFreelancerInfo(freelancerInfo);
                    setData();
                } else {
                    //Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
                }
            } else {
                // Toast.makeText(this, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout d = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (d.isDrawerOpen(GravityCompat.START)) {
            d.closeDrawer(GravityCompat.START);
        } else {
            confirmationDialog();
        }

    }

    private void confirmationDialog() {
        showConfirmationDialog(this);
        dialogConfirmationBinding.yesTV.setOnClickListener(view -> {
            finish();
        });
        dialogConfirmationBinding.noTV.setOnClickListener(view -> {
            dialogConfirmation.dismiss();
        });
    }

    protected void showWarningDialog() {
        dialogWarning = new Dialog(this);
        dialogWarning.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_warning, null, false);
        dialogWarning.setContentView(warningBinding.getRoot());

        Window window = dialogWarning.getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        if (freelancerInfo.getAccountpay() == null && freelancerInfo.getStatus().equals("0")) {
            warningBinding.tvMessage.setText(getString(R.string.your_account_is_inactive_contact_with_agent));
            warningBinding.btnText.setText(getString(R.string.cancel));
            warningBinding.btnWarning.setOnClickListener(view -> {

                dialogWarning.dismiss();
            });
        } else if (!TextUtils.isEmpty(freelancerInfo.getAccountpay()) && freelancerInfo.getStatus().equals("0")) {
            warningBinding.tvMessage.setText(getString(R.string.your_account_is_inactive));
            warningBinding.btnText.setText(getString(R.string.active));
            warningBinding.btnWarning.setOnClickListener(view -> {

                final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
                freelancerViewModel.accountActivation(freelancerInfo.getId())
                        .observe(this, message -> {
                            dialog.dismiss();
                            if (message != null && message.equals("success")) {
                                showSuccessDialog(this);

                                dialogSuccessBinding.animationViewOnline.addAnimatorListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animator) {
                                        // Toast.makeText(context, "onAnimationStart", Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animator) {
                                        // Toast.makeText(context, "onAnimationEnd", Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animator) {
                                        // Toast.makeText(context, "onAnimationCancel", Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animator) {
                                        // Toast.makeText(context, "onAnimationRepeat", Toast.LENGTH_SHORT).show();
                                        preparence.setFreelancerInfo(null);
                                        preparence.setFreelancerId(0);
                                        preparence.setFreelancerLoginStatus(false);
                                        startActivity(new Intent(MainActivity.this, LoginActivity.class)
                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                        dialogSuccess.cancel();
                                    }
                                });


                            } else {
                                Toast.makeText(this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                            }
                        });
                dialogWarning.dismiss();
            });
        }
        dialogWarning.show();
    }


    private void openSelectImageBottomSheet() {
        selectImageBottomSheet = new SelectImageBottomSheet(this);
        selectImageBottomSheet.show(getSupportFragmentManager(), "selectImage");
    }

    @Override
    public void onCameraButtonClicked(int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1030);
            }
        } else {
            dispatchTakePictureIntent();
        }
        selectImageBottomSheet.dismiss();
    }

    @Override
    public void onGalleryButtonClicked(int requestCode) {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(Intent.createChooser(galleryIntent, "Select Photo"), REQUEST_SELECT_PHOTO);
        selectImageBottomSheet.dismiss();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            CropImage.activity(Uri.fromFile(new File(currentPhotoPath)))
                    .setAspectRatio(1, 1)
                    .start(this);
        } else if (requestCode == REQUEST_SELECT_PHOTO && resultCode == RESULT_OK && data != null) {
            Uri imageUrl = data.getData();
            CropImage.activity(imageUrl)
                    .setAspectRatio(1, 1)
                    .start(this);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Uri resultUri = null;
            if (result != null) {
                resultUri = result.getUri();
            }
            getImageFile(FilePath.getPath(this, resultUri));

            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (bitmap != null) {
                setImageToView(bitmap);
            }


            changeProfilePic();
        }
    }


    private void getImageFile(String photoPath) {
        imageFile = new File(photoPath);
    }

    private void setImageToView(Bitmap bitmap) {
        headerBinding.profileImageCIV.setImageBitmap(bitmap);
    }

    private void changeProfilePic() {
        final Dialog dialog = CustomLoadingDialog.createLoadingDialog(this);
        if (imageFile != null) {
            freelancerViewModel.changeProfilePic(freelancerInfo.getId(), imageFile).observe(this, new Observer<String>() {
                @Override
                public void onChanged(String s) {
                    dialog.dismiss();
                    if (s != null && s.equals(StaticKey.SUCCESS)) {
                        freelancerLoginData();
                    } else {
                        Toast.makeText(MainActivity.this, "" + getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                }
            });


        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ignored) {
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.proyojanapps.proyojanapps.customerfileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}
