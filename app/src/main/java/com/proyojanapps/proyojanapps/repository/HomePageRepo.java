package com.proyojanapps.proyojanapps.repository;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proyojanapps.proyojanapps.model.Slider;
import com.proyojanapps.proyojanapps.model.Testimonial;
import com.proyojanapps.proyojanapps.retrofit.ApiService;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePageRepo {
    private Application application;
    private ApiService apiService;

    public HomePageRepo(Application application) {
        this.application = application;
        apiService = RetrofitInstance.getRetrofitInstance().create(ApiService.class);
    }

    public MutableLiveData<List<Testimonial>> getTestimonial() {
        final MutableLiveData<List<Testimonial>> liveData = new MutableLiveData<>();

        Call call = apiService.getSliderList();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));

                            JSONArray dataJA = jsonObjectParent.getJSONArray("tesitmonial");
                            Type type = new TypeToken<List<Slider>>() {
                            }.getType();
                            List<Testimonial> testimonialList = new Gson().fromJson(dataJA.toString(), type);
                            liveData.postValue(testimonialList);
                    } catch (JSONException e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                liveData.postValue(null);
            }
        });

        return liveData;
    }
}
