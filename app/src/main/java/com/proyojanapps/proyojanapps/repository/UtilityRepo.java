package com.proyojanapps.proyojanapps.repository;

import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.proyojanapps.proyojanapps.model.AboutUs;
import com.proyojanapps.proyojanapps.model.Contact;
import com.proyojanapps.proyojanapps.model.District;
import com.proyojanapps.proyojanapps.model.Division;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.TermsAndCondition;
import com.proyojanapps.proyojanapps.model.Testimonial;
import com.proyojanapps.proyojanapps.model.Union;
import com.proyojanapps.proyojanapps.model.Upazilla;
import com.proyojanapps.proyojanapps.retrofit.ApiService;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UtilityRepo {
    private Application application;
    private ApiService apiService;

    public UtilityRepo(Application application) {
        this.application = application;
        apiService = RetrofitInstance.getRetrofitInstance().create(ApiService.class);

    }

    public MutableLiveData<List<Division>> getDivisionList() {
        final MutableLiveData<List<Division>> liveData = new MutableLiveData<>();
        Call call = apiService.getDivisionList();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));

                        JSONArray dataJA = jsonObjectParent.getJSONArray("divisions");
                        Type type = new TypeToken<List<Division>>() {
                        }.getType();
                        List<Division> divisionList = new Gson().fromJson(dataJA.toString(), type);
                        liveData.postValue(divisionList);
                    } catch (JSONException e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<List<District>> getDistrictList(int divisionId) {
        final MutableLiveData<List<District>> liveData = new MutableLiveData<>();
        Call call = apiService.getDistrictList(divisionId);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));

                        JSONArray dataJA = jsonObjectParent.getJSONArray("districts");
                        Type type = new TypeToken<List<District>>() {
                        }.getType();
                        List<District> districtList = new Gson().fromJson(dataJA.toString(), type);
                        liveData.postValue(districtList);
                    } catch (JSONException e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<List<Upazilla>> getUpazilaList(int districtId) {
        final MutableLiveData<List<Upazilla>> liveData = new MutableLiveData<>();
        Call call = apiService.getUpazilaList(districtId);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));

                        JSONArray dataJA = jsonObjectParent.getJSONArray("upazilas");
                        Type type = new TypeToken<List<Upazilla>>() {
                        }.getType();
                        List<Upazilla> upazilaList = new Gson().fromJson(dataJA.toString(), type);
                        liveData.postValue(upazilaList);
                    } catch (JSONException e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<List<Union>> getUnionList(int upazilaId) {
        final MutableLiveData<List<Union>> liveData = new MutableLiveData<>();
        Call call = apiService.getUnionList(upazilaId);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));

                        JSONArray dataJA = jsonObjectParent.getJSONArray("unions");
                        Type type = new TypeToken<List<Union>>() {
                        }.getType();
                        List<Union> unionList = new Gson().fromJson(dataJA.toString(), type);
                        liveData.postValue(unionList);
                    } catch (JSONException e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }


    public MutableLiveData<TermsAndCondition> getTermsAndConditions() {
        final MutableLiveData<TermsAndCondition> liveData = new MutableLiveData<>();
        Call call = apiService.getTermsAndCondition();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        String title = jsonObjectParent.getString("title");
                        String description = jsonObjectParent.getString("description");
                        TermsAndCondition termsAndCondition = new TermsAndCondition(title, description);
                        liveData.postValue(termsAndCondition);
                    } catch (JSONException e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<String> getNotice() {
        final MutableLiveData<String> liveData = new MutableLiveData<>();
        Call call = apiService.getNotice();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));

                        JSONObject obj = jsonObjectParent.getJSONObject("notice");
                        String notice = obj.getString("notice");

                        liveData.postValue(notice);
                    } catch (JSONException e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<String> submitMessage(Map<String, Object> map) {
        final MutableLiveData<String> liveData = new MutableLiveData<>();
        Call call = apiService.submitVisitorContact(map);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        String message = jsonObjectParent.getString("message");
                        liveData.postValue(message);
                    } catch (JSONException e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<List<Testimonial>> getTestimonial() {
        final MutableLiveData<List<Testimonial>> liveData = new MutableLiveData<>();
        Call call = apiService.getTestimonial();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));

                        JSONArray dataJA = jsonObjectParent.getJSONArray("tesitmonial");
                        Type type = new TypeToken<List<Testimonial>>() {
                        }.getType();
                        List<Testimonial> testimonials = new Gson().fromJson(dataJA.toString(), type);
                        liveData.postValue(testimonials);
                    } catch (JSONException e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }



    public MutableLiveData<AboutUs> getAboutUs() {
        final MutableLiveData<AboutUs> liveData = new MutableLiveData<>();
        Call call = apiService.aboutUs();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));

                        JSONObject responseBodyJO = jsonObjectParent.getJSONObject("about_us");
                        AboutUs aboutUs = new Gson().fromJson(responseBodyJO.toString(), AboutUs.class);
                        liveData.postValue(aboutUs);
                    } catch (JSONException e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<List<Contact>> readContact(Context context) {
        final MutableLiveData<List<Contact>> liveData = new MutableLiveData<>();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                ContentResolver cr = context.getContentResolver();
//                Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
//                        null, null, null,String.valueOf(ContentResolver.QUERY_SORT_DIRECTION_DESCENDING));
//
                Cursor cur = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null,
                        ContactsContract.Contacts.HAS_PHONE_NUMBER + " = 1",
                        null,
                        "UPPER(" + ContactsContract.Contacts.DISPLAY_NAME + ") ASC");

                List<Contact> contactList = new ArrayList<>();
                if ((cur != null ? cur.getCount() : 0) > 0) {
                    while (cur != null && cur.moveToNext()) {
                        String id = cur.getString(
                                cur.getColumnIndex(ContactsContract.Contacts._ID));
                        String name = cur.getString(cur.getColumnIndex(
                                ContactsContract.Contacts.DISPLAY_NAME));

                        if (cur.getInt(cur.getColumnIndex(
                                ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                            Cursor pCur = cr.query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                    new String[]{id}, null);
                            String phoneNo=null;
                            while (pCur.moveToNext()) {
                                 phoneNo = pCur.getString(pCur.getColumnIndex(
                                        ContactsContract.CommonDataKinds.Phone.NUMBER));

                                Log.i("contact", "Name: " + name);
                                Log.i("contact", "Phone Number: " + phoneNo);
                            }
                            contactList.add(new Contact(name, phoneNo));
                            pCur.close();

                        }
                    }
                    liveData.postValue(contactList);
                } else {
                    liveData.postValue(null);
                }
                if (cur != null) {
                    cur.close();
                }

            }
        };
        Thread thread = new Thread(r);
        thread.start();
        return liveData;
    }



//    private MutableLiveData<List<Contact>> getContactList(Context context) {
//        final MutableLiveData<List<Contact>> liveData = new MutableLiveData<>();
//        ContentResolver cr = context.getContentResolver();
//        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
//                null, null, null, null);
//
//        if ((cur != null ? cur.getCount() : 0) > 0) {
//            while (cur != null && cur.moveToNext()) {
//                String id = cur.getString(
//                        cur.getColumnIndex(ContactsContract.Contacts._ID));
//                String name = cur.getString(cur.getColumnIndex(
//                        ContactsContract.Contacts.DISPLAY_NAME));
//
//                if (cur.getInt(cur.getColumnIndex(
//                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
//                    Cursor pCur = cr.query(
//                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                            null,
//                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
//                            new String[]{id}, null);
//                    List<Contact> contactList = new ArrayList<>();
//                    while (pCur.moveToNext()) {
//                        String phoneNo = pCur.getString(pCur.getColumnIndex(
//                                ContactsContract.CommonDataKinds.Phone.NUMBER));
//                        contactList.add(new Contact(name, phoneNo));
//                        Log.i("contact", "Name: " + name);
//                        Log.i("contact", "Phone Number: " + phoneNo);
//                    }
//                    pCur.close();
//                    liveData.postValue(contactList);
//                } else {
//                    liveData.postValue(null);
//                }
//            }
//        } else {
//            liveData.postValue(null);
//        }
//        if (cur != null) {
//            cur.close();
//        }
//        return liveData;
//    }
}


