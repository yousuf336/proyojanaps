package com.proyojanapps.proyojanapps.repository;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.Slider;
import com.proyojanapps.proyojanapps.model.Testimonial;
import com.proyojanapps.proyojanapps.retrofit.ApiService;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FreelancerRepo {

    private Application application;
    private ApiService apiService;

    public FreelancerRepo(Application application) {
        this.application = application;
        apiService = RetrofitInstance.getRetrofitInstance().create(ApiService.class);

    }

    public MutableLiveData<Freelancer> registration(Freelancer registration) {
        final MutableLiveData<Freelancer> liveData = new MutableLiveData<>();
        if (registration != null) {
            Map<String, Object> registrationMap = new HashMap<>();
            registrationMap.put("name", registration.getName());
            registrationMap.put("email", registration.getEmail());
            registrationMap.put("phone", registration.getPhone());
            registrationMap.put("refferalid", registration.getReferid());
            registrationMap.put("password", registration.getPassword());
            registrationMap.put("termscondition", registration.getTermscondition());
            Call call = apiService.freelancerRegistration(registrationMap);

            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {
                        try {

                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            JSONObject responseBodyJO = jsonObjectParent.getJSONObject("freelancer_info");
                            Freelancer freelancer = new Gson().fromJson(responseBodyJO.toString(), Freelancer.class);
                            liveData.postValue(freelancer);
                        } catch (Exception e) {
                            liveData.postValue(null);
                            Log.e("message", "" + e.getMessage());
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        }


        return liveData;
    }

    public MutableLiveData<Freelancer> login(String username, String password) {
        final MutableLiveData<Freelancer> liveData = new MutableLiveData<>();
        if (username != null && password != null) {
            Map<String, Object> loginMap = new HashMap<>();
            loginMap.put("username", username);
            loginMap.put("password", password);
            Call call = apiService.freelancerLogin(loginMap);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {

                        try {
                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            String message = jsonObjectParent.getString("message");

                            if (message != null && message.equals("success")) {
                                JSONObject responseBodyJO = jsonObjectParent.getJSONObject("freelancer_info");
                                Freelancer freelancer = new Gson().fromJson(responseBodyJO.toString(), Freelancer.class);
                                freelancer.setMessage(message);
                                liveData.postValue(freelancer);
                            } else {
                                Freelancer f = new Freelancer();
                                f.setMessage(message);
                                liveData.postValue(f);
                            }
                        } catch (Exception e) {
                            liveData.postValue(null);
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        }
        return liveData;
    }

    public MutableLiveData<String> securityPin(int freelancer_id, String securitypin) {
        final MutableLiveData<String> liveData = new MutableLiveData<>();
        if (securitypin != null) {
            Map<String, Object> securityPinMap = new HashMap<>();
            securityPinMap.put("freelancer_id", freelancer_id);
            securityPinMap.put("securitypin", securitypin);
            Call call = apiService.freelancerSecurityPin(securityPinMap);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {
                        try {
                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            String message = jsonObjectParent.getString("message");
                            liveData.postValue(message);
                        } catch (Exception e) {
                            liveData.postValue(null);
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        }
        return liveData;
    }

    public MutableLiveData<String> changePassword(int freelancer_id, String oldPassword, String password) {
        final MutableLiveData<String> liveData = new MutableLiveData<>();
        if (oldPassword != null && password != null) {
            Map<String, Object> changePasswordMap = new HashMap<>();
            changePasswordMap.put("freelancer_id", freelancer_id);
            changePasswordMap.put("oldpassword", oldPassword);
            changePasswordMap.put("password", password);
            Call call = apiService.freelancerChangePassword(changePasswordMap);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {

                        try {
                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            String message = jsonObjectParent.getString("message");

                            liveData.postValue(message);
                        } catch (Exception e) {
                            liveData.postValue(null);
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        }
        return liveData;
    }

    public MutableLiveData<Freelancer> forgetPhoneverify(String phone) {
        final MutableLiveData<Freelancer> liveData = new MutableLiveData<>();
        if (phone != null) {
            Call call = apiService.freelancerForgetphoneverify(phone);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {
                        try {
                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            JSONObject responseBodyJO = jsonObjectParent.getJSONObject("freelancer_info");
                            Freelancer freelancer = new Gson().fromJson(responseBodyJO.toString(), Freelancer.class);
                            liveData.postValue(freelancer);
                        } catch (Exception e) {
                            liveData.postValue(null);
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        }
        return liveData;
    }

    public MutableLiveData<Freelancer> forgetCodeverify(String freelancer_id, String forgetverifycode) {
        final MutableLiveData<Freelancer> liveData = new MutableLiveData<>();
        if (freelancer_id != null && forgetverifycode != null) {
            Map<String, Object> forgetverifycodeMap = new HashMap<>();
            forgetverifycodeMap.put("freelancer_id", freelancer_id);
            forgetverifycodeMap.put("forgetverifycode", forgetverifycode);
            Call call = apiService.freelancerForgetcodeverify(forgetverifycodeMap);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {

                        try {
                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            String message = jsonObjectParent.getString("message");

                            if (message != null && message.equals("success")) {
                                JSONObject responseBodyJO = jsonObjectParent.getJSONObject("freelancer_info");
                                Freelancer freelancer = new Gson().fromJson(responseBodyJO.toString(), Freelancer.class);
                                freelancer.setMessage(message);
                                liveData.postValue(freelancer);
                            } else {
                                Freelancer f = new Freelancer();
                                f.setMessage(message);
                                liveData.postValue(f);
                            }
                        } catch (Exception e) {
                            liveData.postValue(null);
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        }
        return liveData;
    }

    public MutableLiveData<String> forgetConfirm(String freelancer_id, String fnewpassword) {
        final MutableLiveData<String> liveData = new MutableLiveData<>();
        if (freelancer_id != null && fnewpassword != null) {
            Map<String, Object> forgetConfirmMap = new HashMap<>();
            forgetConfirmMap.put("freelancer_id", freelancer_id);
            forgetConfirmMap.put("fnewpassword", fnewpassword);
            Call call = apiService.freelancerForgetconfirm(forgetConfirmMap);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {

                        try {
                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            String message = jsonObjectParent.getString("message");

                            liveData.postValue(message);
                        } catch (Exception e) {
                            liveData.postValue(null);
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        }
        return liveData;
    }

    public MutableLiveData<Freelancer> changeNumber(int freelancer_id, String number) {
        final MutableLiveData<Freelancer> liveData = new MutableLiveData<>();
        if (number != null) {
            Map<String, Object> changePasswordMap = new HashMap<>();
            changePasswordMap.put("freelancer_id", freelancer_id);
            changePasswordMap.put("phone", number);
            Call call = apiService.freelancerChangeNumber(changePasswordMap);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {
                        try {
                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            JSONObject responseBodyJO = jsonObjectParent.getJSONObject("freelancer_info");
                            Freelancer freelancer = new Gson().fromJson(responseBodyJO.toString(), Freelancer.class);
                            liveData.postValue(freelancer);
                        } catch (Exception e) {
                            liveData.postValue(null);
                            Log.e("message", "" + e.getMessage());
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        }
        return liveData;
    }

    public MutableLiveData<Freelancer> resendCode(int freelancer_id) {
        final MutableLiveData<Freelancer> liveData = new MutableLiveData<>();
        Call call = apiService.freelancerResendCode(freelancer_id);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        JSONObject responseBodyJO = jsonObjectParent.getJSONObject("freelancer_info");
                        Freelancer freelancer = new Gson().fromJson(responseBodyJO.toString(), Freelancer.class);
                        liveData.postValue(freelancer);
                    } catch (Exception e) {
                        liveData.postValue(null);
                        Log.e("message", "" + e.getMessage());
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<String> activeAccount(int freelancer_id) {
        final MutableLiveData<String> liveData = new MutableLiveData<>();
        Call call = apiService.freelancerActive(freelancer_id);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        String message = jsonObjectParent.getString("message");
                        liveData.postValue(message);
                    } catch (Exception e) {
                        liveData.postValue(null);
                        Log.e("message", "" + e.getMessage());
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<String> changeProfilePic(int freelancer_id, File profilePic) {
        final MutableLiveData<String> liveData = new MutableLiveData<>();
        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(freelancer_id));

        MultipartBody.Part profilePicLocal = null;
        if (profilePic != null) {
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), profilePic);
            profilePicLocal = MultipartBody.Part.createFormData("profilepic", profilePic.getName(), requestFile);
        }
        Call call = apiService.freelancerChageprofilepic(id, profilePicLocal);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        String message = jsonObjectParent.getString("message");
                        liveData.postValue(message);
                        Log.e("message", "jsonObjectParent : " + jsonObjectParent);
                    } catch (Exception e) {
                        liveData.postValue(null);
                        Log.e("message", "" + e.getMessage());
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }


    public MutableLiveData<Freelancer> profileUpdate(Freelancer freelancer) {
        final MutableLiveData<Freelancer> liveData = new MutableLiveData<>();
        if (freelancer != null) {
            Map<String, Object> map = new HashMap<>();

            map.put("freelancer_id", freelancer.getId());
            if (freelancer.getName() != null)
                map.put("name", freelancer.getName());
            if (freelancer.getDateOfBirth() != null)
                map.put("dateofbirth", freelancer.getDateOfBirth());
            if (freelancer.getGender() != null)
                map.put("gender", freelancer.getGender());
            if (freelancer.getOccupation() != null)
                map.put("occupation", freelancer.getOccupation());
            if (freelancer.getBloodGroup() != null)
                map.put("bloodgroup", freelancer.getBloodGroup());
            if (freelancer.getAddress() != null)
                map.put("address", freelancer.getAddress());
            if (freelancer.getDivision() != null)
                map.put("division", freelancer.getDivision());
            if (freelancer.getDistrict() != null)
                map.put("district", freelancer.getDistrict());
            if (freelancer.getThana() != null)
                map.put("thana", freelancer.getThana());
            if (freelancer.getUnion() != null)
                map.put("union", freelancer.getUnion());

            Call call = apiService.updateProfile(map);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {
                        try {
                            try {
                                JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                                String message = jsonObjectParent.getString("message");

                                if (message != null && message.equals("success")) {
                                    JSONObject responseBodyJO = jsonObjectParent.getJSONObject("freelancer_info");
                                    Freelancer freelancer = new Gson().fromJson(responseBodyJO.toString(), Freelancer.class);
                                    freelancer.setMessage(message);
                                    liveData.postValue(freelancer);
                                } else {
                                    Freelancer f = new Freelancer();
                                    f.setMessage(message);
                                    liveData.postValue(f);
                                }
                            } catch (Exception e) {
                                liveData.postValue(null);
                            }
                        } catch (Exception e) {
                            liveData.postValue(null);
                            Log.e("message", "" + e.getMessage());
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        }
        return liveData;
    }


    public MutableLiveData<Freelancer> addWork(Freelancer.Work addWork) {
        final MutableLiveData<Freelancer> liveData = new MutableLiveData<>();
        MultipartBody.Part profilePicLocal = null;
        Map<String, Object> map = new HashMap<>();
        map.put("freelancer_id", addWork.getFreelancerId());
        map.put("company_name", addWork.getCompanyName());
        map.put("job_title", addWork.getJobTitle());
        map.put("job_location", addWork.getJobLocation());
        map.put("job_description", addWork.getJobDescription());
        map.put("job_from", addWork.getJobFrom());
        map.put("job_to", addWork.getJobTo());
        map.put("current_job", addWork.getCurrentJob());
        Call call = apiService.addWork(map);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        String message = jsonObjectParent.getString("message");

                        if (message != null && message.equals("success")) {
                            JSONObject responseBodyJO = jsonObjectParent.getJSONObject("freelancer_info");
                            Freelancer freelancer = new Gson().fromJson(responseBodyJO.toString(), Freelancer.class);
                            freelancer.setMessage(message);
                            liveData.postValue(freelancer);
                        } else {
                            Freelancer f = new Freelancer();
                            f.setMessage(message);
                            liveData.postValue(f);
                        }
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<Freelancer> addEducation(Freelancer.Education education) {
        final MutableLiveData<Freelancer> liveData = new MutableLiveData<>();
        MultipartBody.Part profilePicLocal = null;
        Map<String, Object> map = new HashMap<>();
        map.put("freelancer_id", education.getFreelancerId());
        map.put("instutation_name", education.getInstutation_name());
        map.put("education_level", education.getEducation_level());
        map.put("instutation_location", education.getInstutation_location());
        map.put("education_from", education.getEducation_from());
        map.put("education_to", education.getEducation_to());
        map.put("current_study", education.getCurrent_study());
        Call call = apiService.addEducation(map);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        String message = jsonObjectParent.getString("message");

                        if (message != null && message.equals("success")) {
                            JSONObject responseBodyJO = jsonObjectParent.getJSONObject("freelancer_info");
                            Freelancer freelancer = new Gson().fromJson(responseBodyJO.toString(), Freelancer.class);
                            freelancer.setMessage(message);
                            liveData.postValue(freelancer);
                        } else {
                            Freelancer f = new Freelancer();
                            f.setMessage(message);
                            liveData.postValue(f);
                        }
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<Freelancer> addSkill(Freelancer.Skill skill) {
        final MutableLiveData<Freelancer> liveData = new MutableLiveData<>();
        MultipartBody.Part profilePicLocal = null;
        Map<String, Object> map = new HashMap<>();
        map.put("freelancer_id", skill.getFreelancerId());
        map.put("skill", skill.getSkill());
        Call call = apiService.addSkill(map);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        String message = jsonObjectParent.getString("message");

                        if (message != null && message.equals("success")) {
                            JSONObject responseBodyJO = jsonObjectParent.getJSONObject("freelancer_info");
                            Freelancer freelancer = new Gson().fromJson(responseBodyJO.toString(), Freelancer.class);
                            freelancer.setMessage(message);
                            liveData.postValue(freelancer);
                        } else {
                            Freelancer f = new Freelancer();
                            f.setMessage(message);
                            liveData.postValue(f);
                        }
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<List<Freelancer.History>> history(int freelancer_id) {
        final MutableLiveData<List<Freelancer.History>> liveData = new MutableLiveData<>();
        Call call = apiService.freelancerHistory(freelancer_id);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));

                        JSONArray dataJA = jsonObjectParent.getJSONArray("history");
                        Type type = new TypeToken<List<Freelancer.History>>() {
                        }.getType();
                        List<Freelancer.History> historyList = new Gson().fromJson(dataJA.toString(), type);
                        liveData.postValue(historyList);

                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<List<Freelancer.MyOffers>> myOffer() {
        final MutableLiveData<List<Freelancer.MyOffers>> liveData = new MutableLiveData<>();
        Call call = apiService.myOffer();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));

                        JSONArray dataJA = jsonObjectParent.getJSONArray("my_offers");
                        Type type = new TypeToken<List<Freelancer.MyOffers>>() {
                        }.getType();
                        List<Freelancer.MyOffers> historyList = new Gson().fromJson(dataJA.toString(), type);
                        liveData.postValue(historyList);

                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<List<Freelancer.MyIncomePlan>> myIncomePlan() {
        final MutableLiveData<List<Freelancer.MyIncomePlan>> liveData = new MutableLiveData<>();
        Call call = apiService.myIncomePlan();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        JSONArray dataJA = jsonObjectParent.getJSONArray("my_income_planing");
                        Type type = new TypeToken<List<Freelancer.MyIncomePlan>>() {
                        }.getType();
                        List<Freelancer.MyIncomePlan> list = new Gson().fromJson(dataJA.toString(), type);
                        liveData.postValue(list);
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }
            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });
        return liveData;
    }


}
