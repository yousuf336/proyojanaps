package com.proyojanapps.proyojanapps.repository;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.Transaction;
import com.proyojanapps.proyojanapps.retrofit.ApiService;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgentActivitesRepo {

    private Application application;
    private ApiService apiService;

    public AgentActivitesRepo(Application application) {
        this.application = application;
        apiService = RetrofitInstance.getRetrofitInstance().create(ApiService.class);

    }


    public MutableLiveData<String> sendMoney(int agent_id, String ownreferid, String amount, String password) {
        final MutableLiveData<String> liveData = new MutableLiveData<>();
        Map<String, Object> sendMoneyMap = new HashMap<>();
        sendMoneyMap.put("agent_id", agent_id);
        sendMoneyMap.put("ownreferid", ownreferid);
        sendMoneyMap.put("amount", amount);
        sendMoneyMap.put("password", password);
        Call call = apiService.agentSendMoney(sendMoneyMap);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {

                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        String message = jsonObjectParent.getString("message");
                        liveData.postValue(message);
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });
        return liveData;
    }

    public MutableLiveData<List<Transaction.AgentCashOutRequest>> cashOutRequestList(String agent_reefer_id) {
        final MutableLiveData<List<Transaction.AgentCashOutRequest>> liveData = new MutableLiveData<>();

        Call call = apiService.agentCashoutRequestList(agent_reefer_id);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {

                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        JSONArray dataJA = jsonObjectParent.getJSONArray("agent_cash_out_request_list");
                        Type type = new TypeToken<List<Transaction.AgentCashOutRequest>>() {
                        }.getType();
                        List<Transaction.AgentCashOutRequest> transactionList = new Gson().fromJson(dataJA.toString(), type);
                        liveData.postValue(transactionList);
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });
        return liveData;
    }

    public MutableLiveData<List<Transaction.AgentSendMoney>> sendMoneyList(int agent_id) {
        final MutableLiveData<List<Transaction.AgentSendMoney>> liveData = new MutableLiveData<>();

        Call call = apiService.agentSendMoneyList(agent_id);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {

                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        JSONArray dataJA = jsonObjectParent.getJSONArray("agent_send_money_list");
                        Type type = new TypeToken<List<Transaction.AgentSendMoney>>() {
                        }.getType();
                        List<Transaction.AgentSendMoney> transactionList = new Gson().fromJson(dataJA.toString(), type);
                        liveData.postValue(transactionList);
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });
        return liveData;
    }

    public MutableLiveData<List<Transaction.AgentCashin>> cashInList(int agent_id) {
        final MutableLiveData<List<Transaction.AgentCashin>> liveData = new MutableLiveData<>();

        Call call = apiService.agentCashInList(agent_id);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {

                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        JSONArray dataJA = jsonObjectParent.getJSONArray("agent_cash_in_list");
                        Type type = new TypeToken<List<Transaction.AgentCashin>>() {
                        }.getType();
                        List<Transaction.AgentCashin> transactionList = new Gson().fromJson(dataJA.toString(), type);
                        liveData.postValue(transactionList);
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });
        return liveData;
    }

    public MutableLiveData<List<Transaction.AgentCashOut>> cashOutList(int agent_id) {
        final MutableLiveData<List<Transaction.AgentCashOut>> liveData = new MutableLiveData<>();

        Call call = apiService.agentCashOutList(agent_id);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {

                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        JSONArray dataJA = jsonObjectParent.getJSONArray("agent_cash_out_list");
                        Type type = new TypeToken<List<Transaction.AgentCashOut>>() {
                        }.getType();
                        List<Transaction.AgentCashOut> transactionList = new Gson().fromJson(dataJA.toString(), type);
                        liveData.postValue(transactionList);
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });
        return liveData;
    }

    public MutableLiveData<String> cashoutSubmit(Transaction.AgentCashOut cashOut) {
        final MutableLiveData<String> liveData = new MutableLiveData<>();
        Map<String, Object> cashoutMap = new HashMap<>();
        cashoutMap.put("agent_id", cashOut.getAgentId());
        cashoutMap.put("amount", cashOut.getAmount());
        cashoutMap.put("password", cashOut.getPassword());
        cashoutMap.put("receiver_number", cashOut.getReceiverNumber());
        cashoutMap.put("note", cashOut.getNote());
        Call call = apiService.agentSubmitCashOut(cashoutMap);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {

                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        String message = jsonObjectParent.getString("message");
                        liveData.postValue(message);
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });
        return liveData;
    }

    public MutableLiveData<String> cashinSubmit(Transaction.AgentCashin cashin) {
        final MutableLiveData<String> liveData = new MutableLiveData<>();

        Map<String, Object> map = new HashMap<>();
        map.put("agent_id", cashin.getAgentId());
        map.put("amount", cashin.getAmount());
        map.put("password", cashin.getPassword());
        map.put("sender_number", cashin.getSenderNumber());
        map.put("trans_number", cashin.getTransNumber());
        map.put("note", cashin.getNote());
        Call call = apiService.agentSubmitCashin(map);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {

                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        String message = jsonObjectParent.getString("message");
                        liveData.postValue(message);
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });
        return liveData;
    }

    public MutableLiveData<String> cashoutPaid(int agent_id, int hiddenId) {
        final MutableLiveData<String> liveData = new MutableLiveData<>();
        Call call = apiService.agentCashOutPaid(agent_id, hiddenId);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {

                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        String message = jsonObjectParent.getString("message");
                        liveData.postValue(message);
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });
        return liveData;
    }

    public MutableLiveData<String> cashoutUnPaid(int agent_id, int hiddenId) {
        final MutableLiveData<String> liveData = new MutableLiveData<>();
        Call call = apiService.agentCashOutUnpaid(agent_id, hiddenId);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {

                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        String message = jsonObjectParent.getString("message");
                        liveData.postValue(message);
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });
        return liveData;
    }

}
