package com.proyojanapps.proyojanapps.repository;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proyojanapps.proyojanapps.model.Agent;
import com.proyojanapps.proyojanapps.model.AgentList;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.FreelancerIncomeReport;
import com.proyojanapps.proyojanapps.retrofit.ApiService;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgentRepo {

    private Application application;
    private ApiService apiService;

    public AgentRepo(Application application) {
        this.application = application;
        apiService = RetrofitInstance.getRetrofitInstance().create(ApiService.class);

    }

    public MutableLiveData<String> agentRegistration(Agent agent) {
        final MutableLiveData<String> liveData = new MutableLiveData<>();
        if (agent != null) {
            RequestBody freelancer_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(agent.getFreelancerId()));
            RequestBody dateofbirth = RequestBody.create(MediaType.parse("text/plain"), agent.getDateOfBirth());
            RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), agent.getGender());
            RequestBody occupation = RequestBody.create(MediaType.parse("text/plain"), agent.getOccupation());
            RequestBody address = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(agent.getAddress()));
            RequestBody division = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(agent.getDivision()));
            RequestBody district = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(agent.getDistrict()));
            RequestBody thana = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(agent.getThana()));
            RequestBody union = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(agent.getUnion()));
            RequestBody sendernumber = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(agent.getSendernumber()));
            RequestBody transectionid = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(agent.getTransectionid()));
            RequestBody amount = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(agent.getAmount()));

            MultipartBody.Part nidfront = null;
            if (agent.getNidfront() != null) {
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), agent.getNidfront());
                nidfront = MultipartBody.Part.createFormData("image", agent.getNidfront().getName(), requestFile);
            }
            MultipartBody.Part nidback = null;
            if (agent.getNidback() != null) {
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), agent.getNidback());
                nidback = MultipartBody.Part.createFormData("image", agent.getNidback().getName(), requestFile);
            }

            MultipartBody.Part trans_slip = null;
            if (agent.getTrans_slip() != null) {
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), agent.getTrans_slip());
                trans_slip = MultipartBody.Part.createFormData("image", agent.getTrans_slip().getName(), requestFile);
            }


            Call call = apiService.agentRegistration(freelancer_id,dateofbirth,gender,occupation,address,division,district,
                    thana,union,sendernumber,transectionid,amount,nidfront,nidback,trans_slip);

            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {
                        try {

                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            String message=jsonObjectParent.getString("message");
//                            JSONObject responseBodyJO = jsonObjectParent.getJSONObject("freelancer_info");
//                            Freelancer freelancer = new Gson().fromJson(responseBodyJO.toString(), Freelancer.class);
                            Log.e("message", "ooooooooooooo  " + jsonObjectParent);
                            liveData.postValue(message);
                        } catch (Exception e) {
                            liveData.postValue(null);
                            Log.e("message", "" + e.getMessage());
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        }


        return liveData;
    }
    public MutableLiveData<Agent> login(String username, String password) {
        final MutableLiveData<Agent> liveData = new MutableLiveData<>();
        if (username != null && password != null) {
            Map<String, Object> loginMap = new HashMap<>();
            loginMap.put("username", username);
            loginMap.put("password", password);
            Call call = apiService.agentLogin(loginMap);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {
                        try {
                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            String message = jsonObjectParent.getString("message");

                            if (message != null && message.equals("success")) {
                                JSONObject responseBodyJO = jsonObjectParent.getJSONObject("agent_info");
                                Agent agent = new Gson().fromJson(responseBodyJO.toString(), Agent.class);
                                liveData.postValue(agent);
                            } else {
                                liveData.postValue(null);
                            }
                        } catch (Exception e) {
                            liveData.postValue(null);
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        }
        return liveData;
    }

    public MutableLiveData<List<AgentList>> agentList() {
        final MutableLiveData<List<AgentList>> liveData = new MutableLiveData<>();
            Call call = apiService.agentList();
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {
                        try {
                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            JSONArray dataJA = jsonObjectParent.getJSONArray("agent_lists");
                            Type type = new TypeToken<List<AgentList>>() {
                            }.getType();
                            List<AgentList> agentLists = new Gson().fromJson(dataJA.toString(), type);
                            liveData.postValue(agentLists);
                        } catch (Exception e) {
                            liveData.postValue(null);
                            Log.e("message", "" + e.getMessage());
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        return liveData;
    }
}
