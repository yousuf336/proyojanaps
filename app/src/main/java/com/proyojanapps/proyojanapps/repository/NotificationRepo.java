package com.proyojanapps.proyojanapps.repository;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proyojanapps.proyojanapps.model.Notification;
import com.proyojanapps.proyojanapps.model.ResponseBody;
import com.proyojanapps.proyojanapps.retrofit.ApiService;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class NotificationRepo {
    private Application application;
    private ApiService apiService;
    private SharedPreferences sharedPreferences;

    public NotificationRepo(Application application) {
        this.application = application;
        apiService = RetrofitInstance.getRetrofitInstance().create(ApiService.class);
      //  sharedPreferences = application.getSharedPreferences(SharedPref.SHARED_PREF_NAME, MODE_PRIVATE);
    }


    public MutableLiveData<List<Notification>> getNotificationList() {
        final MutableLiveData<List<Notification>> liveData = new MutableLiveData<>();


        String token = sharedPreferences.getString("SharedPref.TOKEN", "");
        if (!token.equals("")) {
            Call call = apiService.getNotificationList("Bearer " + token);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    if (response.body() != null) {
                        try {
                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            JSONObject responseBodyJO = jsonObjectParent.getJSONObject("responseBody");
                            ResponseBody responseBody = new Gson().fromJson(responseBodyJO.toString(), ResponseBody.class);

                            if (responseBody.isStatus() == true) {
                                JSONArray dataJA = jsonObjectParent.getJSONArray("dataInfo");
                                Type type = new TypeToken<List<Notification>>() {
                                }.getType();
                                List<Notification> notificationList = new Gson().fromJson(dataJA.toString(), type);
                                liveData.postValue(notificationList);
                            } else {
                                liveData.postValue(null);
                            }
                        } catch (JSONException e) {
                            liveData.postValue(null);
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    liveData.postValue(null);
                }
            });
        }

        return liveData;
    }





    public MutableLiveData<ResponseBody> changeNotificationStatus(int notificationId) {

        final MutableLiveData<ResponseBody> liveData = new MutableLiveData<>();

        String token = sharedPreferences.getString("SharedPref.TOKEN", "");
        if (!token.equals("")) {
            Call call = apiService.changeNotificationStatus("Bearer " + token, notificationId);

            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    if (response.body() != null) {
                        try {
                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            JSONObject responseBodyJO = jsonObjectParent.getJSONObject("responseBody");
                            ResponseBody responseBody = new Gson().fromJson(responseBodyJO.toString(), ResponseBody.class);

                            liveData.postValue(responseBody);

                        } catch (JSONException e) {
                            liveData.postValue(null);
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    liveData.postValue(null);
                }
            });
        }


        return liveData;
    }

}
