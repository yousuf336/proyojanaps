package com.proyojanapps.proyojanapps.repository;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proyojanapps.proyojanapps.model.Freelancer;
import com.proyojanapps.proyojanapps.model.FreelancerIncomeReport;
import com.proyojanapps.proyojanapps.model.FreelancerNetwork;
import com.proyojanapps.proyojanapps.model.IncomePlans;
import com.proyojanapps.proyojanapps.model.Workplace;
import com.proyojanapps.proyojanapps.retrofit.ApiService;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FreelancerActivitesRepo {

    private Application application;
    private ApiService apiService;

    public FreelancerActivitesRepo(Application application) {
        this.application = application;
        apiService = RetrofitInstance.getRetrofitInstance().create(ApiService.class);

    }

    public MutableLiveData<List<IncomePlans>> incomePlan() {
        final MutableLiveData<List<IncomePlans>> liveData = new MutableLiveData<>();
        Call call = apiService.freelancerIncomeplans();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        JSONArray dataJA = jsonObjectParent.getJSONArray("income_plan");
                        Type type = new TypeToken<List<IncomePlans>>() {
                        }.getType();
                        List<IncomePlans> incomeList = new Gson().fromJson(dataJA.toString(), type);
                        liveData.postValue(incomeList);
                    } catch (Exception e) {
                        liveData.postValue(null);
                        Log.e("message", "" + e.getMessage());
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<List<FreelancerNetwork>> myNetwork(int freelancer_id) {
        final MutableLiveData<List<FreelancerNetwork>> liveData = new MutableLiveData<>();
        if (freelancer_id > 0) {
            Call call = apiService.freelancerMyNetwork(freelancer_id);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {

                        try {
                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            JSONArray dataJA = jsonObjectParent.getJSONArray("my_network");
                            Type type = new TypeToken<List<FreelancerNetwork>>() {
                            }.getType();
                            List<FreelancerNetwork> myNetworkList = new Gson().fromJson(dataJA.toString(), type);
                            liveData.postValue(myNetworkList);
                        } catch (Exception e) {
                            liveData.postValue(null);
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        }
        return liveData;
    }

    public MutableLiveData<FreelancerIncomeReport> incomeReport(int freelancer_id) {
        final MutableLiveData<FreelancerIncomeReport> liveData = new MutableLiveData<>();
        if (freelancer_id > 0) {
            Call call = apiService.freelancerIncomereport(freelancer_id);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {

                        try {

                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            JSONObject my_income = jsonObjectParent.getJSONObject("my_income");
                            FreelancerIncomeReport freelancerIncomeReport = new Gson().fromJson(my_income.toString(), FreelancerIncomeReport.class);
                            JSONArray dataJA = jsonObjectParent.getJSONArray("my_member_income");
                            Type type = new TypeToken<List<FreelancerIncomeReport.MemberIncome>>() {
                            }.getType();
                            List<FreelancerIncomeReport.MemberIncome> myIncomeList = new Gson().fromJson(dataJA.toString(), type);
                            freelancerIncomeReport.setMemberIncomesList((ArrayList<FreelancerIncomeReport.MemberIncome>) myIncomeList);
                            liveData.postValue(freelancerIncomeReport);
                        } catch (Exception e) {
                            liveData.postValue(null);
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        }
        return liveData;
    }

    public MutableLiveData<String> cashOut(int freelancer_id, String ownreferid, int payment_method, String amount) {
        final MutableLiveData<String> liveData = new MutableLiveData<>();
        Map<String, Object> cashOutMap = new HashMap<>();
        cashOutMap.put("freelancer_id", freelancer_id);
        cashOutMap.put("agent_id", ownreferid);
        cashOutMap.put("payment_method", payment_method);
        cashOutMap.put("amount", amount);
        Call call = apiService.cashOut(cashOutMap);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {

                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        String message = jsonObjectParent.getString("message");
                        liveData.postValue(message);
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<String> transferAmount(int freelancer_id, String ownreferid, int payment_method, String amount) {
        final MutableLiveData<String> liveData = new MutableLiveData<>();
        Map<String, Object> transferAmountMap = new HashMap<>();
        transferAmountMap.put("freelancer_id", freelancer_id);
        transferAmountMap.put("ownreferid", ownreferid);
        transferAmountMap.put("payment_method", payment_method);
        transferAmountMap.put("amount", amount);
        Call call = apiService.freelancerTransferAmount(transferAmountMap);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.d("yousuf", "onSuccess: " + response.body());
                if (response.body() != null) {

                    try {

                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                        String message = jsonObjectParent.getString("message");
                        liveData.postValue(message);
                    } catch (Exception e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("Yousuf", "onFailure: " + t.getMessage());
                liveData.postValue(null);
            }
        });

        return liveData;
    }


    public MutableLiveData<Workplace> workplace(int freelancer_id) {
        final MutableLiveData<Workplace> liveData = new MutableLiveData<>();
        if (freelancer_id > 0) {
            Call call = apiService.workplace(freelancer_id);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    Log.d("yousuf", "onSuccess: " + response.body());
                    if (response.body() != null) {

                        try {

                            JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                            JSONObject my_income = jsonObjectParent.getJSONObject("workplace");
                            Workplace workplace = new Gson().fromJson(my_income.toString(), Workplace.class);
                            liveData.postValue(workplace);
                        } catch (Exception e) {
                            liveData.postValue(null);
                        }
                    } else {
                        liveData.postValue(null);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("Yousuf", "onFailure: " + t.getMessage());
                    liveData.postValue(null);
                }
            });
        }
        return liveData;
    }

    public MutableLiveData<String> workingRenew(int freelancer_id) {
    final MutableLiveData<String> liveData = new MutableLiveData<>();
    Call call = apiService.workingRenew(freelancer_id);
    call.enqueue(new Callback() {
        @Override
        public void onResponse(Call call, Response response) {
            Log.d("yousuf", "onSuccess: " + response.body());
            if (response.body() != null) {
                try {
                    JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                    String message = jsonObjectParent.getString("message");
                    liveData.postValue(message);
                } catch (Exception e) {
                    liveData.postValue(null);
                }
            } else {
                liveData.postValue(null);
            }
        }

        @Override
        public void onFailure(Call call, Throwable t) {
            Log.d("Yousuf", "onFailure: " + t.getMessage());
            liveData.postValue(null);
        }
    });

    return liveData;
}


    public MutableLiveData<String> withdrawBalance(int freelancer_id) {
    final MutableLiveData<String> liveData = new MutableLiveData<>();
    Call call = apiService.withdrawBalance(freelancer_id);
    call.enqueue(new Callback() {
        @Override
        public void onResponse(Call call, Response response) {
            Log.d("yousuf", "onSuccess: " + response.body());
            if (response.body() != null) {
                try {
                    JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));
                    String message = jsonObjectParent.getString("message");
                    liveData.postValue(message);
                } catch (Exception e) {
                    liveData.postValue(null);
                }
            } else {
                liveData.postValue(null);
            }
        }

        @Override
        public void onFailure(Call call, Throwable t) {
            Log.d("Yousuf", "onFailure: " + t.getMessage());
            liveData.postValue(null);
        }
    });

    return liveData;
}
}
