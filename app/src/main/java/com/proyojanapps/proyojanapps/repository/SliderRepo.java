package com.proyojanapps.proyojanapps.repository;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proyojanapps.proyojanapps.model.Slider;
import com.proyojanapps.proyojanapps.model.ResponseBody;
import com.proyojanapps.proyojanapps.retrofit.ApiService;
import com.proyojanapps.proyojanapps.retrofit.RetrofitInstance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SliderRepo {
    private Application application;
    private ApiService apiService;

    public SliderRepo(Application application) {
        this.application = application;
        apiService = RetrofitInstance.getRetrofitInstance().create(ApiService.class);
    }

    public MutableLiveData<List<Slider>> getSliderList() {
        final MutableLiveData<List<Slider>> liveData = new MutableLiveData<>();

        Call call = apiService.getSliderList();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectParent = new JSONObject(new Gson().toJson(response.body()));

                            JSONArray dataJA = jsonObjectParent.getJSONArray("slider");
                            Type type = new TypeToken<List<Slider>>() {
                            }.getType();
                            List<Slider> sliderList = new Gson().fromJson(dataJA.toString(), type);
                            liveData.postValue(sliderList);
                    } catch (JSONException e) {
                        liveData.postValue(null);
                    }
                } else {
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                liveData.postValue(null);
            }
        });

        return liveData;
    }
}
