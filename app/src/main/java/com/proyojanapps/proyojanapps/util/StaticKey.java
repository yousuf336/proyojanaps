package com.proyojanapps.proyojanapps.util;

public class StaticKey {
    public static final String ENGLISH = "en";
    public static final String BANGLA = "bn";

    public static final int TERMS_AND_CONDITION = 1;
    public static final String EMAIL_EXIST = "yes";
    public static final String PHONE_EXIST = "yes";
    public static final String REFFER_ID_NOT_VALID = "not_valid";

    public static final String LEVEL_ONE = "first_level";
    public static final String LEVEL_TWO = "second_level";
    public static final String LEVEL_THREE = "third_level";
    public static final String LEVEL_FOUR = "fourth_level";
    public static final String LEVEL_FIVE = "five_level";


    public static final String CASH_OUT = "cashout";
    public static final String TRANSFER = "transfer";
    public static final String SUCCESS = "success";
    public static final String FREELANCER_ALREADY_ACTIVE = "FreelancerAlreadyActive";
    public static final String REFER_ID_INVALID = "reeferIdInvalid";
    public static final String INSUFFICIENT_BALANCE = "insufficient_balance";

    public static final String SEND_MONEY = "sendmoney";
    public static final String ACTIVE = "Active";
    public static final String INACTIVE = "Inactive";
    public static final String CASH_OUT_AGENT = "cashoutagent";
    public static final String CASH_IN_AGENT = "cashinagent";
    public static final String UNPAID = "Unpaid";
    public static final String PAID = "Paid";


    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String CHANGE_PASSWORD = "changepassword";
    public static final String RENEW_BALANCE= "renewbalance";
    public static final String WITHDRAW_BALANCE= "withdrawbalance";

}
