package com.proyojanapps.proyojanapps.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by issl on 2/15/2018.
 */

public class MySharedPreparence {
    private Context context;
    SharedPreferences preferences;
    private static final String SHARED_PREFERENCE_NAME = "mysharedprefarence";
    private static final String FREELANCER_LOGIN_STATUS = "freelancerlogin";
    private static final String FREELANCER_ID = "freelancerid";
    private static final String FREELANCER_INFO = "freelancerinfor";
    private static final String AGENT_INFO = "agentinfo";


    public MySharedPreparence(Context context) {
        preferences = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        this.context = context;


    }

    public void setFreelancerLoginStatus(boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(FREELANCER_LOGIN_STATUS, value);

        editor.commit();
    }

    public boolean getFreelancerLoginStaus() {
        return preferences.getBoolean(FREELANCER_LOGIN_STATUS, false);
    }

    public void setFreelancerId(int value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(FREELANCER_ID, value);

        editor.commit();
    }

    public int getFreelancerId() {
        return preferences.getInt(FREELANCER_ID, 0);
    }

    public void setFreelancerInfo(String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(FREELANCER_INFO, value);
        editor.commit();
    }

    public String getFreelancerInfo() {
        return preferences.getString(FREELANCER_INFO, null);
    }

    public void setAgentInfo(String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(AGENT_INFO, value);
        editor.commit();
    }

    public String getAgentInfo() {
        return preferences.getString(AGENT_INFO, null);
    }

}
