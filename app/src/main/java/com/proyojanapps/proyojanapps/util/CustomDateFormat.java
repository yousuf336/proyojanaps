package com.proyojanapps.proyojanapps.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CustomDateFormat {

    private static DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static long getLongValue(String date) {
        try {
            Date convertedDate = formatter.parse(date);
            long dateInLong = convertedDate.getTime();
            return dateInLong;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public static String getDate(long milliSeconds, String dateFormat)
    {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
}
