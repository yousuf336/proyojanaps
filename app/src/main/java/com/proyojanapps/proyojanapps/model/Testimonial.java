package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

public class Testimonial {

    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;

    @SerializedName("image")
    private String image;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }
}
