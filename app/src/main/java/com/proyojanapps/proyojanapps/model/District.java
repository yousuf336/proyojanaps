package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

public class District {
    @SerializedName("district_id")
    private int districtId;
    @SerializedName("name")
    private String name;

    public int getDistrictId() {
        return districtId;
    }

    public String getName() {
        return name;
    }
}
