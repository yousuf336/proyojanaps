package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

public class AgentList {
    @SerializedName("name")
    private String name;
    @SerializedName("phone")
    private String phone;
    @SerializedName("email")
    private String email;
    @SerializedName("reefer_id")
    private String reeferId;
    @SerializedName("division")
    private String division;
    @SerializedName("district")
    private String district;
    @SerializedName("thana")
    private String thana;

    @SerializedName("union")
    private String union;
    @SerializedName("transation")
    private String transation;
    @SerializedName("profilepic")
    private String profilepic;

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getReeferId() {
        return reeferId;
    }

    public String getDivision() {
        return division;
    }

    public String getDistrict() {
        return district;
    }

    public String getThana() {
        return thana;
    }

    public String getUnion() {
        return union;
    }

    public String getTransation() {
        return transation;
    }

    public String getProfilepic() {
        return profilepic;
    }
}
