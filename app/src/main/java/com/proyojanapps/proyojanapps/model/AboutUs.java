package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

public class AboutUs {
    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("image")
    private String image;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }
}