package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

public class Freelancer {
    //.........................for signup.......................
    @SerializedName("freelancer_id")
    private int id;
    @SerializedName("security_check")
    private String securityCheck;
    @SerializedName("security_pin")
    private String securityPin;
    @SerializedName("email_exist")
    private String emailExist;//email_exist
    @SerializedName("phone_exist")
    private String phoneExist;//phone_exist
    @SerializedName("message")
    private String message;
    @SerializedName("reefer_id")
    private String refereeIdValidation;
    //..........................signup end.......................


    @SerializedName("name")
    private String name;
    @SerializedName("ownreferid")
    private String ownReferId;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("referid")
    private String referid;
    @SerializedName("dateofbirth")
    private String dateOfBirth;
    @SerializedName("gender")
    private String gender;
    @SerializedName("occupation")
    private String occupation;
    @SerializedName("bloodgroup")
    private String bloodGroup;
    @SerializedName("address")
    private String address;

    @SerializedName("division")
    private String division;
    @SerializedName("district")
    private String district;
    @SerializedName("thana")
    private String thana;
    @SerializedName("union")
    private String union;
    @SerializedName("accountpay")
    private String accountpay;
    @SerializedName("member_from")
    private String memberFrom;
    @SerializedName("job")
    private String job;
    @SerializedName("company_name")
    private String companyName;
    @SerializedName("job_title")
    private String jobTitle;
    @SerializedName("job_location")
    private String jobLocation;
    @SerializedName("job_description")
    private String jobDescription;
    @SerializedName("job_from")
    private String jobFrom;
    @SerializedName("job_to")
    private String jobTo;
    @SerializedName("current_job")
    private String currentJob;
    @SerializedName("education")
    private String education;
    @SerializedName("instutation_name")
    private String instutationName;
    @SerializedName("education_level")
    private String educationLevel;
    @SerializedName("instutation_location")
    private String instutationLocation;
    @SerializedName("education_from")
    private String educationFrom;
    @SerializedName("education_to")
    private String educationTo;
    @SerializedName("current_study")
    private String currentStudy;
    @SerializedName("skill")
    private String skill;
    @SerializedName("totalworkincome")
    private String totalWorkIncome;
    @SerializedName("totalreferincome")
    private String totalReferIncome;
    @SerializedName("total_balance")
    private double totalBalance;
    @SerializedName("profilepic")
    private String profilepic;
    @SerializedName("password")
    private String password;
    @SerializedName("status")
    private String status;


    private int termscondition;

    public Freelancer() {
    }

    public Freelancer(String name, String email, String phone, String referid,
                      String password, int termscondition) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.referid = referid;
        this.password = password;
        this.termscondition = termscondition;
    }


    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setThana(String thana) {
        this.thana = thana;
    }

    public void setUnion(String union) {
        this.union = union;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public String getAddress() {
        return address;
    }

    public String getDivision() {
        return division;
    }

    public String getDistrict() {
        return district;
    }

    public String getThana() {
        return thana;
    }

    public String getUnion() {
        return union;
    }

    public String getMemberFrom() {
        return memberFrom;
    }

    public String getJob() {
        return job;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public String getJobFrom() {
        return jobFrom;
    }

    public String getJobTo() {
        return jobTo;
    }

    public String getCurrentJob() {
        return currentJob;
    }

    public String getEducation() {
        return education;
    }

    public String getInstutationName() {
        return instutationName;
    }

    public String getEducationLevel() {
        return educationLevel;
    }

    public String getInstutationLocation() {
        return instutationLocation;
    }

    public String getEducationFrom() {
        return educationFrom;
    }

    public String getEducationTo() {
        return educationTo;
    }

    public String getCurrentStudy() {
        return currentStudy;
    }

    public String getSkill() {
        return skill;
    }

    public String getAccountpay() {
        return accountpay;
    }

    public String getStatus() {
        return status;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSecurityCheck(String securityCheck) {
        this.securityCheck = securityCheck;
    }

    public void setSecurityPin(String securityPin) {
        this.securityPin = securityPin;
    }

    public String getEmailExist() {
        return emailExist;
    }

    public String getPhoneExist() {
        return phoneExist;
    }

    public String getRefereeIdValidation() {
        return refereeIdValidation;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setReferid(String referid) {
        this.referid = referid;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmailExist(String emailExist) {
        this.emailExist = emailExist;
    }

    public void setPhoneExist(String phoneExist) {
        this.phoneExist = phoneExist;
    }

    public String getSecurityCheck() {
        return securityCheck;
    }

    public String getSecurityPin() {
        return securityPin;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getOwnReferId() {
        return ownReferId;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public String getOccupation() {
        return occupation;
    }

    public String getTotalWorkIncome() {
        return totalWorkIncome;
    }

    public String getTotalReferIncome() {
        return totalReferIncome;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getReferid() {
        return referid;
    }

    public String getPassword() {
        return password;
    }

    public double getTotalBalance() {
        return totalBalance;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public int getTermscondition() {
        return termscondition;
    }


    public static class Work {
        private int freelancerId;

        private String companyName;
        private String jobTitle;
        private String jobLocation;
        private String jobDescription;
        private String jobFrom;
        private String jobTo;
        private String currentJob;

        public Work(int freelancerId, String companyName, String jobTitle,
                    String jobLocation, String jobDescription, String jobFrom, String jobTo, String currentJob) {
            this.freelancerId = freelancerId;
            this.companyName = companyName;
            this.jobTitle = jobTitle;
            this.jobLocation = jobLocation;
            this.jobDescription = jobDescription;
            this.jobFrom = jobFrom;
            this.jobTo = jobTo;
            this.currentJob = currentJob;
        }

        public int getFreelancerId() {
            return freelancerId;
        }

        public String getCompanyName() {
            return companyName;
        }

        public String getJobTitle() {
            return jobTitle;
        }

        public String getJobLocation() {
            return jobLocation;
        }

        public String getJobDescription() {
            return jobDescription;
        }

        public String getJobFrom() {
            return jobFrom;
        }

        public String getJobTo() {
            return jobTo;
        }

        public String getCurrentJob() {
            return currentJob;
        }
    }

    public static class Education {
        private int freelancerId;

        private String instutation_name;
        private String education_level;
        private String instutation_location;
        private String education_from;
        private String education_to;
        private String current_study;

        public Education(int freelancerId, String instutation_name, String education_level, String instutation_location,
                         String education_from, String education_to, String current_study) {
            this.freelancerId = freelancerId;
            this.instutation_name = instutation_name;
            this.education_level = education_level;
            this.instutation_location = instutation_location;
            this.education_from = education_from;
            this.education_to = education_to;
            this.current_study = current_study;
        }

        public int getFreelancerId() {
            return freelancerId;
        }

        public String getInstutation_name() {
            return instutation_name;
        }

        public String getEducation_level() {
            return education_level;
        }

        public String getInstutation_location() {
            return instutation_location;
        }

        public String getEducation_from() {
            return education_from;
        }

        public String getEducation_to() {
            return education_to;
        }

        public String getCurrent_study() {
            return current_study;
        }
    }

    public static class Skill {
        private int freelancerId;

        private String skill;

        public Skill(int freelancerId, String skill) {
            this.freelancerId = freelancerId;
            this.skill = skill;
        }

        public int getFreelancerId() {
            return freelancerId;
        }

        public String getSkill() {
            return skill;
        }
    }

    public static class History {
        @SerializedName("payment_method")
        private String paymentMethod;

        @SerializedName("request_form")
        private String requestForm;

        @SerializedName("request_to")
        private String requestTo;
        @SerializedName("amount")
        private String amount;

        @SerializedName("payable_amount")
        private String payableAmount;

        @SerializedName("agent_commission")
        private String agentcommission;
        @SerializedName("request_type")
        private String requestType;
        @SerializedName("status")
        private String status;

        private String skill;


        public String getPaymentMethod() {
            return paymentMethod;
        }

        public String getRequestForm() {
            return requestForm;
        }

        public String getRequestTo() {
            return requestTo;
        }

        public String getAmount() {
            return amount;
        }

        public String getPayableAmount() {
            return payableAmount;
        }

        public String getAgentcommission() {
            return agentcommission;
        }

        public String getRequestType() {
            return requestType;
        }

        public String getStatus() {
            return status;
        }

        public String getSkill() {
            return skill;
        }
    }

    public static class MyOffers {
        @SerializedName("name")
        private String name;

        @SerializedName("description")
        private String description;

        @SerializedName("publication_date")
        private String publicationDate;
        @SerializedName("validation_date")
        private String validationDate;

        @SerializedName("image")
        private String image;

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public String getPublicationDate() {
            return publicationDate;
        }

        public String getValidationDate() {
            return validationDate;
        }

        public String getImage() {
            return image;
        }
    }

    public static class MyIncomePlan {
        @SerializedName("name")
        private String name;

        @SerializedName("description")
        private String description;

        @SerializedName("publication_date")
        private String publicationDate;
        @SerializedName("validation_date")
        private String validationDate;

        @SerializedName("image")
        private String image;

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public String getPublicationDate() {
            return publicationDate;
        }

        public String getValidationDate() {
            return validationDate;
        }

        public String getImage() {
            return image;
        }
    }


}
