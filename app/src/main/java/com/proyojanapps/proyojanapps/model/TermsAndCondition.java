package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

public class TermsAndCondition {
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;

    public TermsAndCondition(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
