package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

public class Upazilla {
    @SerializedName("upazilla_id")
    private int upazillaId;
    @SerializedName("name")
    private String name;

    public int getUpazillaId() {
        return upazillaId;
    }

    public String getName() {
        return name;
    }
}
