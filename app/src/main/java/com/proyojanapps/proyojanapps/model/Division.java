package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

public class Division {
    @SerializedName("division_id")
    private int divisionId;
    @SerializedName("name")
    private String name;

    public int getDivisionId() {
        return divisionId;
    }

    public String getName() {
        return name;
    }
}
