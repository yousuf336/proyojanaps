package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

public class Transaction {
    public static class AgentSendMoney {
        @SerializedName("name")
        private String name;
        @SerializedName("phone")
        private String phone;
        @SerializedName("ownreferid")
        private String ownreferid;

        @SerializedName("accountpay")
        private String accountpay;
        @SerializedName("status")
        private String status;
        @SerializedName("active_date")
        private String activeDate;

        public String getName() {
            return name;
        }

        public String getPhone() {
            return phone;
        }

        public String getOwnreferid() {
            return ownreferid;
        }

        public String getAccountpay() {
            return accountpay;
        }

        public String getStatus() {
            return status;
        }

        public String getActiveDate() {
            return activeDate;
        }
    }

    //...............................agent_cash_out_request_list.............................
    public static class AgentCashOutRequest {
        @SerializedName("id")
        private int id;
        @SerializedName("freelancer_name")
        private String freelancerName;
        @SerializedName("agent_name")
        private String agent_name;
        @SerializedName("amount")
        private String amount;
        @SerializedName("payable_amount")
        private String payableAmount;
        @SerializedName("request_type")
        private String requestType;
        @SerializedName("status")
        private String status;
        @SerializedName("pay_date")
        private String payDate;
        @SerializedName("request_date")
        private String request_date;
        //............................agent_send_money_list.............................................


        public int getId() {
            return id;
        }

        public String getFreelancerName() {
            return freelancerName;
        }

        public String getAgent_name() {
            return agent_name;
        }

        public String getAmount() {
            return amount;
        }

        public String getPayableAmount() {
            return payableAmount;
        }

        public String getRequestType() {
            return requestType;
        }

        public String getStatus() {
            return status;
        }

        public String getPayDate() {
            return payDate;
        }

        public String getRequest_date() {
            return request_date;
        }
    }

    //...............................agent_cash_out_list.............................
    public static class AgentCashOut {
        @SerializedName("agent_name")
        private String agent_name;
        @SerializedName("receiver_number")
        private String receiverNumber;
        @SerializedName("sender_number")
        private String senderNumber;
        @SerializedName("trans_number")
        private String transNumber;
        @SerializedName("amount")
        private String amount;
        @SerializedName("note")
        private String note;
        @SerializedName("status")
        private String status;
        @SerializedName("pay_date")
        private String payDate;
        @SerializedName("request_date")
        private String request_date;

        private String password;

          private int agentId;

        public AgentCashOut() {
        }

        public AgentCashOut(String receiverNumber, String amount, String note, String password, int agentId) {
            this.receiverNumber = receiverNumber;
            this.amount = amount;
            this.note = note;
            this.password = password;
            this.agentId = agentId;
        }

        public int getAgentId() {
            return agentId;
        }

        public String getPassword() {
            return password;
        }

        public String getAgent_name() {
            return agent_name;
        }

        public String getReceiverNumber() {
            return receiverNumber;
        }

        public String getSenderNumber() {
            return senderNumber;
        }

        public String getTransNumber() {
            return transNumber;
        }

        public String getAmount() {
            return amount;
        }

        public String getNote() {
            return note;
        }

        public String getStatus() {
            return status;
        }

        public String getPayDate() {
            return payDate;
        }

        public String getRequest_date() {
            return request_date;
        }
    }

    //...............................agent_cash_in_list........................................
    public static class AgentCashin {
        @SerializedName("agent_name")
        private String agentName;
        @SerializedName("sender_number")
        private String senderNumber;

        @SerializedName("trans_number")
        private String transNumber;
        @SerializedName("amount")
        private String amount;
        @SerializedName("note")
        private String note;
        @SerializedName("status")
        private String status;
        @SerializedName("pay_date")
        private String payDate;
        @SerializedName("request_date")
        private String requestDate;
        private String password;

        private int agentId;


        public AgentCashin() {
        }

        public AgentCashin(String senderNumber, String transNumber, String amount, String note, String password, int agentId) {
            this.senderNumber = senderNumber;
            this.transNumber = transNumber;
            this.amount = amount;
            this.note = note;
            this.password = password;
            this.agentId = agentId;
        }

        public String getPassword() {
            return password;
        }

        public int getAgentId() {
            return agentId;
        }

        public String getAgentName() {
            return agentName;
        }

        public String getSenderNumber() {
            return senderNumber;
        }

        public String getTransNumber() {
            return transNumber;
        }

        public String getAmount() {
            return amount;
        }

        public String getNote() {
            return note;
        }

        public String getStatus() {
            return status;
        }

        public String getPayDate() {
            return payDate;
        }

        public String getRequestDate() {
            return requestDate;
        }
    }


}
