package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

public class Slider {

    @SerializedName("image")
    private String sliderImage;


    public Slider() {
    }

    public String getSliderImage() {
        return sliderImage;
    }
}

