package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

public class Notification {
    private int id;

    @SerializedName("notification_type")
    private String title;

    @SerializedName("notification_body")
    private String message;
    private String image;
    private String appName;

    @SerializedName("created_at")
    private String date;

    @SerializedName("notification_status")
    private int status;


    public Notification() {
    }

    public Notification(int id, String title, String message, String date, int status) {
        this.id = id;
        this.title = title;
        this.message = message;
        this.date = date;
        this.status = status;
    }

    public Notification(String title, String message, String image, String appName, String date, int status) {
        this.title = title;
        this.message = message;
        this.image = image;
        this.appName = appName;
        this.date = date;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public String getImage() {
        return image;
    }

    public String getAppName() {
        return appName;
    }

    public String getDate() {
        return date;
    }

    public int getStatus() {
        return status;
    }
}
