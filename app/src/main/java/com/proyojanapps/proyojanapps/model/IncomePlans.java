package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

public class IncomePlans {

    @SerializedName("name")
    private String name;
    @SerializedName("payment")
    private String payment;


    public String getName() {
        return name;
    }

    public String getPayment() {
        return payment;
    }
}
