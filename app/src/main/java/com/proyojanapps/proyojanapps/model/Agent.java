package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

import java.io.File;

public class Agent {
    //.........................for signup.......................
    private int freelancerId;
    @SerializedName("agent_id")
    private int agentId;
    @SerializedName("name")
    private String name;
    @SerializedName("agent_reefer_id")
    private String agentReeferId;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("dateofbirth")
    private String dateOfBirth;
    @SerializedName("gender")
    private String gender;
    @SerializedName("occupation")
    private String occupation;
    @SerializedName("bloodgroup")
    private String bloodGroup;
    @SerializedName("address")
    private String address;
    @SerializedName("division")
    private String division;
    @SerializedName("district")
    private String district;
    @SerializedName("thana")
    private String thana;
    @SerializedName("union")
    private String union;
    @SerializedName("job")
    private String job;
    @SerializedName("job_title")
    private String jobTitle;
    @SerializedName("job_location")
    private String jobLocation;
    @SerializedName("job_description")
    private String jobDescription;
    @SerializedName("job_from")
    private String jobFrom;
    @SerializedName("job_to")
    private String jobTo;
    @SerializedName("company_name")
    private String companyName;
    @SerializedName("current_job")
    private String currentJob;
    @SerializedName("education")
    private String education;
    @SerializedName("instutation_name")
    private String instutationName;
    @SerializedName("education_level")
    private String educationLevel;
    @SerializedName("instutation_location")
    private String instutationLocation;
    @SerializedName("education_from")
    private String educationFrom;
    @SerializedName("education_to")
    private String educationTo;
    @SerializedName("current_study")
    private String currentStudy;
    @SerializedName("skill")
    private String skill;
    @SerializedName("agent_commission")
    private String agentCommission;
    @SerializedName("agent_balance")
    private String agentBalance;
    @SerializedName("registered_as_agent")
    private String registeredAsAgent;
    @SerializedName("agent_status")
    private String agentStatus;
    @SerializedName("profilepic")
    private String profilepic;
    private File nidfront;
    private File nidback;
    private File trans_slip;
    private String sendernumber;
    private String transectionid;
    private String amount;
    //..........................signup end.......................


    public Agent() {
    }

    public Agent(int freelancerId, String dateOfBirth, String gender, String occupation, String address,
                 String division, String district, String thana, String union, String sendernumber,
                 String transectionid, String amount, File nidfront, File nidback, File trans_slip) {
        this.freelancerId = freelancerId;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.occupation = occupation;
        this.address = address;
        this.division = division;
        this.district = district;
        this.thana = thana;
        this.union = union;
        this.sendernumber = sendernumber;
        this.transectionid = transectionid;
        this.amount = amount;
        this.nidfront = nidfront;
        this.nidback = nidback;
        this.trans_slip = trans_slip;
    }

    public int getFreelancerId() {
        return freelancerId;
    }

    public int getAgentId() {
        return agentId;
    }

    public String getName() {
        return name;
    }

    public String getAgentReeferId() {
        return agentReeferId;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public String getOccupation() {
        return occupation;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public String getAddress() {
        return address;
    }

    public String getDivision() {
        return division;
    }

    public String getDistrict() {
        return district;
    }

    public String getThana() {
        return thana;
    }

    public String getUnion() {
        return union;
    }

    public String getJob() {
        return job;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public String getJobFrom() {
        return jobFrom;
    }

    public String getJobTo() {
        return jobTo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCurrentJob() {
        return currentJob;
    }

    public String getEducation() {
        return education;
    }

    public String getInstutationName() {
        return instutationName;
    }

    public String getEducationLevel() {
        return educationLevel;
    }

    public String getInstutationLocation() {
        return instutationLocation;
    }

    public String getEducationFrom() {
        return educationFrom;
    }

    public String getEducationTo() {
        return educationTo;
    }

    public String getCurrentStudy() {
        return currentStudy;
    }

    public String getSkill() {
        return skill;
    }

    public String getAgentCommission() {
        return agentCommission;
    }

    public String getAgentBalance() {
        return agentBalance;
    }

    public String getRegisteredAsAgent() {
        return registeredAsAgent;
    }

    public String getAgentStatus() {
        return agentStatus;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public File getNidfront() {
        return nidfront;
    }

    public File getNidback() {
        return nidback;
    }

    public File getTrans_slip() {
        return trans_slip;
    }

    public String getSendernumber() {
        return sendernumber;
    }

    public String getTransectionid() {
        return transectionid;
    }

    public String getAmount() {
        return amount;
    }
}
