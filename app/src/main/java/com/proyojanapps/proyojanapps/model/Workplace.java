package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

public class Workplace {
    @SerializedName("primary_income")
    private double primaryIncome;
    @SerializedName("commission_income")
    private double commissionIncome;
    @SerializedName("secondary_income")
    private double secondaryIncome;
    @SerializedName("withdraw_income")
    private double withdrawIncome;
    @SerializedName("start_work")
    private String startWork;
    @SerializedName("work_renew_day")
    private int workRenewDay;

    @SerializedName("work_renew")
    private double workRenew;


    public double getWithdrawIncome() {
        return withdrawIncome;
    }

    public double getPrimaryIncome() {
        return primaryIncome;
    }

    public double getCommissionIncome() {
        return commissionIncome;
    }

    public double getSecondaryIncome() {
        return secondaryIncome;
    }

    public String getStartWork() {
        return startWork;
    }

    public int getWorkRenewDay() {
        return workRenewDay;
    }

    public double getWorkRenew() {
        return workRenew;
    }
}
