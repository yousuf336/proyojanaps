package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FreelancerIncomeReport {

    @SerializedName("totalworkincome")
    private String totalWorkIncome;
    @SerializedName("totalreferincome")
    private String totalReferIncome;
    @SerializedName("total_balance")
    private double totalBalance;

    private ArrayList<MemberIncome> memberIncomesList;

    public void setMemberIncomesList(ArrayList<MemberIncome> memberIncomesList) {
        this.memberIncomesList = memberIncomesList;
    }

    public String getTotalWorkIncome() {
        return totalWorkIncome;
    }

    public String getTotalReferIncome() {
        return totalReferIncome;
    }

    public double getTotalBalance() {
        return totalBalance;
    }

    public ArrayList<MemberIncome> getMemberIncomesList() {
        return memberIncomesList;
    }

  public static class MemberIncome {
        @SerializedName("name")
        private String name;
        @SerializedName("totalworkincome")
        private String totalWorkIncome;
        @SerializedName("totalreferincome")
        private String totalReferIncome;
        @SerializedName("total_balance")
        private double totalBalance;


        public String getName() {
            return name;
        }

        public String getTotalWorkIncome() {
            return totalWorkIncome;
        }

        public String getTotalReferIncome() {
            return totalReferIncome;
        }

        public double getTotalBalance() {
            return totalBalance;
        }
    }

}
