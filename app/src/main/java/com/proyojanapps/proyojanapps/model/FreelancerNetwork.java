package com.proyojanapps.proyojanapps.model;

import com.google.gson.annotations.SerializedName;

public class FreelancerNetwork {
    @SerializedName("name")
    private String name;
    @SerializedName("level")
    private String level;

    @SerializedName("phone")
    private String phone;
    @SerializedName("ownreferid")
    private String ownreferid;
    @SerializedName("referid")
    private String referid;
    @SerializedName("status")
    private String status;
    @SerializedName("created_at")
    private String created_at;


    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public String getLevel() {
        return level;
    }

    public String getOwnreferid() {
        return ownreferid;
    }

    public String getReferid() {
        return referid;
    }

    public String getStatus() {
        return status;
    }

    public String getCreated_at() {
        return created_at;
    }
}
