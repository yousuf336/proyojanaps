package com.proyojanapps.proyojanapps.model;

public class ResponseBody {
    private boolean status;
    private String message;

    public ResponseBody() {
    }

    public ResponseBody(boolean status) {
        this.status = status;
    }

    public ResponseBody(String message) {
        this.message = message;
    }

    public ResponseBody(boolean status, String message) {
        this.status = status;
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
